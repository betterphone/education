<meta content="text/html;charset=utf-8" http-equiv="content-type" />
<title></title>
<style type="text/css">
body
{
margin:5px 0px 5px 0px;
font-size:11px;
color:#15526d;
line-height: 18px;
font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
}

td
{
font-size:11px;
color:#15526d;
line-height: 18px;
font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
}

td.top_info
{
font-size:10px;
color:#ff5a00;
font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
font-weight:bold;
line-height: 14px;
background:#f6f6f6;
padding:7px;
}

table.main
{
border:2px solid #ff6f00;
}

.content
{
padding:1px;
}

.salutation
{
margin: 5px 0px;
font-weight:bold;
font-size:12px;
color:#000000;
height:40px;
}

.section_title
{
font-weight:bold;
font-size:11px;
color:#ffffff;
background: #FF952F url(<?=DIR_WS_SITE_GRAPHIC?>HdBg.jpg) repeat-x;
border-bottom:1px solid #ff6f00;
height:21px;
padding:3px;
text-transform: uppercase;
}
.lfcell
{
font-weight:bold;
font-size:11px;
color:#15526d;
background: #ffe8d2;
height:21px;
padding:4px;
}

.calc_cell
{
font-weight:bold;
font-size:11px;
color:#000000;
background: #ffe8d2;
padding:4px;
}

.rtcell
{
font-weight:normal;
font-size:11px;
color:#15526d;
background: #fff5eb;
height:21px;
padding:4px 4px 4px 10px;
background: #fff5eb url(<?=DIR_WS_SITE_GRAPHIC?>rt_lf_bg.jpg) repeat-y;
}
#email_footer
{
text-align:left;
line-height:20px;
font-weight:bold;
margin:10px 0px 10px 10px;
margin:0px 0px 0px 5px;
}
#email_center_content{
    padding:10px 10px 10px 10px;
    text-align:left;
    margin:0px 0px 0px 5px;
    background:#FFFFFF;
    color:#cecece;
    line-height:20px;
    font-weight:bold;
    }</style>
<div align="center">
    <table cellpadding="0" cellspacing="0" class="main" style="border: 2px solid #FF6600;" width="600">
        <tbody>
            <tr height="3">
                <td align="center" bgcolor="#FF6600" class="content" height="3">
                    &nbsp;</td>
            </tr>
            <tr height="3">
                <td align="center" class="content" style="padding:5px 0px 5px 2px;font-size:22px;">
                    <? echo SITE_NAME; ?></td>
            </tr>
            <tr height="3">
                <td align="center" bgcolor="#FF6600" class="content" height="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" class="content">
                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                        <tbody>
                            <tr>
                                <td align="center" class="top_info" style="background:#f5f5f5;line-height:20px;padding:3px;">
                                    Forgot Password.</td>
                            </tr>
                            <tr height="3">
                                <td align="center" bgcolor="#FF6600" class="content" height="3">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" class="salutation">
                                    <div style="margin:5px 0px;padding:5px 0px 5px 2px;background:#dcdcdc;">
                                        <span style="font-weight:bold;">Name : </span><? echo $name; ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="salutation">
                                    <div style="margin:5px 0px;padding:5px 0px 5px 2px;background:#dcdcdc;">
                                        <span style="font-weight:bold;">Email Address : </span><? echo $email; ?></div>
                                </td>
                            </tr>
							<tr>
                                <td align="left" class="salutation">
                                    <div style="margin:5px 0px;padding:5px 0px 5px 2px;background:#dcdcdc;">
                                        <span style="font-weight:bold;">Password : </span><? echo $password; ?></div>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left" class="salutation">
                                    <div style="margin:5px 0px;padding:5px 0px 5px 2px;background:#dcdcdc;">
                                        <? echo $footer; ?></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br />