<!----------- ecommerce - code - google ------------------------------->
<?PHP GLOBAL $order; ?>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?php echo GOOGLE_AC; ?>']);
    _gaq.push(['_trackPageview']);
    _gaq.push(['_addTrans',
        '<?php echo $order->transaction_id ?>', // transaction ID - required
        '<?php echo STORE_NAME; ?>', // affiliation or store name
        '<?php echo $order->grand_total ?>', // total - required
        '<?php echo $order->total_tax ?>', // tax
        '<?php echo $order->total_shipping_amount ?>', // shipping
        '<?php echo $order->billing_city ?>', // city
        '<?php echo $order->billing_state ?>', // state or province
        '<?php echo $order->billing_country ?>'        // country
    ]);
    // add item might be called for every item in the shopping cart
    // where your ecommerce engine loops through each item in the cart and
    // prints out _addItem for each
<?php foreach ($order->items as $kk => $vv): ?>
        _gaq.push(['_addItem',
                '<?php echo $order->transaction_id ?>', // transaction ID - required
                '<?php echo $vv['product_name'] ?>', // SKU/code - required
                '<?php echo $vv['product_sku'] ?>', // product name
    <?php if ($vv['product_variant_value'] != ''): ?>
            '<?php echo $vv['product_variant_value'] ?>', // variation
    <?php else: ?>
            '<?php echo product::getProductDefaultCategoryName($vv['product_id']); ?>', // category
    <?php endif; ?>
            '<?php echo $vv['unit_price'] ?>', // unit price - required
                '<?php echo $vv['quantity'] ?>'         // quantity - required
        ]);
<?php endforeach; ?>
    _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
<!----------- /ecommerce - code - google ------------------------------->