<?php

/*
 * Question Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class question extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('question');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'urlname', 'description', 'position', 'category_id', 'question_type', 'level', 'round', 'is_active', 'is_deleted', 'meta_title', 'meta_desc', 'meta_keyword', 'date_add', 'date_upd');
    }

    /*
     * Create new page or update existing page
     */

    function savePage($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if ($this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['name']);
        }

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                cwebc::concatenateIdWithSlug('question', $new_id);
                return $new_id;
            } else {
                return false;
            }
        }
    }

    function list_question($q_id) {
        $this->Where = "WHERE `id` = '$q_id'";
        return $this->DisplayOne();
    }

    /*
     * Get page by id
     */

    function getPage($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    function getQuiz($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all pages in array
     */

    function listPages() {
        return $this->ListOfAllRecords();
    }

    function listPages_using_cat_id($category_id) {
        if ($category_id) {
            $this->Where = "WHERE `category_id` = '$category_id'";
        }
        return $this->ListOfAllRecords();
    }

    // function question_using_category($category_id) {
    //    $this->Where = "WHERE `category_id` = '$category_id'";
    //  return $this->ListOfAllRecords();
    // }

    function question_using_level_category($category_id, $round) {
        $this->Where = "WHERE `category_id` = '$category_id' AND `round` = '$round' ORDER BY id";
        return $this->ListOfAllRecords();
    }

    function total_question_in_category($category_id) {
        $this->Field = "COUNT(*) as count";
        $this->Where = "WHERE `category_id` = '$category_id'";
        $data = $this->DisplayOne();
        return $data->count;
    }

    function question_using_level_category_remaining($category_id, $round, $done = array()) {
        $this->Where = "WHERE `category_id` = '$category_id' AND `round` = '$round' AND id NOT IN(" . implode(',', $done) . ") ORDER BY id";
        return $this->ListOfAllRecords();
    }

    function total_round_question_count($category_id, $round) {
        $this->Field = "COUNT(*) as count";
        $this->Where = "WHERE `category_id` = '$category_id' AND `round` = '$round'";
        $data = $this->DisplayOne();
        return $data->count;
    }

    function total_level_question_count($category_id, $level_start, $level_end) {
        $this->Field = "COUNT(*) as count";
        $this->Where = "WHERE `category_id` = '$category_id' AND (`round` <= '$level_end' AND `round` >= '$level_start')";
        $data = $this->DisplayOne();
        return $data->count;
    }

    function getTotalRounds($category_id, $level) {
        $this->Field = "MAX(round) as total_round";
        $this->Where = "WHERE `category_id` = '$category_id' AND `level` = '$level'";
        $rounds = $this->DisplayOne();
        return $rounds->total_round;
    }

    /*
     * delete a page by id
     */

    function deleteQuestion($id) {
        $this->id = $id;
        $this->Delete();
    }

    /*
     * Update page position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function getPagequestion($id) {
        return html_entity_decode($this->getPage($id)->page);
    }

    function getPageTitle($id) {
        return $this->getPage($id)->page_name;
    }

    function getPageSlug($id) {
        return $this->getPage($id)->urlname;
    }

}

class question_quiz extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('question_quiz');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'question_id', 'answer_id', 'user_id', 'level_id', 'time', 'round');
    }

    function Save_Quiz($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        } else {
            $this->Insert();
        }
    }

    function fetch() {
        $this->Where = "WHERE `level_id` = 'Easy'";
        return $this->ListOfAllRecords();
    }

    function fetch1() {
        $this->Where = "WHERE `level_id` = 'Intermediate'";
        return $this->ListOfAllRecords();
    }

    function fetch2() {
        $this->Where = "WHERE `level_id` = 'Challenging'";
        return $this->ListOfAllRecords();
    }

    function check_question_exist($q, $user_id, $round) {
        $this->Where = "WHERE `question_id` = '$q' AND `user_id` = '$user_id' AND `round` = '$round'";
        return $this->DisplayOne();
    }

}

?>
