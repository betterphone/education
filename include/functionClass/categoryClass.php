<?php

/*
 * Category Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class category extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('category');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'urlname', 'image', 'description', 'position', 'parent_id', 'is_active', 'is_deleted', 'date_add', 'date_upd', 'meta_title', 'meta_keyword', 'meta_desc');
    }

    /*
     * Create new Category or update existing Category
     */

    function saveCategory($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!isset($this->Data['urlname']) || $this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['name']);
        }

        $this->Data['date_add'] = date("Y-m-d H:i:s");
        $this->Data['is_active'] = isset($POST['is_active']) ? $POST['is_active'] : "0";

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $data_id = $this->Data['id'];
            if ($this->Update()):
                return $data_id;
            endif;
        }
        else {
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                cwebc::concatenateIdWithSlug('category', $new_id);
                return $new_id;
            }
        }
        return false;
    }

    function categories() {
        $this->Where = "ORDER BY `position` ASC";
        return $this->ListOfAllRecords();
    }

    /*
     * Get Category by id
     */

    function getCategory($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all Category in array with parent id
     */

    function listCategory($pid = 0, $show_active = false, $result_type = 'object') {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' and parent_id='$pid' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' and parent_id='$pid' order by $this->orderby $this->order, id desc";
        endif;

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords();
    }

    function listCategoryWithSubcatsExistance($pid = 0, $active = false) {

        $query = new query('category as c');

        if ($active):
            $query->Field = "c.*, (select count(*) from " . TABLE_PREFIX . "category where is_active='1' and is_deleted='0' and parent_id=c.id)as subcount";
        else:
            $query->Field = "c.*, (select count(*) from " . TABLE_PREFIX . "category where is_deleted='0' and parent_id=c.id)as subcount";
        endif;

        if ($active):
            $query->Where = "where c.is_active='1' and c.is_deleted='0' and c.parent_id='$pid' order by c.position asc,c.id desc";
        else:
            $query->Where = "where c.is_deleted='0' and c.parent_id='$pid' order by c.position asc,c.id desc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    /*
     * Get List of all Category in array without parent category
     */

    function listAllCategories($show_active = false, $result_type = '', $get_all = TRUE) {
        if ($get_all === FALSE) {
            $this->Field = 'id,name';
        }
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        endif;

        if ($result_type == 'array')
            return $this->ListOfAllRecords();
        else
            return $this->DisplayAll();
    }

    function getAllParentsCat() {
        $this->Field = 'id,name';
        $this->Where = "where parent_id='0' and is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        return $this->ListOfAllRecords();
    }

    function listSubCategories($parent_id = '0', $show_active = false, $result_type = '') {
        if ($show_active):
            $this->Where = " where is_active='1' and is_deleted='0' ";
        else:
            $this->Where = "where is_deleted='0' ";
        endif;

        $this->Where.= " and parent_id='$parent_id' order by $this->orderby $this->order";

        if ($result_type == 'array')
            return $this->ListOfAllRecords('object');
        else
            return $this->DisplayAll();
    }

    // get all subcategories of a category recursive
    function getAllSubCategoryIdsRecursive($pid = 0, $show_active = false) {
        $categories = array();
        $categories[] = $pid;
        $Query = new query('category as cc');
        $Query->Field = "cc.id,cc.name";
        if ($show_active):
            $Query->Where.=" where cc.is_active='1' and cc.is_deleted='0' and cc.parent_id='$pid'";
        else:
            $Query->Where.=" where cc.is_deleted='0' and cc.parent_id='$pid'";
        endif;
        $result = $Query->ListOfAllRecords('object');

        if (!empty($result)):
            foreach ($result as $kk => $vv) {
                $recursiveObj = new category();
                $sub_cats = $recursiveObj->getAllSubCategoryIdsRecursive($vv->id, $show_active);
                if (!empty($sub_cats)):
                    foreach ($sub_cats as $sub_cat):
                        $categories[] = $sub_cat;
                    endforeach;
                endif;
            }
        endif;
        return $categories;
    }

    function getCategoryNamebyId($id) {
        $this->Field = " id,name";
        $this->Where = " where id='$id'";
        $object = $this->Displayone();
        if ($object):
            return $object->name;
        endif;
        return false;
    }

    /*
     * Get Parent Category Id
     */

    function getParentCategoryId($id) {
        $this->Where = " where id='$id'";
        $object = $this->Displayone();
        return $object->parent_id;
    }

    /*
     * Update Category position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    /*
     * delete a Category by id
     */

    function deleteCategory($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* check category by name */

    public static function checkCategoryByName($name) {
        $query = new query('category');
        $query->Field = " id,name";
        $query->Where = " where name='$name'";
        $object = $query->Displayone();
        if ($object):
            return $object->id;
        endif;
        return false;
    }

    /*
     * get all active categories ids for sitemap
     */

    function getIdsActiveCategory() {
        $this->Field = "id";
        $this->Where = " where is_active='1' and is_deleted='0'";
        return $this->ListOfAllRecords('object');
    }

}

?>
