<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

include_once(DIR_FS_SITE . 'include/functionClass/userMetaClass.php');

class user extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('user');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'username', 'password', 'ip_address', 'last_visit', 'total_visit', 'is_active', 'is_email_verified', 'clinic_name', 'web_address', 'firstname', 'lastname', 'education', 'shipping_firstname', 'shipping_lastname', 'shipping_address1', 'shipping_address2', 'phone', 'mobile', 'city_id', 'zip_code', 'state_id', 'country_id', 'is_newsletter', 'newsletter_add_date', 'forgot_password_token', 'email_confirmation_token', 'file', 'reg_info', 'practitioner_type', 'is_deleted', 'date_add', 'date_upd', 'profile_picture');
    }

    /*
     * check user exists
     */

    function checkUserExists($username, $password) {
        $this->Where = "where is_deleted='0' AND username='" . mysql_real_escape_string($username) . "' AND password='" . mysql_real_escape_string($password) . "'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    function checkFielExists($name, $value) {
        $this->Where = "where  `" . $name . "`='" . mysql_real_escape_string($value) . "'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    function checkFielExistsOther($name, $value, $id) {
        $this->Where = "where  `" . $name . "`='" . mysql_real_escape_string($value) . "' AND id!='" . $id . "'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    function list_Users() {
        return $this->ListOfAllRecords();
    }

    /*
     * get user from email token
     */

    function getUserByEmailToken($token) {
        $this->Where = " where is_deleted='0' and email_confirmation_token='$token'";
        $user = $this->DisplayOne();
        if (is_object($user)) {
            return $user;
        } else {
            return false;
        }
    }

    /*
     * add user from admin
     */

    function updateUserbyAdmin($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if ($this->Data['password'] != ''):
            $this->Data['password'] = md5($this->Data['password']);
        endif;

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        $this->Data['is_newsletter'] = isset($this->Data['is_newsletter']) ? '1' : '0';
        $this->Data['newsletter_add_date'] = isset($this->Data['is_newsletter']) ? date("Y-m-d H:i:s") : '';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            unset($this->Data['username']);
            $id = $this->Data['id'];
            if ($this->Update()):
                return $id;
            endif;
        }
        else {
            $this->Data['is_email_verified'] = '1';
            $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()):
                return $this->GetMaxId();
            endif;
        }
        return false;
    }

    /*
     * login user
     */

    public static function loginUser($user_id, $user_key) {

        $QueryObj1 = new user;
        $user = $QueryObj1->getUser($user_id);

# set logged in and redirect to needed page.
        $login_session = new user_session();
        $login_session->user_id = $user->id;
        $login_session->verified = $user->is_approved;
        $login_session->set_user_id();
        $login_session->username = $user->username;
        $login_session->set_username();

#Update total visits
        $QueryObj1 = new user;
        $QueryObj1->updateTotalVisits($user->id, $user->total_visit);

#Update last visit
        $QueryObj1 = new user;
        $QueryObj1->updateUserLastVisit($user->id);


        /* update user recent views */
        product_views::UpdateUserRecentViews($user->id, $user_key);

        /* update session wishlist to user */
        user_wishlist::UpdateUserWishlist($user->id, $user_key);

        return true;
    }

    /*
     * check user 
     */

    function checkUser($id) {
        $this->Where = " where id='$id' and is_deleted='0' and is_active='1'";
        if ($this->DisplayOne()):
            return true;
        else:
            return false;
        endif;
    }

    /*
     * check user exists with email ---using---
     */

    function checkUserEmailExists($username) {
        $this->Where = "where is_deleted='0' AND username='" . mysql_real_escape_string($username) . "'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    /* update forgot password verify code of user */

    function updateForgotPasswordVerifyCode($id, $code) {

        $this->Data['id'] = $id;
        $this->Data['forgot_password_token'] = $code;
        if ($this->Update()):
            return true;
        else:
            return false;
        endif;
    }

    /* check user exists or not by code */

    function getUserFromForgotPasswordCode($code) {
        $this->Where = " where `forgot_password_token` = '$code'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        else:
            return false;
        endif;
    }

    /*
     * Get List of all users in array
     */

    function listUsers($active = false, $rtype = '', $limited_info = false) {
        if ($limited_info):
            $this->Field = "id,username,companyname,firstname,lastname,age,gender,address1,address2,phone";
        endif;

        if ($active):
            $this->Where.=" where is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        else:
            $this->Where.=" where is_deleted='0' order by $this->orderby $this->order";
        endif;

        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    /*
     * Get List of all users in array
     */

    function listAllUsers() {

        $this->Where = "where is_deleted='0' AND user_type='user'";
        $this->DisplayAll();
    }

    function getUsers() {
        $this->Where = "where is_deleted='0' AND user_type='user'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * Get List of all admin users in array
     */

    function listAdminUsers() {
        $this->Where = "where is_deleted='0'";
        $this->DisplayAll();
    }

    /*
     * unsubscribe user for newsletter
     */

    function unsubscribeUserNewsletter($id) {
        $this->Data['id'] = $id;
        $this->Data['is_newsletter'] = '0';
        return $this->Update();
    }

    /*
     * subscribe user for newsletter
     */

    function subscribeUserNewsletter($id) {
        $this->Data['id'] = $id;
        $this->Data['is_newsletter'] = '1';
        $this->Data['newsletter_add_date'] = date("Y-m-d H:i:s");
        return $this->Update();
    }

    function getAdminUsers() {
        $this->Field = "id,business_email,first_name,last_name,user_type,department";
        $this->Where = "where is_deleted='0' AND user_type='admin'";

        return $this->ListOfAllRecords('object');
    }

    /*
     * Create new user ---using---
     */

    function createUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        /* $rand=rand(0, 99999999);
          $image_obj=new imageManipulation();

          if($image_obj->upload_photo('logo', $_FILES['business_logo'], $rand)):
          $this->Data['business_logo']=$image_obj->makeFileName($_FILES['business_logo']['name'], $rand);
          endif; */

        $this->Data['password'] = md5($this->Data['password']);
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Data['date_add'] = date("Y-m-d H:i:s");
        $this->Insert();
        return $this->GetMaxId();
    }

    /*
     * update user info
     */

    function updateUserDetail($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $POST['id'];
        }
        return false;
    }

    /*
     * get latest users 
     */

    function listLatestUsers($limit = 0) {
        $this->Where = " where is_deleted= '0' order by date_add desc";
        if ($limit != '0'):
            $this->Where.=" limit 0,$limit";
        endif;
        return $this->ListOfAllRecords();
    }

    /*
     * get user email from id
     */

    public static function getUserEmail($id) {
        $query = new user();
        $query->Where = "where id='$id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->username;
        endif;
        return false;
    }

    /*
     * change user password
     */

    function changePassword($id, $password) {
        $this->Data['id'] = $id;
        $this->Data['password'] = $password;
        if ($this->Update()):
            return true;
        else:
            return false;
        endif;
    }

    /*
     * Create new admin
     */

    function saveAdminUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['user_type'] = 'admin';
        $this->Data['password'] = md5($this->Data['password']);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function saveUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * get user new accounts
     */

    function getCountofNewAccounts($from_date, $to_date) {
        $this->Field = " count(*) as count";
        $this->Where = " where cast(date_add as DATE) >= '$from_date' AND CAST(date_add as DATE) <= '$to_date'";
        $object = $this->DisplayOne();
        return $object->count;
    }

    /* save user data */

    function saveUserDetail($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($POST['password'])):
            $this->Data['password'] = md5($this->Data['password']);
        endif;

        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $POST['id'];
        }
        else {
            $this->Data['reg_date'] = date('Y-m-d');
            $this->Data['is_active'] = '1';
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*     * ***** my custom query ******* */

    function getUser($id) {
        $Query = new query('user as cu');
        $Query->Field = "cu.*,cc.name as city_name,cco.short_name as country_name,cs.name as state_name";
        $Query->Where = " left join " . TABLE_PREFIX . "city as cc on cc.id=cu.city_id";
        $Query->Where.=" left join " . TABLE_PREFIX . "country as cco on cco.id=cu.country_id";
        $Query->Where.=" left join " . TABLE_PREFIX . "state as cs on cs.id=cu.state_id";
        $Query->Where.=" where cu.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    /*     * ***** my custom query end ******* */

    function updateTotalVisits($id, $total_visit = 0) {
        $this->Data['id'] = $id;
        $this->Data['total_visit'] = $total_visit + 1;
        $this->Update();
    }

    /*
     * update user last visit when login
     */

    function updateUserLastVisit($id) {
        $this->Data['id'] = $id;
        $this->Data['last_visit'] = date("Y-m-d H:i:s");
        $this->Data['last_visit_ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Update();
    }

    function getUserLimitedInfo($id) {
        $this->Field = "id,username,companyname,firstname,lastname,age,gender,address1,address2,phone";
        $this->Where = "where id='" . $id . "'";
        $user = $this->DisplayOne();
        return $user;
    }

    /*
     * Get admin by id
     */

    function getAdminUser($id) {
        return $this->_getObject('user', $id);
    }

    function check_user_name_exists($username, $id = 0) {
        $username = mysql_real_escape_string($username);
        $id = mysql_real_escape_string($id);

        if ($username == ""):
            return false;
        endif;

        $this->Field = "id";
        if ($id && $id != 0 && $id != ""):
            $this->Where = "where username='" . $username . "' AND id!='" . $id . "'";
        else:
            $this->Where = "where username='" . $username . "'";
        endif;


        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            return $user;
        else:
            return false;
        endif;
    }

    function verify_url_string($string) {

        if ($string != '')
            if (substr($string, -1) == '/')
                return substr($string, 0, strlen($string) - 1);

        return $string;
    }

    function getUsernameFromURL() {


        $string_parts = array();
        $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE);
        $URL = $_SERVER['REQUEST_URI'];
        $string = substr($URL, -(strlen($URL) - strlen($prefix)));
        $string = $this->verify_url_string($string);
        $string_parts = explode('/', $string);

        $user_name = isset($string_parts['1']) ? $string_parts['1'] : "";

        return $user_name;
    }

    function getUserFromUsername($username) {
        $username = mysql_real_escape_string($username);
        if ($username == ""):
            return false;
        endif;

        $this->Where = "where username='" . $username . "'";
        $buss = $this->DisplayOne();
        if ($buss && is_object($buss)):
            return $buss;
        else:
            return false;
        endif;
    }

    /* set user email verified */

    function setEmailVerified($id) {

        $this->Data['id'] = $id;
        $this->Data['is_email_verified'] = '1';
        $this->Data['email_confirmation_token'] = '';
        $this->Update();
        return true;
    }

    /* set user email verified */

    function setEmailVerifiedAndSignUpComplete($id) {

        $this->Data['id'] = $id;
        $this->Data['is_email_verified'] = '1';
        $this->Data['is_signup_complete'] = '1';
        $this->Data['is_on_demo'] = '0';
        $this->Update();
    }

    /*
     * check admin user with name
     */

    function checkUsers($username, $email) {
        $this->Where = "where is_deleted='0' AND (username='" . mysql_real_escape_string($username) . "' OR email='" . mysql_real_escape_string($email) . "')";
        $this->DisplayAll();
    }

    /*
     * check admin user with name in update case
     */

    function checkUsersWithID($username, $email, $id) {
        $this->Where = "where is_deleted='0' AND id!='$id' AND (username='" . mysql_real_escape_string($username) . "' OR business_email='" . mysql_real_escape_string($email) . "')";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deleteAdminUser($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /* update business logo */

    function updateLogo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('logo', $_FILES['business_logo'], $rand)):
            $this->Data['business_logo'] = $image_obj->makeFileName($_FILES['business_logo']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {


            if ($this->Update())
                return $this->Data['id'];
        }
        else {

            return false;
        }
    }

    /* update business favicon */

    function updateFavicon($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('favicon', $_FILES['business_favicon'], $rand)):
            $this->Data['business_favicon'] = $image_obj->makeFileName($_FILES['business_favicon']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {


            if ($this->Update())
                return $this->Data['id'];
        }
        else {

            return false;
        }
    }

    /* update theme id of user */

    function updateThemeId($id, $theme_id) {

        $this->Data['id'] = $id;
        $this->Data['theme_id'] = $theme_id;
        $this->Update();
    }

    /* check if demo period of user is not expired */

    function ValidateDemoPeriod($reg_date, $is_on_demo = 0) {

        if ($is_on_demo && $is_on_demo != 0):

            /* user is on demo period, check date */
            $new_obj = new date();
            $new_date = $new_obj->add_days_to_date(COUNT_DEMO_DAYS, $reg_date);
            $current_date = date('Y-m-d');

            if (strtotime($current_date) > strtotime($new_date)):
                return false;
            else:
                return true;
            endif;


        else:
            return true;
        endif;
    }

    /* check if demo period of user is not expired */

    function GetTotalCompleteSteps($com_steps) {
        $total_complete = 0;

        if (count($com_steps) && is_array($com_steps)):
            foreach ($com_steps as $k => $v):
                if ($v == 1):
                    $total_complete++;
                endif;

            endforeach;
        endif;

        return $total_complete;
    }

    /* get array of user navigation according to selected modules
     * $id is user id
     * $sub_navigation, send it true for getting sub navigation
     * $sub_navigation_limit, set it 0 if no limit
     */

    function GetUserNavigation($id, $sub_navigation = 0, $sub_navigation_limit = 0) {
        global $user_meta;
        $navigation = array();
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";

        $user = $this->DisplayOne();
        if ($user && is_object($user)):
            $modules = array();

            /* get all selected theme modules */
            $modules = array();
            $modules = unserialize(html_entity_decode($user_meta->user_module_config));

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);

            $sr = 0;
            foreach ($modules as $k => $v):

                if (in_array($v['page_name'], $selected_modules)):
                    $navigation[$v['page_name']]['page_name'] = $v['title'];
                    $navigation[$v['page_name']]['page'] = $v['page_name'];

                    if ($sub_navigation && $sub_navigation == 1):/* get subnavigation according to page */

                        if ($k == 'service'): /* get user services */
                            /* Get limited services */
                            $services = array();
                            $service_obj = new service();
                            $service_obj->setUserId($id);
                            $services = $service_obj->listLimitedActiveServices('array', $sub_navigation_limit, 1);
                            $navigation[$v['page_name']]['sub_navigation'] = $services;

                        elseif ($k == 'product'):
                            /* Get limited products */
                            $products = array();
                            $product_obj = new product();
                            $product_obj->setUserId($id);
                            $products = $product_obj->listLimitedActiveProducts('array', $sub_navigation_limit, 1);
                            $navigation[$v['page_name']]['sub_navigation'] = $products;
                        endif;



                    endif;
                endif;


            endforeach;


        endif;

        return $navigation;
    }

    /* get user not to open pages */

    function GetUserNotOpenPages($id) {
        global $user_meta;
        $not_open_pages = array();
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";

        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            /* get all selected theme modules */
            $modules = array();
            $modules = unserialize(html_entity_decode($user_meta->user_module_config));

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);
            $sr = 0;
            foreach ($modules as $k => $v):

                if (!in_array($v['page_name'], $selected_modules)):
                    $not_open_pages[] = $v['page_name'];
                endif;

            endforeach;

        endif;

        return $not_open_pages;
    }

    /*
     * Get count of all users
     */

    function countUsers($user_type = 'user', $show_active = 0) {
        $total_count = 0;
        $this->Field = "id";
        if ($show_active):
            $this->Where = "where user_type='$user_type' AND is_deleted='0' AND is_active='1' ";
        else:
            $this->Where = "where user_type='$user_type' AND is_deleted='0' ";

        endif;

        $object = $this->ListOfAllRecords('object');

        if (count($object)):
            $total_count = count($object);
        endif;

        return $total_count;
    }

    /* check user password is correct */

    function checkUserPassword($id, $password) {

        $password = md5($password);
        $this->Field = "id";
        $this->Where = "where id='$id' AND password='$password'";

        $object = $this->DisplayOne();

        if (is_object($object) && count($object)):
            return true;
        endif;

        return false;
    }

    /* update  verify code of user */

    function updatedVerifyCode($id, $code) {

        $this->Data['id'] = $id;
        $this->Data['verify_code'] = $code;
        $this->Update();
    }

    /* set wizard complete */

    function setWizardComplete($id) {

        $this->Data['id'] = $id;
        $this->Data['is_wizard_complete'] = '1';
        $this->Update();
    }

    /* copy logo to favicon */

    function copyFavicon($image, $id) {

        $this->Data['id'] = $id;
        $this->Data['business_favicon'] = $image;
        $this->Update();

        /* copy logo image to favicon folder */
        $source_path = DIR_FS_SITE_UPLOAD . 'photo/logo/large/' . $image;
        $destination_path = DIR_FS_SITE_UPLOAD . 'photo/favicon/large/' . $image;

        copy($source_path, $destination_path);

        $image_fav = new imageManipulation();
        $image_fav->create_resized_for_module('favicon', $image);
    }

    /* function to hide a module for user */

    function HideModuleForUser($id, $module) {
        $module_find = 0;
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";
        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);

            foreach ($selected_modules as $k => $v):

                if ($v == $module):
                    unset($selected_modules[$k]);
                    $module_find = 1;
                endif;

            endforeach;

            if ($module_find == 1): /* unset module and save array again */
                $query = new user();
                $query->Data['id'] = $id;
                $query->Data['module'] = serialize($selected_modules);
                $query->Update();
                return true;
            else:
                return false; /* failed to hide */
            endif;

        else:
            return false; /* failed to hide */

        endif;
    }

    /*
     * count total new users of today
     */

    function getCountTodayUsers() {
        $this->Field = " count(*) as count";
        $this->Where = " where DATE(date_add) = CURDATE()";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->count;
        else:
            return '0';
        endif;
    }

    function is_email_password_exist($username, $password) {
        $this->Where = "WHERE `username` = '$username' AND `password` = '$password'";

        return $this->DisplayOne();
    }

    function password_exist($password) {
        $this->Where = "WHERE `password` = '$password'";
        return $this->DisplayOne();
    }

    function list_user_using_email($username) {
        $this->Where = "WHERE `username` = '$username'";
        return $this->DisplayOne();
    }

    function email_exist($username) {
        $this->Where = "WHERE `username` = '$username'";
        return $this->DisplayOne();
    }

    function list_user($user_id) {
        $this->Where = "WHERE `id` = '$user_id'";
        return $this->DisplayOne();
    }

    function update_field($user_id, $field, $value) {
        $this->Data[$field] = $value;
        $this->Data['id'] = $user_id;
        return $this->Update();
    }

    function update_field_custom($field, $value, $col, $col_value) {
        $this->Data[$field] = $value;
        $this->Where = " WHERE `$col` = '$col_value'";
        return $this->UpdateCustom();
    }

    function is_value($field, $value) {
        $this->Where = "WHERE `$field` = '$value'";
        return $this->DisplayOne();
    }

}

/* user Points */

class user_points extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'desc', $orderby = 'id', $parent_id = 0) {
        parent::__construct('user_points');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'cat_id', 'round_id', 'user_id', 'points', 'date_add');
    }

    /*
     * add user Points
     */

    function saveUserPoints($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()) {
                return $id;
            } else {
                return false;
            }
        } else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()) {
                return $this->GetMaxId();
            } else {
                return false;
            }
        }
    }

    function checkPrvRound($user_id, $cat_id) {
        $this->Field = "MAX(round_id) as total_round";
        $this->Where = "WHERE `cat_id` = '$cat_id' AND `user_id` = '$user_id'";
        $rounds = $this->DisplayOne();
        return $rounds->total_round + 1;
    }

    function my_points($user_id, $from, $to, $category_id) {
        $this->Field = "SUM(`points`) as sum_points";
        $this->Where = "`cwebc_user_points` WHERE (`round_id` BETWEEN $from AND $to) AND `user_id` = '$user_id' AND `cat_id` = '$category_id'";
        $data = $this->DisplayOne();
        return $data->sum_points;
    }

    function all_points($user_id) {
        $this->Field = "SUM(`points`) as sum_points";
        $this->Where = "`cwebc_user_points` WHERE `user_id` = '$user_id'";
        $data = $this->DisplayOne();
        return $data->sum_points;
    }

}

class information extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('information');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'sex', 'location', 'dob', 'question', 'answer');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $id;
            } else {
                return false;
            }
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            } else {
                return false;
            }
        }
    }

    function is_exist($user_id) {
        $this->Where = "WHERE `user_id` = '$user_id'";
        return $this->DisplayOne();
    }

    function update_($POST, $user_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Where = " WHERE `user_id` = '$user_id'";
        return $this->UpdateCustom();
    }

    function list_user($user_id) {
        $this->Where = "WHERE `user_id` = '$user_id'";
        return $this->DisplayOne();
    }

    function question_answer_check($user_id, $question, $answer) {
        $this->Where = "WHERE `user_id` = '$user_id' AND `question` = '$question' AND `answer` = '$answer'";
        return $this->DisplayOne();
    }

}

class countries extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('country');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function countries_all() {
        return $this->ListOfAllRecords();
    }

}

class state extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('state');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function state_country_id_all($country_id) {
        $this->Where = "WHERE `country_id` = '$country_id'";
        return $this->ListOfAllRecords();
    }

}

class bonus extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('bonus');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $id;
            } else {
                return false;
            }
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            } else {
                return false;
            }
        }
    }

    function my_points($user_id) {
        $this->Field = "SUM(`points`) as sum";
        $this->Where = "WHERE `user_id` = '$user_id'";
        $data = $this->DisplayOne();
        return $data->sum;
    }

}

?>