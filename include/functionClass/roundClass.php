<?php

class round extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('round');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'points', 'level', 'total_time');
    }

    /*
     * Create new page or update existing page
     */

    function saveRound($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                cwebc::concatenateIdWithSlug('question', $new_id);
                return $new_id;
            } else {
                return false;
            }
        }
    }

    /*
     * Get page by id
     */

    function getRound($id) {
        $this->Where = " where `id` = '$id'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all pages in array
     */

    function listRound() {
        return $this->ListOfAllRecords();
    }

}

?>
