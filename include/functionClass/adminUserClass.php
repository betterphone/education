<?php
/*
 * Admin Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class admin extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    
     /*
      * 
     */
    function __construct($order='asc', $orderby='position'){
        $this->InitilizeSQL();
        $this->orderby=$orderby;
        $this->order=$order;
        parent::__construct('user');
        $this->TableName='cwebc_admin_user';
        $this->requiredVars=array('id', 'admin_role_id', 'username', 'password', 'phone', 'email', 'last_sign_in', 'is_active','is_deleted','last_update_date','ip_address','last_access','allow_pages','is_loggedin');
    }

     /*
     * Create new page or update existing page
     */
    function saveAdminUser($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['email']=$_POST['email'];
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
           
            if($this->Update())
              return $Data['id'];
        }
        else{
            
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get page by id
     */
    function getAdminUser($id){
        return $this->_getObject('admin_user', $id);
    }
    
    function getAdmin($id) {
        $this->Where = " where `id` = '$id'";
        return $this->DisplayOne();
    }
   
    
        function list_Admin_Users(){
        return $this->ListOfAllRecords();        
    }
     
    function listAdminUsers(){
        $this->Where="where is_deleted='0' ";    
        $this->DisplayAll();        
    }
    
    /*
     * check admin user with name
     */
    function checkUsers($username,$email){
        $this->Where="where is_deleted='0' AND (username='".mysql_real_escape_string($username)."' OR email='".mysql_real_escape_string($email)."')";    
        $this->DisplayAll();        
    }
    
    /*
     * check admin user with name in update case
     */
    function checkUsersWithID($username,$email,$id){
        $this->Where="where is_deleted='0' AND id!='$id' AND (username='".mysql_real_escape_string($username)."' OR email='".mysql_real_escape_string($email)."')";  
        $this->DisplayAll();        
    }
    
    /*
     * delete a page by id
     */
  
    
  function deleteAdminUser($id) {
        $this->id = $id;
        $this->Delete();
    }
    
   
}
?>