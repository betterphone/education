<?php

class prize extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('prize');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    /*
     * Create new page or update existing page
     */

    function saveprize($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                cwebc::concatenateIdWithSlug('question', $new_id);
                return $new_id;
            } else {
                return false;
            }
        }
    }

    function list_prizes() {
        return $this->ListOfAllRecords();
    }

    /*
     * Get page by id
     */

    function getPrize($id) {
        $this->Where = " where `id` = '$id'";
        return $this->DisplayOne();
    }
    function getPrize_user_id($user_id) {
        $this->Where = " where `user_id` = '$user_id'";
        return $this->ListOfAllRecords();
    }

    /*
     * Get List of all pages in array
     */

    function listPages() {
        return $this->ListOfAllRecords();
    }

    /*
     * delete a page by id
     */

    function deleteprize($id) {
        $this->id = $id;
        $this->Delete();
    }

    /*
     * Update page position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function getPagequestion($id) {
        return html_entity_decode($this->getPage($id)->page);
    }

    function getPageTitle($id) {
        return $this->getPage($id)->page_name;
    }

    function getPageSlug($id) {
        return $this->getPage($id)->urlname;
    }

}

?>
