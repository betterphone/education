<?php

/*
 * Abstract Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class cwebc extends query {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($table) {
        parent::__construct($table);
        $this->TableName = TABLE_PREFIX . $table;
        $this->orderby = 'position';
        $this->order = 'asc';
    }

    /*
     * Create new page or update existing page
     */

    function saveObject($POST) {
//        pr($POST);
//        die;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? $this->Data['is_active'] : 0;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
//            $this->print = 1;
            $this->Insert();
            return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Create new page or update existing page with seo information
     */

    function saveObjectWithSeo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? 1 : 0;

        if ($this->Data['meta_name'] == '') {
            $this->Data['meta_name'] = $this->Data['name'];
        }

        if ($this->Data['meta_keyword'] == '') {
            $this->Data['meta_keyword'] = $this->Data['name'];
        }

        if ($this->Data['meta_description'] == '') {
            $this->Data['meta_description'] = $this->Data['name'];
        }

        if ($this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['name']);
        }

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get page by id
     */

    function getObject($id) {
        return $this->_getObject($this->TableName, $id);
    }

    public static function getUrlname($table, $id) {
        $query = new query($table);
        $query->Field = "urlname";
        $query->Where = " where id='$id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->urlname;
        else:
            return false;
        endif;
    }

    /*
     * get id by url name
     */

    public static function getIdByUrlname($table, $urlname) {
        $query = new query($table);
        $query->Field = "id";
        $query->Where = " where urlname='$urlname'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->id;
        else:
            return '0';
        endif;
    }

    /*
     * Get List of all pages in array
     */

    function ListAll() {
        $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deleteObject($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * permanently delete an item by id
     */

    function purgeObject($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Restore Item an item by id
     */

    function restoreObject($id) {
        $this->id = $id;
        return $this->Restore();
    }

    /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */

    function getThrash() {
        $this->Where = "where is_deleted='1'";
        $this->DisplayAll();
    }

    /*
     * Update position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    /*
     * set active
     */

    function setActive($id) {
        $this->Data['id'] = $id;
        $this->Data['is_active'] = "1";
        $this->Update();
    }

    /*
     * set inactive
     */

    function setInactive($id) {
        $this->Data['id'] = $id;
        $this->Data['is_active'] = "0";
        $this->Update();
    }

    function setPendingReview($id) {
        $this->Data['id'] = $id;
        $this->Data['is_active'] = "2";
        $this->Update();
    }

    function getVar($var, $id) {
        return html_entity_decode($this->getObject($id)->$var);
    }

    /*
     * Enable Paging
     */

    function enablePaging($pageNo = 1, $pageSize = 10) {
        $this->AllowPaging = 1;
        $this->PageNo = $pageNo;
        $this->PageSize = $pageSize;
    }

    function getTotalPages() {
        return $this->TotalPages;
    }

    function getTotalRecords() {
        return $this->TotalRecords;
    }

    /*
     * count total records
     */

    function countTotalRecords($active = false) {
        $this->Field = "count(*) as counttotal";
        $this->Where = " where 1 = 1";
        if ($active):
            $this->Where.=" and is_active='1'";
        endif;
        $this->Where.=" and is_deleted='0'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->counttotal;
        endif;
        return '0';
    }

    /*
     * get active and non deleted sitemap entries
     */

    function getSitemapRecords($is_active = true, $include = 'global') {
        $this->Field = 'id,name,urlname,include_xml_sitemap,sitemap_priority';
        $this->Where = ' where 1=1';
        if ($is_active):
            $this->Where .= ' and is_active="1"';
        endif;
        $this->Where .= ' and is_deleted="0" and include_xml_sitemap = "' . $include . '"';
        return $this->ListOfAllRecords('object');
    }

    /*
     * concetenate id with url name
     */

    public static function concatenateIdWithSlug($table, $id) {
        $query = new query($table);
        $query->Where = " urlname=concat(urlname,'-', id) ";
        $query->Where.=" where id=$id";
        return $query->UpdateCustom();
    }

    /* check urlname in new insert */

    public static function checkUrlnameExists($table, $urlname, $id) {
        $query = new query($table);
        $query->Field = "id,urlname";
        $query->Where = "where urlname='$urlname' and id != $id";
        $query->DisplayAll();
        if ($query->GetNumRows()):
            return true;
        else:
            return false;
        endif;
    }

    /* get image by module
     * either from feed or from user server
     */

    public static function getImage($image, $module = '', $size = 'medium', $from_feed = false, $feed_type = '', $rtype = 'echo', $admin = false) {
        $return_image = '';
        $image = str_replace(' ', '%20', $image);
        if ($image != ''):
            if ($from_feed):
                $img_FS_path = DIR_FS_SITE_MAIN_FEED_IMAGES . $feed_type . '/' . $module . '/' . $size . '/' . $image;
                $img_WS_path = DIR_WS_SITE_MAIN . 'feed_images/' . $feed_type . '/' . $module . '/' . $size . '/' . $image;
            else:
                $img_FS_path = DIR_FS_SITE . 'upload/photo/' . $module . '/' . $size . '/' . $image;
                $img_WS_path = DIR_WS_SITE . 'upload/photo/' . $module . '/' . $size . '/' . $image;
            endif;

            if (file_exists($img_FS_path)):
                $return_image = $img_WS_path;
            else:
                $return_image = DIR_WS_SITE . 'theme/default/graphic/noimage_' . $size . '.jpg';
            endif;
        else:
            $return_image = DIR_WS_SITE . 'theme/default/graphic/noimage_' . $size . '.jpg';
        endif;

        if ($rtype == 'echo'):
            echo $return_image;
            return;
        else:
            return $return_image;
        endif;
    }

    public static function show_meta_title_snippet_value($meta_title, $name, $section) {
        $result = '';

        $section = strtoupper($section);

        $start_title = '<span class="seo_result_title">';
        $end_title = '</span>';

        $useful_meta = $meta_title;
        if ($useful_meta == ''):
            $useful_meta = $name;
        endif;
        $useful_meta = ucwords($useful_meta);

        if ($section == 'PRODUCT'):
            $defined_pattern = PRODUCT_PAGE_META_TITLE;
            if ($useful_meta == ''):
                $useful_meta = 'PRODUCT_NAME';
            endif;
        elseif ($section == 'BRAND'):
            $defined_pattern = BRAND_PAGE_META_TITLE;
            if ($useful_meta == ''):
                $useful_meta = 'BRAND_NAME';
            endif;
        elseif ($section == 'CONTENT'):
            $defined_pattern = CONTENT_PAGE_META_TITLE;
            if ($useful_meta == ''):
                $useful_meta = 'PAGE_NAME';
            endif;
        elseif ($section == 'CATEGORY'):
            $defined_pattern = CATEGORY_PAGE_META_TITLE;
            if ($useful_meta == ''):
                $useful_meta = 'CATEGORY_NAME';
            endif;
        else:
            $defined_pattern = DEFAULT_PAGE_META_TITLE;
            if ($useful_meta == ''):
                $useful_meta = 'PAGE_NAME';
            endif;
        endif;

        $real_meta = $start_title . $useful_meta . $end_title;

        $defined_values = array('%%SITENAME%%', '%%PRODUCT_NAME%%', '%%PAGE_NAME%%', '%%BRAND_NAME%%', '%%CATEGORY_NAME%%');

        $new_values = array(SITE_NAME, $real_meta, $real_meta, $real_meta, $real_meta, $real_meta);

        $new_meta_title = str_replace($defined_values, $new_values, $defined_pattern);

        //$new_meta_title = substr($new_meta_title,0,(MAXIMUM_META_TITLE_LENGHT+38));

        echo $new_meta_title;

        return true;
    }

    public static function show_meta_desc_snippet_value($meta_desc, $name) {

        $useful_meta = $meta_desc;

        if ($useful_meta == '' && FILL_META_DESC_AUTOMATICALLY):

            $useful_meta = trim(strip_tags($name));

        endif;

        //$useful_meta = html_entity_decode($useful_meta);

        $descrptn = $useful_meta;
        //$descrptn =  substr($descrptn,0,MAXIMUM_META_DESC_LENGHT);

        echo $descrptn;

        return true;
    }

}

?>