<?php
/*
 * Email Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class email_template extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='asc', $orderby='id', $parent_id=0){
        parent::__construct('email');
            $this->orderby=$orderby;
            $this->parent_id=$parent_id;
            $this->order=$order;
            $this->requiredVars=array('id', 'email_name','email_fields', 'email_subject', 'email_text', 'email_type','from_name');
    }

    /*
     * Create new page or update existing page
     */
    function saveEmailTemplate($POST){
       $this->Data=$this->_makeData($POST, $this->requiredVars);
        
       if($this->Data['from_name']==''):
           $this->Data['from_name']=STORE_NAME;
       endif;
       if(isset($this->Data['id']) && $this->Data['id']!=''){
           $id = $this->Data['id'];
            if($this->Update())
              return $id;
        }
        else{
            if($this->Insert()):
                 return $this->GetMaxId();
            endif;
        }
        return false;
    }
        
    /*
     * Get Email by id
     */
    function getEmailTemplate($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    
    /*
     * Get List of all Email in array
     */
    function listEmailTemplates(){

        $this->Where="where 1=1 order by $this->orderby $this->order";
        return $this->DisplayAll();        
        
    }

   
}
?>