<?php
class answer extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('quest_ans');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'quest_id', 'answer', 'is_correct', 'position', 'date_add');
    }

    function saveAnswer($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if (isset($_POST['is_correct'])) {
                $this->Data['is_correct'] = 1;
            } else {
                $this->Data['is_correct'] = '0';
            }
            if ($this->Update())
                return $Data['id'];
        }
        else {
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                return $new_id;
            } else {
                return false;
            }
        }
    }

    function list_answer($q_id) {
        $this->Where = "WHERE `quest_id` = '$q_id'";
        return $this->ListOfAllRecords();
    }

    function list_answer_front($q_id) {
        $this->Where = "WHERE `quest_id` = '$q_id' ORDER BY `position` ASC";
        return $this->ListOfAllRecords();
    }

    function total_answer_front_correct($q_id) {
		$this->Field = "COUNT(*) as total";	
        $this->Where = "WHERE `quest_id` = '$q_id' AND `is_correct`='1' ORDER BY `position` ASC";
        $rec=$this->DisplayOne();
		return $rec->total;
    }	
	
    function allCorrectAnswerArray($q_id) {
		$record=array();
		$this->Where = "WHERE `quest_id` = '$q_id' AND `is_correct`='1' ORDER BY `position` ASC";
        $rec=$this->ListOfAllRecords('object');
		if(!empty($rec)):
			foreach($rec as $key=>$value):
				$record[]=$value->id;
			endforeach;
		endif;
		return $record;
    }	

    function list_answer_correct($q_id) {
        $this->Where = "WHERE `quest_id` = '$q_id' AND `is_correct` = 1 ORDER BY `position` ASC";
        return $this->DisplayOne();
    }
	
	
    
    function list_answer_incorrect($q_id) {
        $this->Where = "WHERE `quest_id` = '$q_id' AND `is_correct` = '0' ORDER BY `position` ASC";
        return $this->ListOfAllRecords();
    }
     
    

    function list_answer_using_answer_id($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

    function list_answer_using_id($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    function deleteAnswer($id) {
        $this->id = $id;
        $this->Delete();
    }

}

?>
