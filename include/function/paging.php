<?php
function PageControlAdmin($page, $totalPages, $totalRecords, $url, $type=1, $Class='pad', $tdclass='', $Title='Records', $LClass='cat') {
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 10):
        if (($page + 10) <= $totalPages):
            $end = $page + 10;
            $begin = $page;
        else:
            $begin = $totalPages - 10;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    
    $start_record = (ADMIN_CATEGORY_PAGE_SIZE*($page-1) +1);
    $end_records = ($start_record-1)+ADMIN_CATEGORY_PAGE_SIZE;
    if($end_records>$totalRecords):
        $end_records = $totalRecords;
    endif;
    ?>
<div class="row-fluid">
    <div class="span6">
       <!-- Current Page : <?php echo $page;?> | Total Pages : <?php echo $totalPages;?> -->
        Showing <?php echo $start_record;?> to <?php echo $end_records;?> of <?php echo $totalRecords;?> entries
    </div>
    <div class="span6">
        <div class="dataTables_paginate paging_bootstrap pagination">
            <ul class="pagination" style="visibility: visible;">
                <li>
                    <a href="<?= $url ?>&p=<?= $Pp ?>" title="Previous Page"><i class="icon-angle-left"></i> Prev</a>
                </li>
            <?php for ($i = $begin; $i <= $totalPages && $i <= $end; $i++): ?>
                <?php if ($i == $page): ?>
                <li class="active" >
                    <a href="<?= $url ?>&p=<?= $i ?>" title="Page No: <?= $i ?>"><?= $i ?></a>
                </li>
                <?php else: ?>
                <li>
                    <a href="<?= $url ?>&p=<?= $i ?>"  title="Page No: <?= $i ?>"><?= $i ?></a>
                </li>
                <?php endif; ?>
            <?php endfor; ?>
                <li>
                    <a href="<?= $url ?>&p=<?= $Np ?>" title="Next Page">Next <i class="icon-angle-right"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php
}



function PageControlFront($page, $totalPages, $totalRecords, $url,$querystring='') {
    # $Pp-previous page
    # $Np- next page
      $total_blocks=4;
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > $total_blocks):
        if (($page + $total_blocks) <= $totalPages):
            $end = $page + $total_blocks;
            $begin = $page;
        else:
            $begin = $totalPages - $total_blocks;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>
<div class="shop__pagination">
            <ul id="pagination" class="pagination">
                <li>
                    <?php echo display_url('<', $url, 'p='.$Pp.'&'.$querystring,'pagination--nav paging_link',$Pp);?>
                </li>
                <?php if($page!=1):?>
                <li>
                     <?php echo display_url('First', $url, 'p=1&'.$querystring,'paging_link','1');?>
                  
                </li>
                <?php endif;?>
                
            <?php for ($i = $begin; $i <= $totalPages && $i <= $end; $i++): ?>
                <?php if ($i == $page): ?>
                <li>
                    <?php echo display_url($i, $url, 'p='.$i.'&'.$querystring,'active paging_link',$i);?>
                </li>
                <?php else: ?>
                <li>
                    <?php echo display_url($i, $url, 'p='.$i.'&'.$querystring,'paging_link',$i);?>
                </li>
                <?php endif; ?>
            <?php endfor; ?>
                
              <?php if($page!=$totalPages):?>
                <li>
                    
                     <?php echo display_url('Last', $url, 'p='.$totalPages.'&'.$querystring,'paging_link',$totalPages);?>
                   
                </li>
               <?php endif;?>
                
                <li>
                   <?php echo display_url('>', $url, 'p='.$Np.'&'.$querystring,'pagination--nav paging_link',$Np);?>
                   
                </li>
            </ul>
</div>
<?php
}


function PageControlFrontLoadMore($page, $totalPages, $totalRecords,$remaining_products =0, $PAGE,$query_string='', $Class='pad', $tdclass='', $Title='Records', $LClass='cat') {
    # $Pp-previous page
    # $Np- next page
    $url = make_url($PAGE);
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 10):
        if (($page + 10) <= $totalPages):
            $end = $page + 10;
            $begin = $page;
        else:
            $begin = $totalPages - 10;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>
<div class="col-xs-12  col-sm-12">
    <div class="load_more_outer">
        <a class="btn btn-primary--reverse-transition" id="load_more" rel="<?php echo $Np;?>" page="<?php echo $PAGE;?>" url="<?php echo $url;?>" href="javascript:void()" query_string="<?php echo $query_string;?>" title="Load More">
            <span id="loading_pagination">
            <span class="glyphicon glyphicon-repeat"></span>&nbsp;&nbsp;Load More
            </span>
            <?php if($remaining_products!='0'): ?>
                (<span id="remaining_count"><?php echo $remaining_products;?></span>)
            <?php endif; ?>
        </a>
    </div>
</div>
<?php
}
?>