<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <html lang="en">
        <head>
            <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans:700,300italic,400italic,700italic,300,400' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
                        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
                        <meta http-equiv="cache-control" content="no-cache" />
                        <?php echo head(isset($content) ? $content : ''); ?>
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
                        <?php css($array = array('bootstrap.min', 'style', 'design', 'validationEngine.jquery')); ?>
                        </head>
                        <body>