<?php require 'html.php' ?>
<nav class="navbar navbar-default navbar-fixed-top" id="root_path" data-root="<?php echo DIR_WS_SITE . 'tmp/ajax.php' ?>">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo DIR_WS_SITE ?>"><?php echo SITE_NAME ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">

                <?php if ($_SESSION['user_session']['user_id']) { ?>
                    <li><a href="<?php echo make_url('dashboard') ?>">Dashboard</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Play Game <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo make_url('home'); ?>">All Categories</a></li> 
                            <li><a href="<?php echo make_url('home&type=resume'); ?>">Resume Last Game</a></li> 
                            <li><a href="<?php echo make_url('home&type=new'); ?>">Start New</a></li> 

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo make_url('profile_edit'); ?>">Profile</a></li> 
                            <li><a href="<?php echo make_url('change_password'); ?>">Change password</a></li>
                            <li><a href="<?php echo make_url('history') ?>">My Points</a></li>
                            <li><a href="<?php echo make_url('summary') ?>">My Prizes</a></li>

                        </ul>
                    </li>
                    <li class="active"><a href="<?php echo make_url('logout') ?>">Logout <span class="sr-only">(current)</span></a></li>           
                <?php } else { ?>
                    <li class=""><a href="<?php echo make_url('signup') ?>">Register<span class="sr-only">(current)</span></a></li> 
                    <li class=""><a href="<?php echo make_url('sign_in') ?>">Login<span class="sr-only">(current)</span></a></li> 
                    <?php } ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>