<?php
include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');

$query = new content;
$header_links = $query->contents();
?>
<footer>
    <div class="footer" id="footer">
        <div class="container">
            <ul class="nav navbar-nav text-center">
                <?php foreach ($header_links as $header_link) { ?>
                    <li>
                        <a href="<?php echo make_url('content&id=' . $header_link['id']) ?>"><?php echo $header_link['name'] ?></a>
                    </li>
                <?php } ?>   
                <li><a href="<?php echo make_url('contact_us') ?>">Contact Us</a></li>
            </ul>
        </div>
    </div>
</footer>
<?php require 'script.php' ?>