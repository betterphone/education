<?php js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'validation/jquery.validationEngine', 'validation/languages/jquery.validationEngine-en', 'jquery.simple.timer', 'question')); ?>
<script>
    $(function () {
        $(".validation").validationEngine();
    });
</script>
</body>
</html>