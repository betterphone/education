<?php

include_once(DIR_FS_SITE . 'include/functionClass/productClass.php');
(isset($_GET['category_id']) && $_GET['category_id']!='') ? $category_id = $_GET['category_id'] : $category_id = '0';
(isset($_GET['brand_id']) && $_GET['brand_id']!='') ? $brand_id = $_GET['brand_id'] : $brand_id = '0';
(isset($_GET['price_from']) && $_GET['brand_id']!='') ? $price_from = $_GET['price_from'] : $price_from = '0';
(isset($_GET['price_to']) && $_GET['price_to']!='') ? $price_to = $_GET['price_to'] : $price_to = '0';
(isset($_GET['pid']) && $_GET['pid']!='') ? $pid = $_GET['pid'] : $pid = '0';
(isset($_GET['relation']) && $_GET['relation']!='') ? $relation = $_GET['relation'] : $relation = 'up-sell';

if($category_id == '0' && $brand_id  == '0' && $price_from  == '0' && $price_to  == '0' ):
    $show_table = false;
    $products = array();
else:
    $show_table = true;
    $Query = new product();
    $products = $Query -> filterProductsForCategoryRelation($category_id,$brand_id,$price_from,$price_to,$pid,$relation);
endif;

if($relation=='up-sell'):
    $position = 'upsells';
elseif($relation=='cross-sell'):
    $position = 'crosssells';
elseif($relation=='related'):
    $position = 'related';
endif;
?>
<div class="modal-dialog">
        <div class="modal-content">
            <?php if($show_table): ?>
                <form class="horizontal-form" action="<?php echo make_admin_url('product', 'relation', 'update','id='.$pid.'&position='.$position)?>" method="POST" id="validation">
                <div class="modal-body" style="padding-top: 0;padding-bottom: 0;">
                    <table class="table table-striped table-bordered table-hover" id="filter_products_relations">
                        <thead>
                             <tr class="heading">
                                <th width="5%" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#filter_products_relations .checkboxes" /></th>
                                <th>Product</th>
                                <th width="8%">UPC</th>
                                <th width="8%">Unit Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($products)): ?>
                                <?php $sr = 1; foreach($products as $kk=>$vv): ?>
                                    <tr class="odd GradeX">
                                        <td><input class="checkboxes" id="multiopt[<?php echo $vv->id ?>]" name="multiopt[<?php echo $vv->id ?>]" type="checkbox" /></td>
                                        <td><?php echo $vv->name;?></td>
                                        <td><?php echo $vv->upc;?></td>
                                        <td class="hidden-480"><?php echo $vv->unit_price;?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div> 
                <div class="modal-footer">
                    <input type="hidden" name="product_id" value="<?php echo $pid;?>"/>
                    <input type="hidden" name="relation_type" value="<?php echo $relation;?>"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="update_relation" class="btn blue">Add Products & Close</button>
                </div>
                </form>
            <?php else: ?>
                <div class="modal-body">
                    Please input at least one search option.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            <?php endif; ?>
        </div>
</div>
<div class="clearfix"></div>