<?php
isset($_POST['bid']) ? $bid = $_POST['bid'] : $bid = '0';

include_once(DIR_FS_SITE . 'include/functionClass/bannerClass.php');

$q = new banner_location();
$banner_location = $q->getBannerLocation($bid);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
                <i class="icon-film"></i>
                Edit Banner Location (
                <strong>
                    <?php echo ucwords($banner_pages_array[$banner_location->type]); ?>
                </strong>)
            </h4>
        </div>
        <form class="horizontal-form" action="<?php echo make_admin_url('banner', 'update_location', 'update', 'id=' . $banner_location->banner_id) ?>" method="POST" id="validation">
            <div class="modal-body" >
                <div class="form-group">
                    <label class="control-label span2">Valid Range</label>
                    <div class="span5">
                        <div class="input-group input-xlarge input-daterange">
                            <input type="text" readonly class="form-control form_datetime_sql" name="from_date" value="<?php echo ($banner_location->from_date == '0000-00-00 00:00:00') ? date("Y-m-d H:i:s") : $banner_location->from_date; ?>"/>
                            <span class="input-group-addon">
                                to
                            </span>
                            <input type="text" readonly class="form-control form_datetime_sql" name="to_date" value="<?php echo ($banner_location->to_date == '0000-00-00 00:00:00') ? date("Y-m-d H:i:s", strtotime("+1 day")) : $banner_location->to_date; ?>"/>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block">
                            date time range for which banner can be applied
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label span2">Banner Location</label>
                    <div class="span5">
                        <select name="position" id="position" class="form-control">
                            <?php foreach ($banner_locations_on_each_page as $kk => $vv) { ?>
                                <?php if ($banner_location->type == 'mega_menus' && ($kk == 'Left' || $kk == 'Right')) { ?>
                                    <option value="<?php echo $kk; ?>" <?php echo ($banner_location->position == $kk) ? "selected" : ""; ?>><?php echo $kk; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <input type="hidden" name="id" value="<?php echo $bid; ?>"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit_categories" name="submit_edit_location" class="btn blue"><i class="icon-ok"></i> Update & close</button>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>