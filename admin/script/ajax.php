<?php
include_once('../../include/config/config.php');

if (isset($_GET['act'])) {
    extract($_GET);
    if ($act == 'get_submenus') {
        $show_submit = FALSE;
        if (array_key_exists($module, $navigationPages)) {
            $show_submit = TRUE;
            if ($navigationPages[$module][1] === TRUE) {
                switch ($module) {
                    case 'blog':
                        @include_once '../../include/functionClass/blogClass.php';
                        $blog = new blog();
                        $blog->listBlogs(1);
                        $data = [];
                        while ($row = $blog->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Blog</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap validate[required] chosen-select" name="navigation_query">
                                    <?php foreach ($data as $blog) { ?>
                                        <option value="id=<?php echo $blog->id ?>"><?php echo $blog->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;
                    case 'product':
                        @include_once '../../include/functionClass/productClass.php';
                        $object = new product();
                        $object->listProduct(true, '', false);
                        $data = [];
                        while ($row = $object->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Product</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                    <?php foreach ($data as $info) { ?>
                                        <option value="id=<?php echo $info->id ?>"><?php echo $info->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;
                    case 'content':
                        @include_once '../../include/functionClass/contentClass.php';
                        $object = new content();
                        $object->listPages(FALSE);
                        $data = [];
                        while ($row = $object->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Content Page</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                    <?php foreach ($data as $info) { ?>
                                        <option value="id=<?php echo $info->id ?>"><?php echo $info->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;
                    case 'shop':
                        @include_once '../../include/functionClass/categoryClass.php';
                        $object = new category();
                        $object->listAllCategories(1, '', FALSE);
                        $data = [];
                        while ($row = $object->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Category Page</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                    <?php foreach ($data as $info) { ?>
                                        <option value="id=<?php echo $info->id ?>"><?php echo $info->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;
                    case 'brand':
                        @include_once '../../include/functionClass/brandClass.php';
                        $object = new brand();
                        $object->listBrand(1, '', 1);
                        $data = [];
                        while ($row = $object->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Brand</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                    <?php foreach ($data as $info) { ?>
                                        <option value="id=<?php echo $info->id ?>"><?php echo $info->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;
                    default:
                        break;
                }
            }
        }
        if ($show_submit === TRUE) {
            ?>
            <div class="form-group">
                <label class="span2 control-label" for="is_active">Active</label>
                <div class="span8">
                    <input class="" type="checkbox" name="is_active" id="is_active" value="1" checked />
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="offset2">
                    <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Save</button>
                    <button class="btn default" type="submit" name="cancel">Cancel</button>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="offset2" style="font-weight: bold">Please Select a Module from Drop Down Menu...</div>
            <?php
        }
    }
    if ($act == "special_cats") {
        @include_once '../../include/functionClass/categoryClass.php';
        @include_once '../../include/functionClass/productClass.php';
//        $local = TRUE;
        $local = FALSE; //uncomment this before uploading
        $main_cat = $local === TRUE ? 55 : 33;
        $clone_cats = $local === TRUE ? '255,256' : '266,267';
        $obj = new category_product;
        $data = $obj->getProductsOfCategory($main_cat);
        foreach ($data as $key => $value) {
            static $cat_products = array();
            $cat_products[] = $value->product_id;
        }
        $clone_cats = explode(',', $clone_cats);
        foreach ($clone_cats as $cat_id) {
            $obj = new category_product;
            if (isset($del)) {
                $Query = "DELETE FROM $obj->TableName WHERE category_id=$cat_id";
                $obj->ExecuteQuery($Query);
            }
            if (!isset($skip)) {
                foreach ($cat_products as $prod_id) {
                    $obj = new category_product;
                    $obj->saveCategoryProductRelation($cat_id, $prod_id);
                }
            }
        }
    }
    if ($act == 'special_alter') {
        $obj = new query('banner');
        $Query = "ALTER TABLE `$obj->TableName` ADD `mobile_image` VARCHAR(255) NOT NULL AFTER `image`";
        $obj->ExecuteQuery($Query);
        echo $Query;
    }
    if ($act == 'special_setting_insert') {
        $obj = new query('setting');
        $Query = "INSERT INTO `$obj->TableName` (`key`, `value`, `name`, `title`, `type`, `options`, `hint`, `position`, `status`) VALUES
('TRACKING_CODE_FOOTER', '', 'tracking_codes', 'Footer Tracking Codes', 'textarea', '', 'Space to add various tracking codes to the bottom of the website', 2, 1),
('TRACKING_CODE_HEADER', '', 'tracking_codes', 'Header Tracking Codes', 'textarea', '', 'Space to add various tracking codes to the &lt;head&gt; of the website', 1, 1);";
        $obj->ExecuteQuery($Query);
        echo $Query;
    }
}
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'make_mega_menu') {
        @include_once '../../include/functionClass/navigationClass.php';
        $obj = new navigationItem;
        $data = ['id' => $menu_id, 'is_mega_menu' => $value];
        $obj->Data = $data;
        $obj->Update();
    }
}