<?php
include_once('../../include/config/config.php');
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');

$category_id = isset($_REQUEST['select_parent_category_id']) ? $_REQUEST['select_parent_category_id'] : '0';

$categories = array();
if ($category_id != '' && $category_id != '0'):
    $QueryCat = new category();
    $categories = $QueryCat->listRecursiveCategory($category_id);
endif;

if (!empty($categories)):
    $sr = 1;
    ?> <table class="table table-striped table-bordered" id="sample_2"><tbody> <?php
    foreach($categories as $kk => $object): ?>
    <tr class="">
        <td class="hidden-480">
            <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
        </td>
        <td>
            <a target="_blank" href="<?php echo make_url('shop', 'id='.$object->id); ?>" title="View category page">
                <?php echo $object->name ?>
                
                <?php if(!empty($object->sub_cats)): ?>
                    <a rel="<?php echo $object->id; ?>" class="selected_parent_category_id">
                        <i class="icon-chevron-down"></i>
                    </a>
                <?php endif; ?>
            </a>
        </td>
        <td>
            <a href="<?php echo make_admin_url('product', 'list', 'list', 'cat_id='.$object->id); ?>" title="view products">
                 
                <span class="label label-sm label-<?php echo ($object->products>0)?'success':'warning';?>"><?php echo $object->products; ?></span>
            
            </a>
        </td>
        <td class="center">
            <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $object->id ?>'>
                <div module="category" class="btn status_on mini_buttons <?php echo ($object->is_active=='1')?'active':''; ?>" on="<?php echo $object->id; ?>" rel="<?php echo ($object->is_active=='1')?'on':'off'; ?>" >ON</div>
                <div module="category" class="btn status_off mini_buttons button_right <?php echo ($object->is_active=='0')?'active':''; ?>" off="<?php echo $object->id; ?>" rel="<?php echo ($object->is_active=='1')?'off':'on'; ?>">OFF</div>
            </div>
        </td>
        <td class="hidden-480">
            <input type="text" name="position[<?php echo $object->id ?>]" value="<?php echo $object->position; ?>" maxlength="5" size="5"/>
        </td>
        <td>
            <a href="<?php echo make_admin_url('category', 'list', 'list', 'pid='.$object->id); ?>" class="label label-sm label-info">
                <i class="icon-sitemap"></i> Subcategory
            </a>
        </td>
        <td>
            <a href="<?php echo make_admin_url('category', 'update', 'update', 'id='.$object->id) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> Edit</a>
            <a href="<?php echo make_admin_url('category', 'delete', 'list', 'id='.$object->id) ?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> Trash</a>
        </td>
    </tr>
    <?php if(!empty($object->sub_cats)): ?>
        <tr class="" id="sub_cat_row_<?php echo $object->id; ?>" style="display:none;">
            <td id="sub_cat_td_<?php echo $object->id;?>"  colspan="7"></td>
        </tr>
    <?php 
          endif;
          $sr++;
    endforeach; 
    ?> </tbody></table> <?php
endif;
die;
?>