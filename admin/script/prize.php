<?php

include_once(DIR_FS_SITE . 'include/functionClass/prizeClass.php');

$modName = 'prize';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
switch ($action):
    case 'list':
        $query = new prize();
        $prizes = $query->list_prizes();

        break;
    case 'insert':
        if (isset($_POST['submit'])) {
            $query = new prize();
            $query->saveprize($_POST);
            Redirect(make_admin_url('prize'));
        }

        $query = new user();
        $users = $query->list_Users();
        break;

    case 'update':
        $query = new prize();
        $prize = $query->getPrize($id);
        if (isset($_POST['submit'])) {
            $query = new prize();
            $query->saveprize($_POST);
            Redirect(make_admin_url('prize'));
        }

        $query = new user();
        $users = $query->list_Users();
        break;
    case 'delete':
        $query = new prize();
        $query->deleteprize($id);
        $admin_user->set_pass_msg('The record has been deleted successfully.');
        Redirect(make_admin_url('prize', 'list', 'list'));
        break;

endswitch;
?>
