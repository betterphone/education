<?php
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');

$modName='category';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;
isset($_GET['pid'])?$pid=$_GET['pid']:$pid=0;

$parent_id = 0;
$QueryObj1 = new category();
if($pid!=0):
    $parent_id = $QueryObj1->getParentCategoryId($pid);
elseif($id!=0):
    $parent_id = $QueryObj1->getParentCategoryId($id);
endif;

/*Handle actions here.*/
switch ($action):
	case 'list':
			$QueryObj= new category();
			$categories = $QueryObj->listCategoryWithSubcatsExistance($pid);
	break;
	case 'update':
        /* update Category */
		if(isset($_POST['submit'])):
                    if($_POST['id']!=$_POST['parent_id']):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/

                            
                                $QueryObj = new category();    
                                $getId = $QueryObj ->saveCategory($_POST);
                           
                            
                            if($getId):
                                $admin_user->set_pass_msg('Category has been updated successfully.');
                             Redirect(make_admin_url('category', 'list', 'list'));
                                if($_POST['submit']=='SubmitClose'):
                                    Redirect(make_admin_url('category', 'list', 'list'));   
                                endif;
                            else:
                                $admin_user->set_error();
                                $admin_user->set_pass_msg('Error occurred while updating category.');
                            endif;
                        endif;
                        else:
                            $admin_user->set_error();
                            $admin_user->set_pass_msg('Category and parent category cannot be same.');
                        endif;
                        Redirect(make_admin_url('category', 'update', 'update', 'id='.$id));
		endif;
		
                /* get Category contents */
                $Query_obj= new category();
                $values=$Query_obj->getCategory($id);
                
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('category', 'list', 'list'));
                endif;

		break;

                
	case 'insert':
             
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $Category= new category();    
                            $new_id = $Category->saveCategory($_POST);

                            if($new_id):
                                $admin_user->set_pass_msg('Category has been inserted successfully.');
                             Redirect(make_admin_url('category', 'list', 'list'));
                                if($_POST['submit']=='SubmitClose'):
                                    Redirect(make_admin_url('category', 'list', 'list')); 
								endif;
                            else:
                                $admin_user->set_error();
                                $admin_user->set_pass_msg('Error occurred while inserting category.');
                            endif;
                            Redirect(make_admin_url('category', 'update', 'update', 'id='.$new_id));
                        endif;
                endif;
                break;
        case 'delete':
                $content= new category(); 
                $content->deleteCategory($id);
                    
                /* Delete Index code */
                $SearchObj = new searchIndex();
                $SearchObj->deleteIndex($id, 'category');
                /* End Delete Index code */
                
                $admin_user->set_pass_msg('Category has been deleted successfully.');
                Redirect(make_admin_url('category', 'list', 'list'));
                break;
            
        case 'delete_image':
                if($id){
                    $QueryObj = new category();
                    $object = $QueryObj->getCategory($id);
                    
                    $QueryObj1 = new category();
                    $QueryObj1->removeImage($id);
                    
                    #delete images from all folders
                    $image_obj=new imageManipulation();
                    $image_obj->DeleteImagesFromAllFolders('Category',$object->image);
                    
                    $admin_user->set_pass_msg('Image has been successfully deleted.');
                    Redirect(make_admin_url('category', 'update', 'update','id='.$id));
                } 
                break;
                
         case'permanent_delete':
		$QueryObj = new category();
		$QueryObj->purgeObject($id);
                      
                /* Permanent Delete Index code */
                $SearchObj = new searchIndex();
                $SearchObj->permanentIndexDelete($id, 'category');
                /* End Permanent Delete Index code */
                
                // permanent url rewrrite entry delete
                url_rewrite::deleteUrlRewriteEntry('category', $id);
                
		$admin_user->set_pass_msg('Category has been deleted successfully');
		Redirect(make_admin_url('category', 'thrash', 'thrash'));
        	break;
         
          case'restore':
		$QueryObj = new category();
		$QueryObj->restoreObject($id);
                     
                /* Resote Index code */
                $SearchObj = new searchIndex();
                $SearchObj->RestoreIndex($id, 'category');
                /* End Resote Index code */
                
		$admin_user->set_pass_msg('Category has been restored successfully');
		Redirect(make_admin_url('category', 'thrash', 'thrash'));
        	break;
            
          case'thrash':
		$QueryObj = new category();
		$QueryObj->getThrash();
		break;
    default:break;
endswitch;
?>