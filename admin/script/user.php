<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$modName = 'user';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_GET['aid']) ? $aid = $_GET['aid'] : $aid = 0;

/* Handle actions here. */
switch ($action):
    case 'list':
        $QueryObj = new user();
        $admin = $QueryObj->list_Users();
        break;
    case 'update':
        $query = new admin();
        $admin = $query->getAdmin($id);
        if (isset($_POST['submit'])) {
            $query = new admin;
            $query->saveAdminUser($_POST);
            $admin_user->set_pass_msg('User has been updated successfully.');
            Redirect(make_admin_url('user', 'list', 'list'));
        }
        break;
    case 'insert':
        if (isset($_POST['submit'])) {
            $query = new admin;
            $query->saveAdminUser($_POST);
            $admin_user->set_pass_msg('User has been added successfully.');
            Redirect(make_admin_url('user', 'list', 'list'));
        }
        break;
    case 'delete':
        $query = new admin();
        $admin = $query->deleteAdminUser($id);
        $admin_user->set_pass_msg('User has been deleted successfully.');
        Redirect(make_admin_url('user', 'list', 'list'));
        break;
    default:break;
endswitch;
?>
