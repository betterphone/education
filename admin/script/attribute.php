<?php
include_once(DIR_FS_SITE.'include/functionClass/attributeClass.php');

$modName='attribute';

$action = isset($_GET['action'])?$_GET['action']:'list';
$section = isset($_GET['section'])?$_GET['section']:'list';
$id = isset($_GET['id'])?$_GET['id']:0;
$super = isset($_GET['super'])?$_GET['super']:0;
$set = isset($_GET['set'])?$_GET['set']:0;

/*Handle actions here.*/
switch ($action):
	case 'list_super':
                    $QueryObj= new attribute_super_set();
                    $QueryObj->listAttributeSuperSets();
                    break;
                
        case 'list_set':
                if($super!='0'):
                    $QueryObj_s= new attribute_super_set();
                    $super_set = $QueryObj_s->getAttributeSuperSet($super);
                    if(!is_object($super_set)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something went wrong.');
                        Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                    endif;
                    
                    $QueryObj= new attribute_set();
                    $QueryObj->listAttributeSets($super);
                else:
                   $admin_user->set_error();
                   $admin_user->set_pass_msg('Something went wrong.');
                   Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                endif;
                break;
                
	case 'list':
                if($set!='0'):
                    $QueryObj_a= new attribute_set();
                    $attr_set = $QueryObj_a->getAttributeSet($set);
                    
                    $QueryObj_s= new attribute_super_set();
                    $super_set = $QueryObj_s->getAttributeSuperSet($attr_set->attr_super_set_id);
                    
                    if(!is_object($attr_set)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something went wrong.');
                        Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                    endif;
                    
                    $QueryObj= new attribute();
                    $QueryObj->listSetAttributes($set);
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                endif;
                break;
                
	case 'insert_super':
                /* create attribute*/
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $attribute= new attribute_super_set();    
                            $new_id = $attribute->saveAttributeSuperSet($_POST);

                            $admin_user->set_pass_msg('Attribute has been inserted successfully.');
                            Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                        endif;
                endif;
                break;
        
         case 'insert_set':
            
                if($super!='0'):
                    $QueryObj_s= new attribute_super_set();
                    $super_set = $QueryObj_s->getAttributeSuperSet($super);
                    
                    if(!is_object($super_set)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something went wrong.');
                        Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                    endif;
               
                    /* create attribute*/
                    if(isset($_POST['submit'])):
                        
                        /*server side validation*/
                            $validation=new user_validation();
                            $validation->add('name', 'req');
                            $validation->add('name', 'reg_words');

                            $valid= new valid();

                            if($valid->validate($_POST, $validation->get())):
                                $error=0;
                            else:
                                $error=1;/*set error*/
                                $error_obj->errorAddArray($valid->error);
                            endif;

                            if($error!='1'): /*if there is no error*/
                                $attribute= new attribute_set();    
                                $new_id = $attribute->saveAttributeSet($_POST);

                                $admin_user->set_pass_msg('Attribute Set has been inserted successfully.');
                                Redirect(make_admin_url('attribute', 'list_set', 'list_set','super='.$super));
                            endif;
                    endif;
                else:
                   $admin_user->set_error();
                   $admin_user->set_pass_msg('Something went wrong.');
                   Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                endif;
                break;
        
        case 'insert':
                if($set!='0'):
                    $QueryObj_a= new attribute_set();
                    $attr_set = $QueryObj_a->getAttributeSet($set);
                    
                    $QueryObj_s= new attribute_super_set();
                    $super_set = $QueryObj_s->getAttributeSuperSet($attr_set->attr_super_set_id);
                    
                    if(!is_object($attr_set)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something went wrong.');
                        Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                    endif;
               
                    /* create attribute*/
                    if(isset($_POST['submit'])):
                        
                        /*server side validation*/
                            $validation=new user_validation();
                            $validation->add('name', 'req');
                            $validation->add('name', 'reg_words');

                            $valid= new valid();

                            if($valid->validate($_POST, $validation->get())):
                                $error=0;
                            else:
                                $error=1;/*set error*/
                                $error_obj->errorAddArray($valid->error);
                            endif;

                            if($error!='1'): /*if there is no error*/
                                $attribute= new attribute();    
                                $new_id = $attribute->saveAttribute($_POST);

                                $admin_user->set_pass_msg('Attribute Set has been inserted successfully.');
                                Redirect(make_admin_url('attribute', 'list', 'list','set='.$set));
                            endif;
                    endif;
                else:
                   $admin_user->set_error();
                   $admin_user->set_pass_msg('Something went wrong.');
                   Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                endif;
                break;
                
        case 'delete_super':
                $content= new attribute_super_set(); 
                $content->id=$id;
                if($content->Delete()):
                    $admin_user->set_pass_msg('Attribute Super set has been deleted successfully.');
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Please delete the all attributes of attribute set first.');
                endif;
                        
                
                Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                break;

        case 'delete_set':
                $content= new attribute_set(); 
                $content->id=$set;
                if($content->Delete()):
                    $admin_user->set_pass_msg('Attribute Set has been deleted successfully.');
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Please delete the all attributes of attribute set first.');
                endif;

                Redirect(make_admin_url('attribute', 'list_set', 'list_set','super='.$super));
                break;
        
        case 'delete_attr':
                $content= new attribute(); 
                $content->id=$id;
                $content->Delete();
                        
                $admin_user->set_pass_msg('Attribute has been deleted successfully.');
                Redirect(make_admin_url('attribute', 'list', 'list','set='.$set));
                break;
            
        case 'update_super':
                /* update attribute */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new attribute_super_set();    
                            $QueryObj ->saveAttributeSuperSet($_POST);
                            $admin_user->set_pass_msg('Attribute has been updated successfully.');
                            Redirect(make_admin_url('attribute', 'update_super', 'update_super', 'id='.$id));
                        endif;
		endif;
		
                /* get attribute contents */
                $Query_obj= new attribute_super_set();
                $values=$Query_obj->getAttributeSuperSet($id);
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('attribute', 'list', 'list'));
                endif;
		break;
                
         case 'update_set':
                /* update attribute */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new attribute_set();    
                            $QueryObj ->saveAttributeSet($_POST);
                            $admin_user->set_pass_msg('Attribute Set has been updated successfully.');
                            Redirect(make_admin_url('attribute', 'update_set', 'update_set', 'set='.$set));
                        endif;
		endif;
		
                /* get attribute contents */
                $Query_obj = new attribute_set();
                $values = $Query_obj->getAttributeSet($set);
                
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('attribute', 'list_set', 'list_set','super='.$super));
                else:
                    $Query_obj1= new attribute_super_set();
                    $super_set=$Query_obj1->getAttributeSuperSet($values->attr_super_set_id);
                endif;
		break;
             
         case 'update':
                /* update attribute */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new attribute();    
                            $QueryObj ->saveAttribute($_POST);
                            
                            $admin_user->set_pass_msg('Attribute has been updated successfully.');
                            Redirect(make_admin_url('attribute', 'list', 'list', 'set='.$set));
                        endif;
		endif;
		
                /* get attribute contents */
                $Query_obj = new attribute();
                $values = $Query_obj->getAttribute($id);
                
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                else:
                    $Query_obj1= new attribute_set();
                    $attr_set=$Query_obj1->getAttributeSet($values->attr_set_id);
                    
                    $Query_obj2= new attribute_super_set();
                    $super_set=$Query_obj2->getAttributeSuperSet($attr_set->attr_super_set_id);
                endif;
		break;
                
         case 'update_attr_list':
                 if(is_var_set_in_post('submit_position')):
                    foreach ($_POST['position'] as $k=>$v):
                            $Query_obj= new attribute();
                            $Query_obj->updatePosition($v,$k);
                    endforeach;
                endif;
                
                if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Delete Selected'):
                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new attribute(); 
                                   $deleteObj->id=$k;
                                   $deleteObj->Delete();
                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('attribute', 'list', 'list','set='.$set));
                         endif;   
                    endif;
                endif;
                
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('attribute', 'list', 'list','set='.$set));
                break;
                
         case 'update2':
                 if(is_var_set_in_post('submit_position')):
                    foreach ($_POST['position'] as $k=>$v):
                            $Query_obj= new attribute_super_set();
                            $Query_obj->updatePosition($v,$k);
                    endforeach;
                endif;
                
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('attribute', 'list_super', 'list_super'));
                break;
    default:break;
endswitch;
?>