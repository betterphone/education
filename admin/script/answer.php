<?php

include_once(DIR_FS_SITE . 'include/functionClass/answerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');

$modName = 'answer';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
switch ($action):
    case 'list':
        $query = new answer();
        $answers = $query->list_answer($_GET['quest_id']);
        $query = new question();
        $question = $query->list_question($_GET['quest_id']);
        break;
    case 'insert':
        if (isset($_POST['submit'])) {
            $query = new answer();
            $query->saveAnswer($_POST);
            $admin_user->set_pass_msg('Inserted Successfully');
            Redirect(make_admin_url('answer', 'list', 'list&quest_id=' . $_GET['quest_id']));
        }
        break;
    case 'update':
        $query = new answer();
        $answer = $query->list_answer_using_answer_id($_GET['a_id']);
        if (isset($_POST['submit'])) {
            $query = new answer();
            $query->saveAnswer($_POST);
            $admin_user->set_pass_msg('Update Successfully');
            Redirect(make_admin_url('answer', 'list', 'list&quest_id=' . $_GET['quest_id']));
        }
        break;
    case 'delete':
        $query = new answer();
        $query->deleteAnswer($_GET['a_id']);
        $admin_user->set_pass_msg('Answer deleted successfully.');
        Redirect(make_admin_url('answer', 'list', 'list&quest_id=' . $_GET['quest_id']));
        break;
    default:break;
endswitch;
?>
