<?php
$parent = $_REQUEST["parent"];
$id = isset($_REQUEST["id"])?$_REQUEST["id"]:0;

include ('../../include/config/config.php');
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');

$Query = new category();
$category = $Query->getCategory($id);

$category_parent = '';
$current_category = '';
if(is_object($category)):
    $category_parent = $category->parent_id;
    $current_category = $category->id;
endif;

$data = array();

if ($parent == "#") {
    $QueryObj= new category();
    $categories = $QueryObj->listCategory('0',false,'array');
    foreach($categories as $kk=>$vv):
        $queryy = new category();
        $sub_cats = $queryy -> checkSubCategories($vv['id']);
            $data[] = array(
                            "id" => $vv['id'], 
                            "text" => $vv['name'], 
                            "icon" => "icon-folder-close",
                            "children" => $sub_cats, 
                            "type" => "root",
                            "state" => array(
                                'selected'=>($vv['id']==$category_parent)?true:false,
                                'disabled'=>($vv['id']==$current_category)?true:false
                            )
                    );
    endforeach;
} else {
    $QueryObj= new category();
    $categories = $QueryObj->listCategory($parent,false,'array');
    foreach($categories as $kk=>$vv):
        $queryy = new category();
        $sub_cats = $queryy -> checkSubCategories($vv['id']);
            $data[] = array(
                        "id" => $vv['id'], 
                        "text" => $vv['name'], 
                        "icon" => "icon-sitemap",
                        "children" => $sub_cats, 
                        "type" => "root",
                        "state" => array(
                            'selected'=>($vv['id']==$category_parent)?true:false,
                            'disabled'=>($vv['id']==$current_category)?true:false
                        )
                );
    endforeach;
}
header('Content-type: text/json');
header('Content-type: application/json');
echo json_encode($data);
?>