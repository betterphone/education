<?php
$parent = $_REQUEST["parent"];
$id = isset($_REQUEST["id"])?$_REQUEST["id"]:0;

include ('../../include/config/config.php');
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');

$Query = new category_product();
$product_cats = $Query->getProductCategories($id);

$cat_ids = array();
if(!empty($product_cats)):
    foreach($product_cats as $kk=>$vv):
        $cat_ids[$vv['category_id']] = $vv;
    endforeach;
endif;

$data = array();

if ($parent == "#") {
    $QueryObj= new category();
    $categories = $QueryObj->listCategory('0',false,'array');
    foreach($categories as $kk=>$vv):
        $queryy = new category();
        $sub_cats = $queryy -> checkSubCategories($vv['id']);

        $data[] = array(
			"id" => $vv['id'], 
			"text" => $vv['name'], 
			"icon" => "icon-folder-close",
			"children" => $sub_cats, 
			"type" => "root",
                        "state" => array(
                                'selected'=>(array_key_exists($vv['id'], $cat_ids))?true:false
                            )
		);
    endforeach;
} else {
    $QueryObj= new category();
    $categories = $QueryObj->listCategory($parent,false,'array');
    foreach($categories as $kk=>$vv):
        $queryy = new category();
        $sub_cats = $queryy -> checkSubCategories($vv['id']);
        $data[] = array(
                    "id" => $vv['id'], 
                    "text" => $vv['name'], 
                    "icon" => "icon-sitemap",
                    "children" => $sub_cats, 
                    "type" => "root",
                    "state" => array('selected'=>(array_key_exists($vv['id'], $cat_ids))?true:false)
            );
    endforeach;
}

$adsas = array (
          0 => 
          array (
            'text' => 'Same but with checkboxes',
            'children' => 
            array (
              0 => 
                  array (
                        'text' => 'initially selected',
                        'state' => 
                        array (
                          'selected' => true,
                        ),
                      ),
              1 => 
                  array (
                    'text' => 'custom icon',
                    'icon' => 'fa fa-warning icon-danger',
                  ),
              2 => 
                  array (
                    'text' => 'initially open',
                    'icon' => 'fa fa-folder icon-default',
                    'state' => 
                    array (
                      'opened' => true,
                    ),
                    'children' => 
                    array (
                      0 => 'Another node',
                    ),
                  ),
              3 => 
                  array (
                    'text' => 'custom icon',
                    'icon' => 'fa fa-warning icon-warning',
                  ),
            ),
          ),
          1 => array (
                    'text' => 'custom icon',
                    'icon' => 'icon-warning-sign',
                  ),
    );

$adssaas = '[{
                        "text": "Same but with checkboxes",
                        "children": [{
                            "text": "initially selected",
                            "state": {
                                "selected": true
                            }
                        }, {
                            "text": "custom icon",
                            "icon": "fa fa-warning icon-danger"
                        }, {
                            "text": "initially open",
                            "icon" : "fa fa-folder icon-default",
                            "state": {
                                "opened": true
                            },
                            "children": ["Another node"]
                        }, {
                            "text": "custom icon",
                            "icon": "fa fa-warning icon-warning"
                        }, {
                            "text": "disabled node",
                            "icon": "fa fa-check icon-success",
                            "state": {
                                "disabled": true
                            }
                        }]
                    },
                    "And wholerow selection"]'
        ;
header('Content-type: text/json');
header('Content-type: application/json');
echo json_encode($data);
?>