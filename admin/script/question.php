<?php

include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/roundClass.php');

$modName = 'question';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_POST['category_id']) ? $category_id = $_POST['category_id'] : $category_id = '';
/* Handle actions here. */

$query = new round();
$allLevels = $query->listRound();

switch ($action):
    case 'list':
        $QueryObj = new question();
        $questions = $QueryObj->listPages_using_cat_id($category_id);

        $query = new category;
        $categories = $query->categories();
        
        break;
    case 'update':
        $query = new category;
        $categories = $query->categories();
        if (isset($_POST['submit'])):

            $question = new question();
            $new_id = $question->savePage($_POST);
            $admin_user->set_pass_msg('The question has been inserted successfully.');
            Redirect(make_admin_url('question', 'list', 'list', 'id=' . $new_id));
        endif;
        /* get page contents */
        $question_obj = new question();
        $page_question = $question_obj->getPage($id);

        break;
    case 'insert':
        $query = new category;
        $categories = $query->categories();
        if (isset($_POST['submit'])):
            if ($_POST['question_type'])
                $question = new question();
            $new_id = $question->savePage($_POST);
            $admin_user->set_pass_msg('The question has been inserted successfully.');
            Redirect(make_admin_url('question', 'list', 'list', 'id=' . $new_id));
        endif;
        break;
    case 'delete':
        $query = new question();
        $query->deleteQuestion($id);
        $admin_user->set_pass_msg('The question has been deleted successfully.');
        Redirect(make_admin_url('question', 'list', 'list'));
        break;
    default:break;
endswitch;
?>
