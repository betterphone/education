<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Answer Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-text-width"></i>
                <a href="<?php echo make_admin_url('question', 'list', 'list'); ?>">List Question Pages</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Answer
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE ANSWER-->
<div class="row-fluid">
    <form class="form-horizontal"  method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Page</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Answer<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="answer"  value="<?php echo $answer->answer ?>" id="name" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Position<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="numner" name="position"  value="<?php echo $answer->position ?>" id="position" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Status<span class="required">*</span></label>
                        <div class="col-md-8">
                            Correct <input type="checkbox" name="is_correct"  value="1" id="is_correct"  <?php echo ($answer->is_correct == 1) ? 'checked' : '' ?> />
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" value="<?php echo $_GET['a_id'] ?>" tabindex="7" />
                           <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button>
                            <a href="<?php echo make_admin_url('answer', 'list', 'list&quest_id=' . $_GET['quest_id']); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
