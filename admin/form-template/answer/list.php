<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Answer Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List Answer Pages
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>




<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<!-- BEGIN PAGE ANSWER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Of Answer</div>

                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>

            <h1 style="font-size:20px;text-align:center;font-weight: bold !important;">
                <?php
                echo $question->name;
                ?></h1>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('answer', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">         
                        <thead>
                            <tr>
                                <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                <th>Answer</th>
                                <th>Correct</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($answers as $key => $answer) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?></td>
                                    <td><?php echo $answer['answer'] ?></td>
                                    <td><?php echo ($answer['is_correct'] == 0) ? 'Incorrect' : 'Correct' ?></td>
                                    <td><?php echo $answer['position'] ?></td>
                                    <td><a href="<?php echo make_admin_url('answer', 'update', 'update&quest_id=' . $_GET['quest_id'] . '&a_id=' . $answer['id']) ?>"title="click here to edit this record"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo make_admin_url('answer', 'delete', 'delete', '&quest_id=' . $_GET['quest_id'] . '&a_id=' . $answer['id']) ?>" onclick="return confirm('Are you sure? You are deleting this page.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a>

                                    </td>
                                </tr>


                            <?php } ?>
                        </tbody>

                    </table>
                </form>    
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="clearfix"></div>
