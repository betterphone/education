<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Answer Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-text-width"></i>
                 <a href="<?php echo make_admin_url('question', 'list', 'list');?>">List Questions</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                New Answer
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE ANSWER-->
<div class="row-fluid">
    <form class="form-horizontal" method="POST" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add Answer</div>                   
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Answer<span class="required">*</span></label>
                        
                        <div class="col-md-8">
                            <input type="text" name="answer"  value="" id="answer" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>
<div class="form-group">
                                        
                                        <label class="col-md-2 control-label border-left-2px" for="position">Position</label>
                                        <div class="col-md-2">
                                           <input type="number" name="position"  value="" id="position" class="form-control" />
                                        </div>
                                </div>
 <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Status<span class="required">*</span></label>
                        <div class="col-md-8">
                            Correct <input type="checkbox" name="is_correct" value="1" id="is_correct" />
                        </div>
                    </div>

                    <h4 class="form-section hedding_inner"></h4>

                    <div class="form-actions fluid">
                        <div class="offset2">
                            
                            <input type="hidden" name="quest_id" value="<?php echo $_GET['quest_id'] ?>" />
                            <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('answer', 'list', 'list&quest_id=' . $_GET['quest_id']); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
