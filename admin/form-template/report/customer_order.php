<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-group"></i> Order by Customers Report </h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             Customer Order
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Customer Order</div> 
                <div class="actions">
                    <div class="btn-group">
                       <?php if($user_id!='0') :?>
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','customer_order','customer_order','user_id='.$user_id.'&print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="well">
                <form id="validate1" action="<?php echo make_admin_url('report','customer_order','customer_order');?>" name="select_customer">
                    <input type="hidden" name="Page" value="report"/>
                    <input type="hidden" name="action" value="customer_order"/>
                    <input type="hidden" name="section" value="customer_order"/>
                    <div class="span3">
                        <div class="form-group">
                            <select name="user_id" id="user_id" class="form-control span8">
                                <option value=''>Select User</option>
                                <?php if(!empty($users)):?>
                                    <?php foreach($users as $kk=>$vv): ?>
                                        <option <?php echo ($vv['id']==$user_id)?"selected":"";?> value='<?php echo $vv['id'];?>'><?php echo $vv['firstname']." ".$vv['lastname'];?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                    </div>
                    <div class="span5">
                    <button type="submit" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover" id="sample_21">
                            <thead>
                                 <tr>
                                    <th>Order&nbsp;#</th>
                                    <th>Purchase Date</th>
                                    <th>Product&nbsp;Quantity</th>
                                    <th>Order&nbsp;Price(<?php echo CURRENCY_SYMBOL;?>)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($user_orders)):?>
                                    <?php foreach($user_orders as $kk=>$vv): ?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $vv['order_id'];?></td>
                                            <td><?php echo $vv['date_add'];?></td>
                                            <td><?php echo $vv['totalquantity'];?></td>
                                            <td><?php echo show_price($vv['grand_total']);?></td>
                                    </tr>
                                    <?php endforeach;?>
                                <?php else: ?>
                                    <tr class="odd gradeX">
                                            <td colspan="4">No record Found. <?php echo ($user_id=='0')?"Please select the user.":"";?></td>
                                    </tr>
                                <?php endif;?>
                            </tbody>
                        </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>