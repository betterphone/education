<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-group"></i> Manage Users</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List Users
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Users</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('user', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Username</th>
                                <th>Points</th>
                                <th>Add Prize</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $k = 1; ?>
                            <?php foreach ($admin as $ad) { ?>
                                <tr>
                                    <td>
                                        <?php
                                        echo $k++;
                                        ?>
                                    </td>
                                    <td><?php echo $ad['username'] ?></td>
                                    <td>
                                        <?php
                                        $query = new user_points();
                                        $all_points = $query->all_points($ad['id']);
                                        echo $all_points;
                                        ?>
                                    </td>
                                    <td>
                                        <?php if ($all_points > 0) { ?>
                                            <a href="<?php echo make_admin_url('prize', 'insert', 'insert&user_id=' . $ad['id']) ?>" class="btn blue btn-xs">Add</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
