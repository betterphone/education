<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-group"></i> Manage Users</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                         <i class="icon-group"></i>
                         <a href="<?php echo make_admin_url('user', 'list', 'list');?>">List Users</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit Users
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('user', 'update', 'update','id='.$id)?>" method="POST" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit user</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">   
                            <div class="form-group">
                            <label class="span2 control-label" for="username">Username<span class="required">*</span></label>
                            <div class="span8">
                                <input type="text" name="username"  value="<?php echo $admin->username ?>" id="name" class="span6 form-control validate[required,custom]" />                                          
                            </div>
                        </div>
                                <div class="form-group">
                        <label class="span2 control-label" for="username">Email<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="email"  value="<?php echo $admin->email ?>" id="email" class="span6 form-control validate[required,custom[email]]" />
                        </div>
                    </div>
                            
                               
                                <div class="form-group">
                                        <label class="span2 control-label" for="password">New Password</label>
                                        <div class="span8">
                                           <input type="password" name="password"  value="<?php echo $admin->password ?>" id="password" class="span6 form-control" />
                                        </div>
                                </div>	
                               	
                               
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input type="hidden" name="id" value="<?php echo $id;?>"/>
                                         <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button>
                                         <a href="<?php echo make_admin_url('user', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>
