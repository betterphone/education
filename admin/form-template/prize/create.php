<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Prizes</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>              
                <a href="<?php echo make_admin_url('prize', 'list', 'list'); ?>">List Prizes</a> 
                <i class="icon-angle-right"></i>                         
            </li>
            <li class="last">
                New Prizes
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <form class="form-horizontal" method="POST"  id="validation">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add New Prizes</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Prize Name<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="name" name="prize"  value="" id="name" class="form-control m-wrap validate[required]" required />                                          
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Prize Cost<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="name" name="cost"  value="" id="name" class="form-control m-wrap validate[required]" required />                                          
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Month<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="month"  value="" id="datepicker" class="form-control m-wrap validate[required] datepicker" required />                                          
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Active User<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select name="user_id" class="form-control m-wrap validate[required]" required>
                                        <option value="">Select User</option>
                                        <?php foreach ($users as $user) { ?>
                                            <?php
                                            $query = new user_points();
                                            $all_points = $query->all_points($user['id']);
                                            if ($all_points > 0) {
                                                ?>
                                                <option value="<?php echo $user['id'] ?>" <?php echo (isset($_GET['user_id']) && $_GET['user_id'] == $user['id'] ? 'selected' : '') ?>><?php echo $user['username'] . ' (' . $all_points ?>)</option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('prize', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>


<script>
    $('.datepicker').datepicker();
</script>