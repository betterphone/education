<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Prizes</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>      
            <li class="last">
                List Prizes
                <i class="icon-angle-right"></i>
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Prizes</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body">
                <form method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="datatable_without_sorting">
                        <thead> 
                            <tr>
                                <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                <th>Prize Title</th>
                                <th>Cost</th> 
                                <th>Month</th> 
                                <th>Username</th> 
                                <th>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($prizes as $key => $prize) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?></td>
                                    <td><?php echo $prize['prize'] ?></td>
                                    <td><?php echo $prize['cost'] ?></td>
                                    <td><?php echo $prize['month'] ?></td>
                                    <td>
                                        <?php
                                        $user_table = get_object('user', $prize['user_id']);
                                        echo $user_table->username;
                                        ?>
                                    </td>
                                    <td><a href="<?php echo make_admin_url('prize', 'update', 'update&id=' . $prize['id']) ?>"title="click here to edit this record" /><i class="icon-pencil"></i></a>                                             <a href="<?php echo make_admin_url('prize', 'delete', 'delete', 'id=' . $prize['id']) ?>" onclick="return confirm('Are you sure? You are deleting this prize.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>