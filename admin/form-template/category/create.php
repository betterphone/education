<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-sitemap"></i> Manage Categories</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-sitemap"></i>              
                               <a href="<?php echo make_admin_url('category', 'list', 'list');?>">List Categories</a> 
                        <i class="icon-angle-right"></i>                         
                    </li>
                    <li class="last">
                        New Category
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('category', 'insert', 'insert','pid='.$pid)?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Add New Category</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">
                           <div class="row-fluid">
                              <!-----<div class="<?php echo (count($categories))?"col-md-8":"col-md-11";?>">--->
                               <div class="col-md-12">
                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="name">Name<span class="required">*</span></label>
                                        <div class="col-md-8">
                                           <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                 

<!--                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="urlname">Urlname</label>
                                        <div class="col-md-8">
                                           <input type="text" name="urlname"  value="" id="urlname" class="form-control" />
                                           <div class="clearfix"></div>
                                           <span class="help-block">leave empty for automatic create urlname<br/>must be unique if entered</span>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="is_active">Status</label>
                                        <div class="col-md-6">
                                                <div class="radio-list">
                                                        <label class="radio-inline">
                                                        <input type="radio" name="is_active" id="is_active" value="1" checked /> Publish </label>
                                                        <label class="radio-inline">
                                                        <input type="radio" name="is_active" id="is_active" value="0" /> Draft </label>
                                                </div>
                                        </div>
                                </div>
								
                               <div class="form-group">
                                        
                                        <label class="col-md-2 control-label border-left-2px" for="position">Position</label>
                                        <div class="col-md-2">
                                           <input type="number" name="position"  value="" id="position" class="form-control" />
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="description">Description</label>
                                        <div class="col-md-8">
                                            <textarea id="description" class="form-control ckeditor" name="description" rows="6"></textarea>
                                    </div>
                                 </div> 
                                <h4 class="form-section hedding_inner">SEO Information</h4>
                                
                                <div class="clearfix"></div>
                                <hr/>
                                <div class="form-group">
                                       <label class="col-md-2 control-label" for="meta_title">Meta Title</label>
                                       <div class="col-md-8">
                                           <input type="text" name="meta_title" id="meta_title" rel="title" class="form-control meta_info" value=""> 
                                           <div class="clearfix"></div>
                                           <span class="help-block characterLeftTitle" id="characterLeft"></span>
                                       </div>  
                                </div>  
                               <div class="form-group">
                                        <label class="col-md-2 control-label" for="meta_keyword">Meta Keywords</label>
                                        <div class="col-md-8">
                                            <input type="text" name="meta_keyword" id="meta_keyword" class="form-control m-wrap" value=""/>
                                        </div>
                                </div>          
                               <div class="form-group">
                                        <label class="col-md-2 control-label" for="meta_desc">Meta Description</label>
                                        <div class="col-md-8">
                                            <textarea rows="3" class="form-control meta_info" rel="desc" id="meta_desc" name="meta_desc"></textarea>
                                            <div class="clearfix"></div>
                                            <span class="help-block characterLeftDesc" id="characterLeft"></span>
                                        </div>
                               </div>    -->
        
       
                        </div>
                        
                    </div>
                         <div class="form-actions fluid">
                            <div class="offset2">
                                 <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                 
                                 <input type='hidden' name='parent_id' value='<?=$pid?>'/>
								 <a href="<?php echo make_admin_url('category', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                            </div>
                         </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>