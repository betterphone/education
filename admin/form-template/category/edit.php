<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Categories</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>
                <a href="<?php echo make_admin_url('category', 'list', 'list'); ?>">List Categories</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Category
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('category', 'update', 'update', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Category</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">   
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Name<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="name"  value="<?php echo $values->name ?>" id="name" class="form-control m-wrap validate[required]" />
                                </div>
                            </div>
                           

<!--                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Urlname</label>
                                <div class="col-md-8">
                                    <input type="text" name="urlname" value="<?php echo url_rewrite::getUrlname('category', $values->id); ?>" id="name" class="form-control m-wrap" />
                                    <span class="help-block"><?php echo make_url('shop', 'id=' . $values->id); ?></span>
                                    <span class="help-block">must be unique, avoid using special chars.</span>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="is_active">Status</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active == 1) ? "checked" : ""; ?> value="1"/> Publish </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active == 0) ? "checked" : ""; ?> value="0"/> Draft </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label border-left-2px" for="position">Position</label>
                                <div class="col-md-2">
                                    <input type="number" name="position"  value="<?php echo $values->position ?>" id="position" class="form-control" />
                                </div>
                            </div>
                            
                           
                           
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Description</label>
                                <div class="col-md-8">
                                    <textarea id="description" class="form-control ckeditor m-wrap" name="description" rows="6">
                                        <?php echo html_entity_decode($values->description); ?>
                                    </textarea>
                                </div>
                            </div> 
                            <h4 class="form-section hedding_inner">SEO Information</h4>
                           
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="meta_title">Meta Title</label>
                                <div class="col-md-8">
                                    <input type="text" name="meta_title" id="meta_title" rel="title" class="form-control meta_info" value="<?php echo $values->meta_title; ?>"> 
                                    <div class="clearfix"></div>
                                    <span class="help-block characterLeftTitle" id="characterLeft"></span>
                                </div>  
                            </div>  
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="meta_keyword">Meta Keywords</label>
                                <div class="col-md-8">
                                    <input type="text" name="meta_keyword" id="meta_keyword" class="form-control m-wrap" value="<?php echo $values->meta_keyword ?>">
                                </div>
                            </div>          
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="meta_desc">Meta Description</label>
                                <div class="col-md-8">
                                    <textarea rows="3" class="form-control meta_info" rel="desc" id="meta_desc" name="meta_desc"><?php echo html_entity_decode($values->meta_desc) ?></textarea>
                                    <div class="clearfix"></div>
                                    <span class="help-block characterLeftDesc" id="characterLeft"></span>
                                </div>
                            </div> -->
                        </div>
                           
              
                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" id="category_id" value="<?php echo $values->id ?>" />
                            <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                             
                            <a href="<?php echo make_admin_url('category', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>