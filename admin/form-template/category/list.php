<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-sitemap"></i> Manage Categories</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>      
                        
                        <?php if($pid!=0):?>
                            <li class="last">
                                <i class="icon-sitemap"></i>
                                <a href="<?php echo make_admin_url('category','list','list');?>">List Categories</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <?php echo category_chain($pid);?>
                        <?php else: ?>
                            <li class="last">
                                List Categories
                                <i class="icon-angle-right"></i>
                            </li>
                        <?php endif;?>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Categories</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('category', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="datatable_without_sorting">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480">Sr.</th>
                                    <th>Name</th>
                                    <!--<th>Sub-categories</th>-->
                                    <!--<th>Status</th>-->
                                    <th class="hidden-480">Position</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if(!empty($categories)):
									$sr=1;	?>
                                <tbody>
                                    <?php foreach($categories as $kk=>$vv): ?>
                                    <tr>
										<td><?=$sr?>.</td>
										<td><a href='<?=make_admin_url('category','update','update','id='.$vv->id)?>'><?php echo $vv->name;?></a></td>
<!--										<td>
										<? 
										if($pid=='0'):
											if($vv->subcount=='0'):?>
											<a class='sub_category_add' href='<?=make_admin_url('category','insert','insert','pid='.$vv->id)?>'>Add Sub-Cat</a>
										<? else: ?>
											<a class='sub_category_add' href='<?=make_admin_url('category','list','list','pid='.$vv->id)?>'>Sub-Cat (<?php echo $vv->subcount;?>)</a>
										<? endif;
										else:
											echo "--";
										endif;	
										?>
										</td>-->
<!--										<td>
											<select module="category" name="is_active[<?php echo $vv->id;?>]" row_id="<?php echo $vv->id;?>" class="form-control notclickable status_change ">
												<option <?=($vv->is_active=='1')?'selected':''?> value="1">Publish</option>
												<option <?=($vv->is_active=='0')?'selected':''?>  value="0">Draft</option>
											</select>
										</td>-->
										<td><input module="category" type="text" name="position[<?php echo $vv->id;?>]" row_id="<?php echo $vv->id;?>" value="<?php echo $vv->position;?>" maxlength="5" size="5" class="notclickable position_update"></td>
										<td>
											<a id="do_action" href="<?=make_admin_url('category','update','update','id='.$vv->id)?>" title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a>
											<a href="<?=make_admin_url('category','delete','delete','id='.$vv->id)?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a>
										</td>										
									</tr>
                                    <?php $sr++;
										endforeach;?>
                                </tbody>
                                
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>
 <script src="assets/scripts/category_listing.js" type="text/javascript"></script>