<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Question Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-text-width"></i>
                <a href="<?php echo make_admin_url('question', 'list', 'list'); ?>">List Questions</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                New Question
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE QUESTION-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('question', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add Question</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Question<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>       

                    <div class="portlet-body form form-body">      
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="name">Category<span class="required">*</span></label>
                            <div class="col-md-8">
                                <select class="form-control" name="category_id">
                                    <?php foreach ($categories as $category) { ?>
                                        <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                        </div> 
                         <div class="form-group">
                            <label class="col-md-2 control-label" for="name">Question Type<span class="required">*</span></label>
                            <div class="col-md-8">
                                <select class="form-control" name="question_type">                                   
                                    <option>Single True</option>
                                    <option> Multiple True</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="round">Round / Level <span class="required">*</span></label>
                            <div class="col-md-8">
                                <select class="form-control" name="round">
                                   <? foreach($allLevels as $key=>$value):?>
											<option value="<?=$value['id']?>">Level <?=$value['id']?> &nbsp;&nbsp;&nbsp;&nbsp;(<?=$value['level']?>)</option>
									<? endforeach?>
                                </select> 
                            </div>
                        </div>
                       
                        <div class="form-actions fluid">
                            <div class="offset2">
                                <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                <a href="<?php echo make_admin_url('question', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

