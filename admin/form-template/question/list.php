<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Questions Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List Questions Pages
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<!-- BEGIN PAGE QUESTION-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Of Questions</div>

                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST">
                    <div class="row-fluid">
                        <div class="span4">
                            <select name="category_id" class="form-control">
                                <option value="">All</option>
                                <?php foreach ($categories as $category) { ?>
                                    <option value="<?php echo $category['id'] ?>" <?php echo (isset($_POST['category_id']) && $_POST['category_id'] == $category['id']) ? 'selected' : '' ?>><?php echo $category['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="span4">
                            <button type="submit" class="btn blue">Submit</button>
                        </div>
                    </div>
                </form>
                <br />
                <form action="<?php echo make_admin_url('question', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                <th>Question</th>
                                <th>Category</th>
                                <th>Question Type</th>
                                <th>Round</th>
                                <th>Manage Answer</th> 
                                <th>Action</th>                                        
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($questions as $key => $question) { ?>
                                <tr>
                                    <td>
                                        <?php echo $key+=1 ?>
                                    </td>
                                    <td>
                                        <?php echo $question['name'] ?>
                                    </td>
                                    <td>
                                        <?php
                                        $cwebc_category = get_object('category', $question['category_id']);
                                        echo ($cwebc_category->name);
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo $question['question_type'] ?>

                                    </td>

                                    <td> 
                                        Round <?php echo $question['round'] ?>

                                    </td>
                                    <td>
                                        <a href="<?php echo make_admin_url('answer', 'list', 'list&quest_id=' . $question['id']) ?>" /><button type="button" class="btn btn-mini">View Answer</button></a>
                                    </td>


                                    <td> <a href="<?php echo make_admin_url('question', 'update', 'update&id=' . $question['id']) ?>"title="click here to edit this record" /><i class="icon-pencil"></i></a>                                             <a href="<?php echo make_admin_url('question', 'delete', 'delete', 'id=' . $question['id']) ?>" onclick="return confirm('Are you sure? You are deleting this question.');"  title="click here to delete this  record" class="btn btn-xs default"><i class="icon-trash"></i></a>  </td>                                                                
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="clearfix"></div>
