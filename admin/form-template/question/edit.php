<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-text-width"></i> Manage Questions Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-text-width"></i>
                <a href="<?php echo make_admin_url('question', 'list', 'list'); ?>">List Question Pages</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Question
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('question', 'update', 'update', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
        <div class="span12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Question</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Question<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="name"  value="<?php echo $page_question->name ?>" id="name" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="portlet-body form form-body">      
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="name">Category<span class="required">*</span></label>
                            <div class="col-md-8">
                                <select class="form-control" name="category_id">
                                    <?php foreach ($categories as $category) { ?>
                                        <option value="<?php echo $category['id'] ?>" <?php echo ($category['id'] == $page_question->category_id) ? 'selected' : '' ?>><?php echo $category['name'] ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="question_type">Level<span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="question_type">

                                <option <?php echo $page_question->question_type == 'Single True' ? 'selected' : '' ?>>Single True</option>
                                <option <?php echo $page_question->question_type == 'Multiple True' ? 'selected' : '' ?>>Multiple True</option>
                            </select> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="round">Round / Level <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="round">
                                <? foreach ($allLevels as $key => $value): ?>
                                    <option value="<?= $value['id'] ?>" <?= ($page_question->round == $value['id']) ? 'selected' : '' ?>>Level <?= $value['id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;(<?= $value['level'] ?>)</option>
                                <? endforeach ?>
                            </select> 
                        </div>
                    </div>

                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" value="<?php echo $page_question->id ?>" tabindex="7" />
                            <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button>  
                            <a href="<?php echo make_admin_url('question', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<script>

    $(document).ready(function () {
        $('select[name="level"]').change();
    });

    $(document).on('change', 'select[name="level"]', function () {
        var el = $(this);
        var value = el.val();
        if (value == 'Intermediate') {
            $('select[name="round"]').html('<option <?php echo $page_question->round == '1' ? 'selected' : '' ?>>1</option><option <?php echo $page_question->round == '2' ? 'selected' : '' ?>>2</option><option <?php echo $page_question->round == '3' ? 'selected' : '' ?>>3</option>');
        }

        if (value == 'Challenging') {
            $('select[name="round"]').html('<option <?php echo $page_question->round == '1' ? 'selected' : '' ?>>1</option><option <?php echo $page_question->round == '2' ? 'selected' : '' ?>>2</option><option <?php echo $page_question->round == '3' ? 'selected' : '' ?>>3</option><option <?php echo $page_question->round == '4' ? 'selected' : '' ?>>4</option><option <?php echo $page_question->round == '5' ? 'selected' : '' ?>>5</option>');

        }
        if (value == 'Easy') {
            $('select[name="round"]').html('<option <?php echo $page_question->round == '1' ? 'selected' : '' ?>>1</option><option <?php echo $page_question->round == '2' ? 'selected' : '' ?>>2</option>');
        }
    });
</script>
