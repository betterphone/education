<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Round Settings</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>
                <a href="<?php echo make_admin_url('level', 'list', 'list');?>">List Rounds</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
               Round Settings
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<form class="form-horizontal" action="<?php echo make_admin_url('level', 'update', 'update', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Round</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                                                        <div class="form-group">
                                <label class="col-md-2 control-label" for="points">Points<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="name" name="points"  value="<?php echo $round->points ?>" id="points" class="form-control m-wrap validate[required]" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="total_time">Total Time<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="name" name="total_time"  value="<?php echo $round->total_time ?>" id="total_time" class="form-control m-wrap validate[required]" />
									<p class='hint'>Time to complete this level (In Secs)</p>
								</div>
                            </div>
							<div class="form-group">
                                <label class="col-md-2 control-label" for="level">Level<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <select name="level" class="form-control m-wrap validate[required]">
										<option value="EASY" <?=($round->level=='EASY')?'selected':''?>>Easy</option>
										<option value="INTERMEDIATE" <?=($round->level=='INTERMEDIATE')?'selected':''?>>Intermediate</option>
										<option value="CHALLENGING" <?=($round->level=='CHALLENGING')?'selected':''?>>Challenging</option>
									</select>
								</div>
                            </div>



                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" tabindex="7" />
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('level', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>