<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Round Settings</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>      
            <li class="last">
                Round Settings
                <i class="icon-angle-right"></i>
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Round Settings</div>
                
            </div>
            <div class="portlet-body">
                <form method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="datatable_without_sorting">
                        <thead> 
                            <tr>
                                <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                <th>Round</th>
                                <th class='text-center'>Level</th> 
								<th>Time</th> 
								<th>Points</th> 
                                <th class='text-right'>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($records as $key => $level) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?>.</td>
                                    <td>Round <?php echo $level['id'] ?></td>
                                    <td class='text-center'><?php echo $level['level'] ?></td>
									<td><?php echo $level['total_time'] ?></td>
									<td><?php echo $level['points'] ?></td>
                                    <td class='text-right'><a href="<?php echo make_admin_url('level', 'update', 'update', 'id=' . $level['id']) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> Edit</a> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>