<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-hand-down"></i> Manage Handling</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            Handling Charges
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Handling Charges</div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body form form-body">
                <div class="note note-info">
                        <p>
                            Additional handling for particular product can be set from products section. <br/>
                            Handling charges will be applied if free shipping not applicable.
                        </p>
                </div>
                <form class="form-horizontal" action="<?php echo make_admin_url('handling', 'update', 'update');?>" method="post" id="form_data" name="form_data" >	
                    <?php if($QueryObj->GetNumRows()!=0):?>
                        <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                 <div class="form-group">
                                        <label class="span2 control-label" for="name"><?php echo $object->name;?> Shipping (<?php echo CURRENCY_SYMBOL;?>)</label>
                                        <div class="span10">
                                           <input type="text" name="shipping[<?php echo $object->id;?>]"  value="<?php echo $object->handling_charges;?>" id="name" class="span1 form-control" />
                                        </div>
                                </div>
                        <?php $sr++; endwhile;?>
                    <?php endif;?>  
                    <div class="form-actions fluid">
                        <div class="offset2">
                             <input class="btn blue" type="submit" name="submit" value="Submit"/> 
                        </div>
                    </div>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>