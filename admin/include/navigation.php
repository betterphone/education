<!-- BEGIN HEADER -->
<div class="header navbar <?php echo (BACKEND_STICKY_NAV_BAR) ? 'navbar-fixed-top' : ''; ?> ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner container">

        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler hidden-phone">
        </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="<?php echo make_admin_url('home', 'list', 'list'); ?>" style="color:#fff;padding-left: 18px;">
            <?php echo SITE_NAME ?>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
 
            </ul>
        </div>
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown user">
                <a style="line-height:26px;" class="dropdown-toggle" href="<?php echo make_admin_url('setting', 'list', 'list'); ?>">
                    <i class="icon-cogs"></i>
                    <span class="username">Settings</span>
                </a>
            </li>
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a style="line-height:26px;" href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-user"></i>
                    <span class="username">Account</span>
                    <i class="icon-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    
                    <li>
                        <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=password'); ?>">
                            <i class="icon-lock"></i> Change Password
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo make_admin_url('logout'); ?>">
                            <i class="icon-key"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="container">
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                    <li class="start <?php echo ($Page == 'home') ? 'active' : '' ?>">
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                            <i class="icon-home"></i>
                            <span class="title">
                                Dashboard
                            </span>
                        </a>
                    </li>
                    <?php
                    $_Menus = array(
                        array(
                            'name' => 'Manage Question Bank',
                            'link' => 'javascript:;',
                            'icon' => 'icon-barcode',
                            'has_submenus' => TRUE,
							'pages' => array('question','category'),
                            'submenus' => array(
                                'question' => array(
                                    'name' => 'Questions',
                                    'link' => make_admin_url('question', 'list', 'list')
                                ),								
                                'category' => array(
                                    'name' => 'Category',
                                    'link' => make_admin_url('category', 'list', 'list')
                                )
                            )
                        ),
                        array(
                            'name' => 'Users Manager',
                            'link' => 'javascript:;',
                            'icon' => 'icon-user',
                            'has_submenus' => true,
							'pages' => array('user'),
                            'submenus' => array(
                                'user' => array(
                                    'name' => 'List Users',
                                    'link' => make_admin_url('user', 'list', 'list')
                                )
                            )
                        ),						
                        array(
                            'name' => 'Manage Prizes',
                            'link' => 'javascript:;',
                            'icon' => 'icon-bookmark',
                            'has_submenus' => TRUE,
							'pages' => array('prize'),
                            'submenus' => array(
                                'prize' => array(
                                    'name' => 'Prizes',
                                    'link' => make_admin_url('prize', 'list', 'list')
                                )
                            )
                        ),
                        array(
                            'name' => 'Content Pages',
                            'link' => 'javascript:;',
                            'icon' => 'icon-list-alt',
                            'has_submenus' => TRUE,
							'pages' => array('content'),
                            'submenus' => array(
                                'content' => array(
                                    'name' => 'List Pages',
                                    'link' => make_admin_url('content', 'list', 'list')
                                )
                            )
                        ),
                       
                        array(
                            'name' => 'Round / Level Settings',
                            'link' => 'javascript:;',
                            'icon' => 'icon-globe',
                            'has_submenus' => TRUE,
							'pages' => array('level'),
                            'submenus' => array(
                                'level' => array(
                                    'name' => 'Round / Level',
                                    'link' => make_admin_url('level', 'list', 'list')
                                )
                            )
                        ),
                        array(
                            'name' => 'Preferences',
                            'link' => 'javascript:;',
                            'icon' => 'icon-cogs',
                            'has_submenus' => TRUE,
							'pages' => array('setting'),							
                            'submenus' => array(
                                'setting' => array(
                                    'name' => 'Preferences',
                                    'link' => make_admin_url('setting', 'list', 'list', 'sname=general')
                                )
                            )
                        ),
                    );
                    make_admin_menus($_Menus, $Page, $action);
                    ?>
                    
                </ul>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
