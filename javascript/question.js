function startTimerNow(time) {
    $('.start_game_div').hide();
    $('.questionAnsDiv').show();

    var secondsLeft = time;
    var timerElement = $('.totol_time');
    timerElement.data('seconds-left', secondsLeft);
    timerElement.startTimer({
        onComplete: function (element) {
            stopTest();
        }
    });
}

function stopTest() {
    $('.questionAnsDiv').html('<div class="time_over"><img src="graphic/gameover.png"/><br /><br /><div class="btn btn btn-cta btn-cta-primary" onclick="window.location.reload(true);">Restart</div></div>');
}
function submitQuizForm() {
    $('.required_ans').hide();
    var valid = check_submission('.answer_id');
    var needAns = $('#question_id').attr('rel');

    if (valid != needAns) {
        $('.required_ans').html(needAns + " Answer Required..!");
        $('.required_ans').show();
    }
    else {
        $('.questionAnsDiv').addClass('loading');
        var url = "php/ajax.php?type=quiz";
        $.ajax({
            type: "POST",
            url: url,
            data: $('.qstAnsForm').serialize(),
            success: function (response) {
                $('#question_div').html(response);
                $('.questionAnsDiv').removeClass('loading');
            }
        });

    }
    return false;
}

function check_submission(classId) {
    var valid = '0';
    $(classId).each(function () {
        if ($(this).is(':checked')) {
            valid = parseInt(valid) + parseInt(1);
        }
    });
    return valid;
}