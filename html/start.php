<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="row" style="margin-top:-50px">
    <img src="http://themes.3rdwavemedia.com/devstudio/1.5/assets/images/background/promo-background-1.jpg" class="img img-responsive" />
</div>
<div class="container">
    <br />
    <div class="row">
        <section id="testimonials" class="testimonials section">
            <div class="container">
                <h2 class="title text-center">How it works</h2>
                <p class="intro text-center">Start Game: Read rules first</p>
                <div class="row">
                    <div class="item col-md-6 col-sm-6 col-xs-12">
                        <i>Total time available for each round is 10 seconds. You must continue playing until you get all the answers within allotted time. Once the 10 seconds expires, you can start again until all answers are correct. You have the option to begin again by clicking “Play again”. Same questions will be repeated. You must get all answers right in each round to get the points for that round, before proceeding to the next round.</i>                                       
                    </div>
                    <div class="item col-md-6 col-sm-6 col-xs-12">
                        <i>You can sign out at any time and resume later, your score for the last round completed will be saved. There are 15 rounds for each set of games, for which you will be able to obtain a maximum of 1500 points. The games are sequential so you will not be able to play any of the other games until you have completed a previous one. </i>                       
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <section id="who" class="who section">
            <div class="container text-center">
                <h2 class="title text-center">Want Bonus Points?</h2>
                <p class="intro text-center">
                    Obtain 10 bonus points for every share of the link to the website. 
                </p>
            </div>
            <?php if ($is_logged_in) { ?>
                <div class="row">
                    <!--                    <div class="col-md-4"></div>-->
                    <!--<div class="col-md-4">-->
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <a href="https://www.facebook.com/dialog/feed?app_id=<?php echo $app_id ?>&link=<?php echo $link ?>&picture=<?php echo $picture ?>&name=<?php echo $name ?>&caption=%20&description=<?php echo $description ?>&redirect_uri=<?php echo $redirect ?>" target="_blank"><img src="<?php echo DIR_WS_SITE ?>graphic/facebook.png" class="social_image"></a>
                        </div>
                        <!--                            <div class="col-md-4" style="text-align: center;">
                                                        <a href="http://twitter.com/share?text=<?php echo $description ?>;url=<?php echo $link ?>" target="_blank"><img src="<?php echo DIR_WS_SITE ?>graphic/twitter.png" class="social_image"></a>
                                                    </div>
                                                    <div class="col-md-4" style="text-align: center;">
                                                        <a href="https://pinterest.com/pin/create/button/?url=<?php echo $link ?>&media=<?php echo $picture ?>&description=<?php echo $description ?>" target="_blank">
                                                            <img src="<?php echo DIR_WS_SITE ?>graphic/pin.png" class="social_image" />                    
                                                        </a>
                                                    </div>-->
                    </div>
                    <!--</div>-->
                    <!--<div class="col-md-4"></div>-->
                </div>
            <?php } ?>
        </section>
    </div>
    <div class="row">
        <section>
            <center>
                <br />
                <br />
                <div class="row">
                    <?php if ($is_logged_in) { ?>
                        <a href="<?php echo make_url('home') ?>" class="btn btn btn-cta btn-cta-primary btn-big">Let’s Play Now</a>
                    <?php } else { ?>
                        <a href="<?php echo make_url('home') ?>" class="btn btn btn-cta btn-cta-primary btn-big">Login here to start share</a>
                    <?php } ?>
                </div>
            </center>
        </section>
    </div>
    <br />
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>