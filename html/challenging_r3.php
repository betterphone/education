<?php require 'header.inc.php' ?>
<div class="container">
    <h2 style="text-align:center">Level- Challenging</h2>
    <p style="text-align:center;font-weight:bold">Round- 3</p>
    <div class="col-md-6">
        <h1 style="font-size:16px;text-align:left;font-weight: bold;">
            <form method="POST">
                <?php
                $sr = '0';
                foreach ($question_using_category as $question) {
                    ?>
                    Q.<?php echo $question['name'] ?>  <br /><br /> 
                    <?php
                    $query = new answer();
                    $answers = $query->list_answer_front($question['id']);
                    ?>
                    <input type='hidden' name='result[<?php echo $sr; ?>][question_id]' value='<?php echo $question['id']; ?>'/>
                    <?php foreach ($answers as $answer) { ?>
                        <?php if ($question['question_type'] == 'Single True') { ?>
                            <input type="radio" name="result[<?php echo $sr; ?>][answer_id]" value="<?php echo $answer['id'] ?>" required > <?php echo $answer['answer'] ?>
                            <br /><br /> 
                        <?php } else { ?>


                            <input type="checkbox" name="result[<?php echo $sr; ?>][answer_id]" value="<?php echo $answer['id'] ?>"> <?php echo $answer['answer'] ?>
                            <br /><br /> 
                        <?php } ?>
                        <?php
                    }
                    ?><br /><br />  
                    <?php
                    $sr++;
                }
                ?>
                 <?php if (!$question_exist) { ?>
                    <input class="btn primary" type="submit" name="submit" value="Submit" tabindex="7" />
                <?php } ?>
            </form>
        </h1>   
    </div>
    <?php if (isset($_POST['submit'])) { ?>
        <div class="row">                
            <div class="col-md-6">                   
                <div class="panel panel-default">
                    <div class="panel-heading">Your Score:<span class="pull-right"><a href="<?php echo make_url('challenging_r4', 'category_id=' . $category_id) ?>" class="btn btn-xs btn-primary">Next Round</a></span></div>
                    <div class="panel-body">
                        <P style="font-weight:bold">Correct Questions:</p>
                        <?php
                        if ($as) {
                            foreach ($as as $key => $a) {
                                $correct_answer_selected = substr($key, 9);
                                $query = new question;
                                $correct_answer_selected = $query->list_question($correct_answer_selected);
                                echo $correct_answer_selected->name . '<br /><br />';
                            }
                        } else {
                            ?>
                            <div class="alert alert-info">no one is correct</div>
                        <?php }
                        ?>  
                    </div>
                </div>
            </div>
        </div> 
    <?php } ?>
</div>
</div>
<?php require 'footer.inc.php' ?>  



