<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?php display_message(1); ?>
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <?php if (!isset($_GET['m'])) { ?>
                        Choose Method
                    <?php } else if (isset($_GET['m']) && ($_GET['m'] == 'a')) { ?>
                        Security Question
                    <?php } else if (isset($_GET['m']) && ($_GET['m'] == 'e')) { ?>
                        Email Address
                    <?php } ?>
                </div>
                <div class="panel-body">
                    <center>
                        <?php if (!isset($_GET['m'])) { ?>
                            <a href="<?php echo make_url('forgot&m=a') ?>">Security Question</a>
                            <br />
                            <br />
                            <a href="<?php echo make_url('forgot&m=e') ?>">Email Address</a>
                        <?php } else { ?>
                            <a href="<?php echo make_url('forgot') ?>" class="">Change recovery method</a>
                        <?php } ?>
                    </center>
                    <br />
                    <?php if (isset($_GET['m']) && ($_GET['m'] == 'e')) { ?>
                        <div class="panel-body">
                            <form action="<?= make_url('forgot'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
                                <div class="form-group">
                                    <label class="text-dark" for="firstname">Email Address <span class="warning">*</span></label>
                                    <input id="emailInput" placeholder="Email address" class="form-control validate[required,custom[email]]" type="email" name="forgot">
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-cta btn-cta-secondary" name="submit" type="submit">
                                </div>
                                </fieldset>
                            </form>

                        </div>
                    <?php } else if (isset($_GET['m']) && ($_GET['m'] == 'a')) {
                        ?>
                        <form method="POST">
                            <label class="text-dark" for="firstname">Email Address <span class="warning">*</span></label>
                            <input type="text" name="email" class="form-control" placeholder="Enter email" required /><br />
                            <label class="text-dark" for="firstname">Select Question <span class="warning">*</span></label>
                            <select class="form-control question" name="question">
                                <option value="">Select question</option>
                                <?php foreach ($s_questions as $s_question) { ?>
                                    <option><?php echo $s_question ?></option>
                                <?php } ?>
                            </select>
                            <br />
                            <div class="answer_data" style="display : none">
                                <label class="text-dark" for="firstname">Write Answer <span class="warning">*</span></label>
                                <input type="text" name="answer" class="form-control" placeholder="Write answer" />
                                <br />
                                <button name="recovery_submit" class="btn btn-cta btn-cta-secondary">Submit</button>
                            </div>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>
<script>
    $(document).on('change', '.question', function () {
        var el = $(this);
        var el_value = el.val();
        if (el_value) {
            $('.answer_data').show();
        } else {
            $('.answer_data').hide();
        }
    });
</script>