<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <?php display_message(1); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Basic Information</div>
                <div class="panel-body">
                    <form method="POST" class="form-signin">
                        <label for="inputEmail" class="">
                            First Name
                            <span class="warning">*</span>
                        </label>
                        <input type="text" id="" name="firstname" value="<?php echo $user->firstname ?>" class="form-control" placeholder="First Name" required><br />
                        <label for="inputEmail" class="">
                            Last Name
                            <span class="warning">*</span>
                        </label>
                        <input type="text" id="" name="lastname" value="<?php echo $user->lastname ?>" class="form-control" placeholder="Last Name" required><br />
                        <label for="inputEmail" class="">
                            Email Address
                            <span class="warning">*</span>
                        </label>
                        <input type="email" name="username" id="inputEmail" value="<?php echo $user->username ?>"class="form-control" placeholder="Email address" required><br />

                        <label for="inputPhone" class="">
                            Phone Number
                            <span class="warning">*</span>
                        </label>
                        <input type="number" name="phone" id="inputphone" value="<?php echo $user->phone ?>" class="form-control" placeholder="Phone" required>
                        <input type="hidden" name="id" id="inputphone" value="<?php echo $user->id ?>" class="form-control" placeholder="Phone" required>
                        <br />
                        <button class="btn btn-cta btn-cta-secondary" name="submit" type="submit">Update</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Information</div>
                <div class="panel-body">
                    <form method="POST" class="form-signin">
                        <label for="inputEmail" class="">Sex</label>
                        <select name="sex" class="form-control">
                            <option value="">Select Sex</option>
                            <option value="male" <?php echo (isset($information->sex) && $information->sex == 'male') ? 'selected' : '' ?>>Male</option>
                            <option value="female" <?php echo (isset($information->sex) && $information->sex == 'female') ? 'selected' : '' ?>>Female</option>
                        </select>
                        <br />
                        <label for="inputEmail" class="">Dob</label>
                        <div class="row">
                            <div class="col-md-4">
                                <small>Date</small>
                                <select name="date" class="form-control">
                                    <option value="">Select Date</option>                                                  
                                    <?php for ($x = 1; $x <= 31; $x++) { ?>
                                        <option value="<?php echo $x ?>" <?php echo $date == $x ? 'selected' : '' ?>><?php echo $x ?></option>
                                    <?php } ?>
                                </select>
                            </div>    
                            <div class="col-md-4">
                                <small>Month</small>
                                <select name="month" class="form-control">
                                    <option value="">Select Month</option>                      
                                    <?php for ($x = 1; $x <= 12; $x++) { ?>
                                        <option value="<?php echo $x ?>" <?php echo $month == $x ? 'selected' : '' ?>><?php echo $x ?></option>
                                    <?php } ?>
                                </select>
                            </div>    
                            <div class="col-md-4">
                                <small>Year</small>
                                <select name="year" class="form-control">
                                    <option value="">Select Year</option> 
                                    <?php for ($x = 2016 - 18; $x >= 1970; $x--) { ?>
                                        <option value="<?php echo $x ?>" <?php echo $year == $x ? 'selected' : '' ?>><?php echo $x ?></option>

                                    <?php } ?>
                                </select>
                            </div>    
                        </div>
                        <br />
                        <label for="inputEmail" class="">Country</label>
                        <!--<div class="row">-->
                        <!--<div class="col-md-12">-->
                        <select name="location" class="form-control country">
                            <option value="">Select Country</option>
                            <?php foreach ($countries as $country) { ?>
                                <option value="<?php echo $country['id'] ?>" <?php echo (isset($information->location) && $information->location == $country['id']) ? 'selected' : '' ?>><?php echo $country['short_name'] ?></option>
                            <?php } ?>
                        </select>
                        <!--</div>-->
                        <!--                                                                <div class="col-md-6">
                                                                                            <select name="location" class="form-control">
                                                                                                <option value="">Select State</option>
                        
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>-->
                        <br />
                        <label name="question">Question</label>
                        <select name="question" class="form-control">
                            <option value="">Select question</option>
                            <?php foreach ($s_questions as $s_question) { ?>
                                <option value="<?php echo html_entity_decode($s_question) ?>" <?php echo isset($information->question) && $information->question == $s_question ? 'selected' : '' ?>><?php echo $s_question ?></option>
                            <?php } ?>
                        </select>
                        <br />
                        <div class="answer_data" <?php echo (isset($information->answer) && $information->answer != '') ? '' : 'style="display:none"' ?>>
                            <label for="inputEmail" class="">Answer</label>
                            <input type="text" name="answer" id="answer" value="<?php echo isset($information->answer) ? $information->answer : '' ?>"class="form-control answer" placeholder="Answer"  />
                        </div>
                        <br />
                        <button class="btn btn-cta btn-cta-secondary" name="information_submit" type="submit">UPDATE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN footer BAR -->
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>




<script>
    $(document).on('change', 'select[name="question"]', function () {
        $('.answer').val('');
        var el = $(this);
        var el_value = el.val();
        if (el_value) {
            $('.answer_data').show();
            $('.answer').attr('required', true);
        } else {
            $('.answer_data').hide();
            $('.answer').attr('required', false);
        }
    });

    $(document).on('click', 'button[name="information_submit"]', function (e) {
        var question = $('select[name="question"]').val();
        var answer = $('#answer').val();
        if (question && !answer) {
            e.preventDefault();
            $('#answer').css({
                'border': '1px solid #cc0000'
            });
        }
    });

//                                                    $(document).on('change', '.country', function () {
//                                                        var ajax = $('#root_path').attr('data-root');
//                                                        var el = $(this);
//                                                        var country_id = el.val();
//                                                        $.post(ajax, {action: 'get_state', country_id: country_id}, function (data) {
//                                                            alert(data);
//                                                        });
//                                                    });
</script>