<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $content->name ?>
        </div>
        <div class="panel-body">
            <?php echo html_entity_decode($content->page) ?>
        </div>
    </div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>