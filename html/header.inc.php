<?php
include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');

$query = new content;
$header_links = $query->contents();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Easy Access Education > Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE . 'css/style.css' ?>" />
        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!--<script src="<?php echo DIR_WS_SITE . 'javascript/js.js' ?>"></script>-->
    </head>
    <body>


        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo make_url('start')?>">Easy Access Education</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <?php if ($_SESSION['user_session']['user_id']) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo make_url('profile'); ?>">Profile</a></li> 
                                    <li><a href="<?php echo make_url('change_password'); ?>">Change password</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-header">Play Game</li>
                                    <li><a href="#">Resume Last Game</a></li>
                                    <li><a href="<?php echo make_url('home'); ?>">Start New</a></li>
                                    
                                </ul>
                            </li>
                            <li class="active"><a href="<?php echo make_url('logout') ?>">Logout <span class="sr-only">(current)</span></a></li>           
                        <?php } else { ?>
                            <li class="active"><a href="<?php echo make_url('signup') ?>">Register<span class="sr-only">(current)</span></a></li> 
                            <li class="active"><a href="<?php echo make_url('sign_in') ?>">Login<span class="sr-only">(current)</span></a></li> 
                            <?php } ?>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

