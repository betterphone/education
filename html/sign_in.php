<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php display_message(1); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Sign In</div>
            <div class="panel-body">
                <form method="POST" class="form-signin">
                    <label for="inputEmail">Email address <span class="warning">*</span></label>
                    <input type="email" id="inputEmail" name="username" class="form-control" placeholder="Email address" required /><br />
                    <label for="inputEmail">Password <span class="warning">*</span></label>
                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required />
                    <br />
                    <button class="btn btn-cta btn-cta-secondary" name="submit" type="submit">Sign in</button> 
                </form>
                <br />
                <a href="<?php echo make_url('forgot') ?>" >
                    Forgot Password!
                </a> 
            </div>
        </div>
    </div> 
</div>
<div class="col-md-4"></div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>