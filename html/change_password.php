<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php display_message(1); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Change Password</div>
            <div class="panel-body">
                <form action="<?= make_url('change_password'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
                    <div class="form-group">
                        <label class="text-dark" for="password">New Password  <span class="warning">*</span></label>
                        <input type="password" name="password"  id="password" class="form-control form-control--contact validate[required,minSize[6]]" placeholder="PASSWORD"/>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" for="password">Confirm Password  <span class="warning">*</span></label>
                        <input type="password" name="confirm_password"  id="confirm_password" class="form-control form-control--contact validate[required,minSize[6],equals[password]]" placeholder="CONFIRM PASSWORD" />
                    </div>  
                    <input type='hidden' name='id' value="<?php echo $user_id ?>">
                    <button class='btn btn-cta btn-cta-secondary submit-button' type='submit' name='submit'>Save</button>                
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>