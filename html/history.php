<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">
        <section id="who" class="who section">
            <div class="container text-center">
                <h2 class="title text-center">My Points</h2>
            </div>
        </section>
    </div>
    <div class="row">
        <form method="POST">
            <div class="col-md-2 col-xs-12 col-sm-12">
                <select class="form-control" name="cat_id">
                    <option value="">Show All</option>
                    <?php foreach ($categories as $category) { ?>
                        <option value="<?php echo $category['id'] ?>" <?php echo $cat_id == $category['id'] ? 'selected' : '' ?>><?php echo $category['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-2 col-xs-12 col-sm-12">
                <button type="submit" name="c_filter" class="btn btn-cta btn-cta-secondary" style="width:100%">Submit</button>
            </div>
        </form>
    </div>
    <br />
    <?php if (!$cat_id) { ?>
        <?php foreach ($categories as $category) { ?>
            <?php
            $query = new user_points();
            $my_points_easy = $query->my_points($user_id, 1, 5, $category['id']);
            $query = new user_points();
            $my_points_i = $query->my_points($user_id, 6, 10, $category['id']);
            $query = new user_points();
            $my_points_c = $query->my_points($user_id, 11, 15, $category['id']);
            ?>
            <div class="panel panel-default text-center">
                <div class="panel-heading"><?php echo $category['name'] ?></div>
                <div class="panel-body">
                    <?php if ($my_points_easy || $my_points_i || $my_points_c) { ?>
                        <div class="row">
                            <div class="col-md-3">
                                <h4>EASY</h4>
                                <p>
                                    <?php
                                    if ($my_points_easy) {
                                        echo $my_points_easy;
                                    } else {
                                        echo '-----';
                                    }
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-3">
                                <h4>INTERMEDIATE</h4>
                                <p>
                                    <?php
                                    if ($my_points_i) {
                                        echo $my_points_i;
                                    } else {
                                        echo '-----';
                                    }
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-3">
                                <h4>CHALLENGING</h4>
                                <p>
                                    <?php
                                    if ($my_points_c) {
                                        echo $my_points_c;
                                    } else {
                                        echo '-----';
                                    }
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-3">
                                <h4>POINTS</h4>
                                <p>
                                    <?php
                                    $total = $my_points_easy + $my_points_i + $my_points_c;
                                    echo $total;
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <?php if ($my_points_easy && (!$my_points_i || !$my_points_c)) { ?>
                                    <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-pause"></i> Resume</a>  
                                <?php } ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-play"></i> Play</a> 
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <?php
    if ($cat_id) {
        $query = new user_points();
        $my_points_easy = $query->my_points($user_id, 1, 5, $getCategory->id);
        $query = new user_points();
        $my_points_i = $query->my_points($user_id, 6, 10, $getCategory->id);
        $query = new user_points();
        $my_points_c = $query->my_points($user_id, 11, 15, $getCategory->id);
        ?>
        <div class="panel panel-default">
            <div class="panel-heading"><?php echo $getCategory->name ?></div>
            <div class="panel-body">
                <?php if ($my_points_easy || $my_points_i || $my_points_c) { ?>
                    <div class="row">
                        <div class="col-md-3">
                            <h4>EASY</h4>
                            <p>
                                <?php
                                if ($my_points_easy) {
                                    echo $my_points_easy;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>INTERMEDIATE</h4>
                            <p>
                                <?php
                                if ($my_points_i) {
                                    echo $my_points_i;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>CHALLENGING</h4>
                            <p>
                                <?php
                                if ($my_points_c) {
                                    echo $my_points_c;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>POINTS</h4>
                            <p>
                                <?php
                                $total = $my_points_easy + $my_points_i + $my_points_c;
                                echo $total;
                                ?>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <?php if ($my_points_easy && (!$my_points_i || !$my_points_c)) { ?>
                                <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-pause"></i> Resume</a>  
                            <?php } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-play"></i> Play</a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>