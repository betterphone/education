<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Change Password</div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <form class="validation" method="POST" id="form_validation">
                    <div id="request_form">	
                        <div class="form-group">
                            <label class="text-dark" for="password">New Password:<span class="warning">*</span></label>
                            <input type="password" name="password"  id="password" class="form-control form-control--contact validate[required,minSize[6]]" placeholder="PASSWORD"/>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="password">Confirm Password:<span class="warning">*</span></label>
                            <input type="password" name="confirm_password"  id="confirm_password" class="form-control form-control--contact validate[required,minSize[6],equals[password]]" placeholder="CONFIRM PASSWORD" />
                        </div>  
                        <button class="btn btn-cta btn-cta-secondary" type='submit' name='submit'>Save</button>                
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>