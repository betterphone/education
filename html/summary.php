<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">
        <section id="who" class="who section">
            <div class="container text-center">
                <h2 class="title text-center">My Prizes</h2>
            </div>
        </section>
    </div>
    <div class="panel panel-default text-center" style="border-color : #3b4452">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Total Points</h4>
                    <p>
                        <?php
                        echo $all_points ? $all_points : 0;
                        ?>
                    </p>
                </div>
                <div class="col-md-12">
                    <h4>Prizes</h4>
                    <?php if ($list_prizes) { ?>
                        <div class="row">
                            <?php foreach ($list_prizes as $list_prize) { ?>
                                <div class="col-md-4">
                                    <h4>Name</h4>
                                    <?php echo $list_prize['prize'] ?>
                                </div>
                                <div class="col-md-4">
                                    <h4>Prize</h4>
                                    <?php echo $list_prize['cost'] ?>
                                </div>
                                <div class="col-md-4">
                                    <h4>Date</h4>
                                    <?php echo $list_prize['month'] ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        No Prize
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Rewards</div>
        <div class="panel-body">
            Milestone – 2000 points, enter a chance to win 1 of 100 cash prizes of N1,000 each<br /><br />
            Milestone – 3000 points, enter a chance to win 1 of 50 cash prizes of N2,000 each<br /><br />
            Milestone – 5000 points, enter a chance to win 1 of 30 cash prizes of N5,000 each, and 1 draw to win a trip to Canada. Every 3 months, a winner is chosen in a draw from the winners in each month.Every players balance goes to zero on the last day of the month after winners’ draws are made and winners chosen<br /><br />

            You can win in each of the categories, for example, if you have 5000 points, you are eligible for the 5000 points draw, 3000 points draw, as well as the 2000, and you can win in all of the categories, the higher your points, the higher your odds of wining something.<br /><br />
            If you have 3,000 points, you are eligible for the 3000 points draw, as well as the 2000<br /><br />
            But if you have 2000 points, you are only eligible for the 2000 draw 
            So, the higher your points, the higher your chances of winning. <br /><br />

            <small>
                Note: A grand prize winner of the trip to Canada is ineligible for another grand prize, but is eligible to win other prizes over and over again.
            </small>
        </div>
    </div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>