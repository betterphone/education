<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <?php display_message(1); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Contact Us</div>
            <div class="panel-body">
                <form action="<?= make_url('contact_us'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail" class="">
                                    First Name
                                    <span class="warning">*</span>
                                </label>
                                <input id="fname" name="firstname" type="text" placeholder="First Name" class="form-control validate[required]" value="<?php echo isset($logged_in->firstname) ? $logged_in->firstname : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail" class="">
                                    Last Name
                                    <span class="warning">*</span>
                                </label>
                                <input id="lname" name="lastname" type="text" placeholder="Last Name" class="form-control validate[required]" value="<?php echo isset($logged_in->lastname) ? $logged_in->lastname : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail" class="">
                                    Email Address
                                    <span class="warning">*</span>
                                </label>
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control validate[required,custom[email]]" value="<?php echo isset($logged_in->username) ? $logged_in->username : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail" class="">
                                    Phone Number
                                    <span class="warning">*</span>
                                </label>
                                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control validate[required]" value="<?php echo isset($logged_in->phone) ? $logged_in->phone : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="">
                            Message
                            <span class="warning">*</span>
                        </label>
                        <textarea class="form-control validate[required]" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you soon." rows="7"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" name='submit' class="btn btn-cta btn-cta-secondary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>