<br /><br /><br />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <br />
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <ul>
                        <ul class="nav navbar-nav">
                            <?php foreach ($header_links as $header_link) { ?>
                                <li><a href="<?php echo make_url('content&id=' . $header_link['id']) ?>"><?php echo $header_link['name'] ?></a></li>
                                 
                            <?php } ?>   
                                <li><a href="<?php echo make_url('contact_us')?>">Contact Us</a></li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"></p>
        </div>
    </div>
</footer>
</body>
</html>