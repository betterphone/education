<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">
        <section id="who" class="who section">
            <div class="container text-center">
                <h2 class="title text-center">Dashboard</h2>
            </div>
        </section>
    </div>
    <div class="text-center">

        <a href="<?php echo make_url('home') ?>" class="btn btn btn-cta btn-cta-primary btn-big">Let’s Play Now</a>
    </div>

    <div class="panel panel-default text-center" style="border-color : #3b4452">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Total Points</h4>
                    <p>
                        <?php
                        echo $all_points ? $all_points : 0;
                        ?>
                    </p>
                </div>
                <div class="col-md-12">
                    <h4>Prizes</h4>
                    <?php if ($list_prizes) { ?>
                        <div class="row">
                            <?php foreach ($list_prizes as $list_prize) { ?>
                                <div class="col-md-4">
                                    <h4>Name</h4>
                                    <?php echo $list_prize['prize'] ?>
                                </div>
                                <div class="col-md-4">
                                    <h4>Prize</h4>
                                    <?php echo $list_prize['cost'] ?>
                                </div>
                                <div class="col-md-4">
                                    <h4>Date</h4>
                                    <?php echo $list_prize['month'] ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        No Prize
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <?php foreach ($categories as $category) { ?>
        <?php
        $query = new user_points();
        $my_points_easy = $query->my_points($user_id, 1, 5, $category['id']);
        $query = new user_points();
        $my_points_i = $query->my_points($user_id, 6, 10, $category['id']);
        $query = new user_points();
        $my_points_c = $query->my_points($user_id, 11, 15, $category['id']);
        ?>
        <div class="panel panel-default text-center">
            <div class="panel-heading"><?php echo $category['name'] ?></div>
            <div class="panel-body">
                <?php if ($my_points_easy || $my_points_i || $my_points_c) { ?>
                    <div class="row">
                        <div class="col-md-3">
                            <h4>EASY</h4>
                            <p>
                                <?php
                                if ($my_points_easy) {
                                    echo $my_points_easy;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>INTERMEDIATE</h4>
                            <p>
                                <?php
                                if ($my_points_i) {
                                    echo $my_points_i;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>CHALLENGING</h4>
                            <p>
                                <?php
                                if ($my_points_c) {
                                    echo $my_points_c;
                                } else {
                                    echo '-----';
                                }
                                ?>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h4>POINTS</h4>
                            <p>
                                <?php
                                $total = $my_points_easy + $my_points_i + $my_points_c;
                                echo $total;
                                ?>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <?php if ($my_points_easy && (!$my_points_i || !$my_points_c)) { ?>
                                <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-pause"></i> Resume</a>  
                            <?php } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <a href="<?php echo make_url('quiz&category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><i class="fa fa-play"></i> Play</a>  
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>