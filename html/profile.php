<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <html lang="en">
        <head>
            <meta http-equiv="content-type" content="text/html;charset=utf-8" />
            <meta http-equiv="cache-control" content="no-cache" />
            <?php echo head(isset($content) ? $content : ''); ?>


            <?php css($array = array('bootstrap.min', 'style')); ?>
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
                <?php js($array = array('jquery.min', 'bootstrap.min')); ?> 			
        </head>

        <body>

            <!-- BEGIN TOP BAR -->
            <?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
            <!-- END TOP BAR -->
            <div class="container">
                <div class="row">                     
                    <br /><br />
                    <div class="col-md-5  toppad  pull-right col-md-offset-3 ">     
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                        <?php display_message(1); ?>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hi...&nbsp;<?php echo $user->firstname . ' ' . $user->lastname ?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class=" col-md-12"> 
                                        <table class="table table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>First Name:</td>
                                                    <td><?php echo $user->firstname ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name:</td>
                                                    <td><?php echo $user->lastname ?></td>
                                                </tr>  
                                                <tr>
                                                    <td>Email</td>
                                                    <td><a href="mailto:info@support.com"><?php echo $user->username ?></a></td>
                                                </tr>
                                                <td>Phone Number</td>
                                                <td><?php echo $user->phone ?></td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                &nbsp;
                                <span class="pull-right">
                                    <a href="<?php echo make_url('profile_edit') ?>" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- BEGIN footer BAR -->
            <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>
            <!-- END footer BAR -->
        </body>
    </html>