<?php require 'header.inc.php' ?>
<div class="container">
    <div class="col-md-12"></div>
    <div class="col-md-12">
       <h3 class="form-signin-heading" style="text-align:center; font-weight:bold;">How To Play Game</h3><br />
       <div class="panel panel-default">
            <div class="panel-heading">
               Please Read Rules Before Starting The Game 
            </div>
            <div class="panel-body">
                <p>Total time available for each round is 10 seconds. You must continue playing until you get all the answers within allotted time. Once the 10 seconds expires, you can start again until all answers are correct. You have the option to begin again by clicking “Play again”. Same questions will be repeated. You must get all answers right in each round to get the points for that round, before proceeding to the next round. 
You can sign out at any time and resume later, your score for the last round completed will be saved. There are 15 rounds for each set of games, for which you will be able to obtain a maximum of 1500 points. The games are sequential so you will not be able to play any of the other games until you have completed a previous one.
</p>
            </div>
        </div> 
        
       <button> <a href="<?php echo make_url('home'); ?>" class="button" name="button" > Let’s Play Now</a></button>
      
    </div>
   
    
</div>
<?php require 'footer.inc.php' ?>