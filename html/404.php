<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<section class="section-404 section">
    <div class="container text-center">
        <h2 class="title-404 text-center">404</h2>
        <p class="intro text-center">Oops! Something went wrong. <br>The requested page or file may have been moved or deleted.</p>
        <div class="center-block">
            <a class="btn btn-cta btn-cta-secondary" href="<?php echo make_url('start') ?>">Back to Home</a>
        </div>
    </div>
</section>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>