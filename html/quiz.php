<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">
        <section id="who" class="who section">
            <div class="container text-center">
                <h2 class="title text-center">Round <?php echo $roundNo ?></h2>
                <p class="intro text-center">
                    <?php echo $getCategory->name ?> -> <?php echo $roundDetail->level ?> 
                </p>
                <div class="row text-center">
                    <div class="col-md-4">
                        <p>
                            Total Level Question : <?php echo $total_level_question_count ?>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p>
                            Total Round Question : <?php echo $total_round_question_count ?>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p>
                            Total Question : <?php echo $total_question ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <br />
    <div class='start_round_div custom_container'>
        <span class='totol_time'>
            <span class="hours">00:</span>
            <span class="minutes">00:</span>
            <span class="seconds">00</span>
        </span> 		
    </div>
    <div class="row custom_container">
        <?php if ($startQuestion != 'fill_profile') { ?>
            <?php
            if (!empty($startQuestion)) {
                $sr = '0';
                ?>
                <div class="panel">		
                    <div class="panel-heading start_game_div"> 
                        <a class='start_game btn btn btn-cta btn-cta-primary' onclick="startTimerNow(<?= $roundDetail->total_time ?>)">Get Started Now!</a>
                    </div>
                    <div class="questionAnsDiv"> 
                        <form id="form_validation" class="qstAnsForm push-down-15" method="POST" onsubmit="submitQuizForm();
                                        return false;">            
                            <div id='question_div'>	
                                <div class="panel panel-default">		
                                    <div class="panel-heading"> Q. <?php echo $startQuestion['name'] ?>  </div>
                                </div>
                                <p class='required_ans'>Answer Required</p>
                                <?php
                                $query = new answer();
                                $answers = $query->list_answer_front($startQuestion['id']);

                                $query = new answer();
                                $NeedAnswer = $query->total_answer_front_correct($startQuestion['id']);
                                ?>

                                <input type='hidden' id='question_id' rel="<?= $NeedAnswer ?>" name='question_id' value='<?php echo $startQuestion['id']; ?>'/>                                                <?php if ($answers) { ?>
                                    <?php foreach ($answers as $answer) { ?>
                                        <div class="form-group">	
                                            <? if ($startQuestion['question_type'] == 'Multiple True'): ?>
                                                <label><input class='answer_id' type="checkbox" name="answer[]" value="<?php echo $answer['id'] ?>"> <?php echo $answer['answer'] ?></label>
                                            <? else: ?>
                                                <label><input class='answer_id' type="radio" name="answer[0]" value="<?php echo $answer['id'] ?>"> <?php echo $answer['answer'] ?></label>
                                            <? endif; ?>
                                        </div> 
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="form-group">
                                        No answer
                                    </div>
                                <?php }
                                ?>
                                <input type='hidden' name='user_id' value='<?= $user_id ?>'/>
                                <?php if ($answers) { ?>
                                    <div class="footer-group text-center">			
                                        <input class="btn btn-cta btn-cta-secondary submit_answer" type="submit" name="submit_answer" value="Submit" tabindex="7" />
                                    </div>
                                <?php } ?>
                            </div>	
                        </form>			
                    </div>
                </div>	
            <?php } else { ?>
                <p class='text-center'>Sorry, No Question Found...!</p>
            <?php } ?>	
        <?php } else { ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="text-center">
                        <a href="<?php echo make_url('profile_edit&r=quiz&category_id=' . $category_id) ?>" class="btn btn-cta btn-cta-primary">Please complete your profile first</a>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>	
</div>
<?php /* if (isset($_POST['submit'])) { ?>
  <div class="row">
  <div class="col-md-6">
  <div class="panel panel-default">
  <div class="panel-heading">Your Score:<span class="pull-right"><a href="<?php echo make_url('easy_round2', 'category_id=' . $category_id) ?>" class="btn btn-xs btn-primary">Next Round</a></span></div>
  <div class="panel-body">
  <P style="font-weight:bold">Correct Questions:</p>
  <?php
  if ($as) {
  foreach ($as as $key => $a) {
  $correct_answer_selected = substr($key, 9);
  $query = new question;
  $correct_answer_selected = $query->list_question($correct_answer_selected);
  echo $correct_answer_selected->name . '<br /><br />';
  }
  } else {
  ?>
  <div class="alert alert-info">no one is correct</div>
  <?php }
  ?>
  </div>
  </div>
  </div>
  </div>
  <?php } */ ?>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>