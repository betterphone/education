<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <?php display_message(1); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Sign Up</div>
            <div class="panel-body">
                <form action="<?php make_url('signup'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="firstname">First Name <span class="warning">*</span></label>
                                <input type="text" name="firstname" id="firstname" class="form-control form-control--contact validate[required]" placeholder="FIRST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="lastname">Last Name <span class="warning">*</span></label>
                                <input type="text" name="lastname" id="lastname" class="form-control form-control--contact validate[required]" placeholder="LAST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="username">Email Address <span class="warning">*</span></label>
                                <input type="email" name="username"  id="username" class="form-control form-control--contact validate[required,custom[email]]" placeholder="EMAIL"/>
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="phone">Phone Number <span class="warning">*</span></label>
                                <input type="number" name="phone"  id="phone" class="form-control form-control--contact validate[required]" placeholder="PHONE" maxlength="12" size="10"  />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="password">Password <span class="warning">*</span></label>
                                <input type="password" name="password"  id="password" class="form-control form-control--contact validate[required,minSize[6]]" placeholder="PASSWORD"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-dark" for="password">Confirm Password <span class="warning">*</span></label>
                                <input type="password" name="confirm_password"  id="confirm_password" class="form-control form-control--contact validate[required,minSize[6],equals[password]]" placeholder="CONFIRM PASSWORD" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox"  id="terms" class="form-control--contact " required /> I Agree <a href='<?= make_url('content&id=6'); ?>' target="_blank">terms and conditions</a>.
                    </div>
                    <div class="clearfix"></div>
                    <button class="btn btn-cta btn-cta-secondary" name="submit" type="submit">Sign Up</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>