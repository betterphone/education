<?php include_once(DIR_FS_SITE_TEMPLATE . 'nav.php'); ?>
<div class="container">
    <div class="row">                
        <div class="col-md-12">
            <section id="who" class="who section">
                <div class="container text-center">
                    <h2 class="title text-center">Choose Category</h2>
                    <!--<p class="intro text-center">Choose Category</p>-->
                </div>
            </section>
            <div class="panel panel-default" style="border-color : #3b4452">
                <div class="panel-body">
                    <?php if ($categories) { ?>
                        <?php
                        $resume_count = 0;
                        ?>
                        <?php foreach ($categories as $category) { ?>
                            <?php
                            $QueryObj = new user_points();
                            $roundNo = $QueryObj->checkPrvRound($user_id, $category['id']);
                            if ($roundNo == 1) {
                                $play_or = 'Play';
                                $fa_icon = '<i class="fa fa-play"></i> ';
                            } else {
                                $QueryObj = new round();
                                $roundDetail = $QueryObj->getRound($roundNo);
                                $play_or = $roundDetail ? 'Resume' : 'Completed';
                                if ($play_or == 'Resume') {
                                    $fa_icon = '<i class="fa fa-pause"></i> ';
                                } else if ($play_or == 'Completed') {
                                    $fa_icon = '<i class="fa fa-check"></i> ';
                                }
                            }
                            ?>
                            <?php if (!$type) { ?>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?php echo $category['name'] ?>
                                        <span class="pull-right">
                                            <a href="<?php echo make_url('quiz', 'category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><?php echo $fa_icon . $play_or ?></a>
                                        </span>
                                    </div>                           
                                </div>
                            <?php } else { ?>
                                <?php if ($type == 'resume' && $play_or == 'Resume') { ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body"><?php echo $category['name'] ?><span class="pull-right"><a href="<?php echo make_url('quiz', 'category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><?php echo $fa_icon . $play_or ?></a></span></div>                           
                                    </div>
                                <?php } ?>
                                <?php if ($type == 'new' && $play_or == 'Play') { ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body"><?php echo $category['name'] ?><span class="pull-right"><a href="<?php echo make_url('quiz', 'category_id=' . $category['id']) ?>" class="btn btn-cta-xs btn-cta-primary"><?php echo $fa_icon . $play_or ?></a></span></div>                           
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="alert alert-info">No record found..!</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer.php'); ?>