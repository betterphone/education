<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

if (isset($_POST['submit'])) {

    $error = 0;
    if ($_POST['password'] != $_POST['confirm_password']):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Password and confirm password not matched..!');
    endif;

    $user_obj = new user();
    $user = $user_obj->checkFielExists('username', $_POST['username']);
    if (is_object($user)):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry This email already exist..!');
    endif;

    $user_obj = new user();
    $user = $user_obj->checkFielExists('phone', $_POST['phone']);
    if (is_object($user)):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry This phone no already exist..!');
    endif;

    if ($error == '0') {
        $query = new user;
        $user_id = $query->saveUser($_POST);
        $_SESSION['user_session']['user_id'] = $user_id;
        $_SESSION['user_session']['logged_in'] = 1;
        Redirect(make_url('start'));
    } else {
        Redirect(make_url('signup'));
    }
}

/* SEO information */
$content = add_metatags("Sigup");
?>
