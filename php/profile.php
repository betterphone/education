<?php
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}

$user_id = $_SESSION['user_session']['user_id'];

$query = new user;
$user = $query->list_user($user_id);

/* SEO information */  
$content=add_metatags("Profile");
?>