<?

include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/roundClass.php');

isset($_GET['type']) ? $type = $_GET['type'] : $type = '';

$user_id = $_SESSION['user_session']['user_id'];

if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}

$query = new category;
$categories = $query->categories();

$content = add_metatags("Home");
?>