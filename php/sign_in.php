<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

if ($_SESSION['user_session']['user_id']) {
    Redirect(make_url('rules'));
}
if (isset($_POST['submit'])) {
    $query = new user;
    $is_email_password_exist = $query->is_email_password_exist($_POST['username'], $_POST['password']);
    if ($is_email_password_exist) {
        $query = new user;
        $login_id = $query->list_user_using_email($_POST['username']);
        $_SESSION['user_session']['user_id'] = $login_id->id;
        $_SESSION['user_session']['logged_in'] = 1;
        Redirect(make_url('dashboard'));
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Email and Password is incorrect');
    }
}

$content = add_metatags("Sign In");
?>