<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
if (isset($_POST['submit'])) {
    $subject = SITE_NAME . ': Contact Request';
    $footer = 'Best Regards, ' . "<br/>" . SITE_NAME;

    $name = $_POST['firstname'] . ' ' . $_POST['lastname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = $_POST['message'];
    $footer = '<br /><br /><b>Contact form submitted on:</b><br />' . SITE_NAME . '<br /><br />This email is triggered by the ' . SITE_NAME . ', when a visitor submitted the contact form located on this page. The form was submitted at <b>' . date("h:m:i A") . '</b> on <b>' . date("d F, Y") . '</b> from <b>' . $_SERVER['REMOTE_ADDR'] . '</b> IP address.<br /><br /><br />&ldquo;Please do not reply to this email.&ldquo;<br /><br />Thank you -';

    send_email_by_cron('bharat@cwebconsultants.com', $message, $subject, $email, $name);
    $admin_user->set_pass_msg('Your Message has been sent');
    Redirect(make_url('contact_us'));
}

$content = add_metatags("Contact Us");
?>
