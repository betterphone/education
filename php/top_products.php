<?php
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/attributeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/brandClass.php');
include_once(DIR_FS_SITE.'include/functionClass/optionClass.php');

$p = isset($_GET['p'])?$_GET['p']:"1";
$id = isset($_GET['id'])?$_GET['id']:"0";
$sort_by = isset($_GET['sort_by'])?$_GET['sort_by']:'total_sale';
$sort_order = isset($_GET['sort_order'])?$_GET['sort_order']:'desc';

$smarty->assign_notnull("id",$id,true); 
$smarty->assign_notnull("sort_by",$sort_by,true); 
$smarty->assign_notnull("sort_order",$sort_order,true);

$parent_category = 0;
include_once(DIR_FS_SITE_PHP.'filter_get_parameters.php');

/*get all top selling  products */ 
//$shopObj = new product();
//$allproducts = $shopObj -> getTopSellingProducts(false,false,100,$sort_by,$sort_order,false,'shop');
//$smarty->assign_notnull("allproducts",$allproducts,true);

$shopObj = new product();
//$products_array = $shopObj->getAllActiveProducts($p, false, false, '', false, $sort_by, $sort_order, false,false,'shop',true);
$products_array = $shopObj->getAllFilterProducts($p,false,false,'',$categories,$brands,$attributes,$selcted_options,$sort_by,$sort_order,false,false,'shop',true);

$allproducts = $products_array['records'];
$total_records = $products_array['TotalRecords'];
$total_pages = $products_array['TotalPages'];
$RemainingRecords = $products_array['RemainingRecords'];

$smarty->assign_notnull("allproducts", $allproducts, true);
$smarty->assign_notnull("total_records", $total_records, true);
$smarty->assign_notnull("total_pages", $total_pages, true);
$smarty->assign_notnull("RemainingRecords", $RemainingRecords, true);
$smarty->assign_notnull("p", $p, true);


//////////////////////////////////// filter starts ////////////////////////////////////

// get all sub categories of coming category
$category_filter_array = array();
$catfilterObj = new category();
$category_filter_array = $catfilterObj->listAllCategoriesName($parent_category,$brands,$attributes,$selcted_options,false,false,true);
$smarty->assign_notnull("brand_categories", $category_filter_array, true);
$smarty->assign_notnull("category_filter_array",$category_filter_array,true);

// get all brands name under coming category
$brand_filter_array = array();
$brandfilterObj = new brand();
$brand_filter_array = $brandfilterObj->listAllBrands($categories, $attributes, true, 'object', 'name', $selcted_options, FALSE,FALSE,TRUE);
$smarty->assign_notnull("brand_filters", $brand_filter_array, true);
$smarty->assign_notnull("brand_filter_array",$brand_filter_array,true);
 
// get all option sets under coming category
$option_sets=array();
$option_sets=option::listOptions('array');
foreach($option_sets as $koption=> $option_set):
    $options=array();
    $options = product_option::listAllOptions($option_set->id,$categories,$brands,$attributes,$selcted_options,true,'object','pcount', false,false,true);

    if($options && count($options)>0)
       $option_set->options=$options;
    else
       unset($option_sets->$koption);
endforeach;
$smarty->assign_notnull("option_sets",$option_sets,true);
$smarty->assign_notnull("filter_option_sets",$option_sets,true);
 
// get main attribute set
$super_attribute_set=1;
$smarty->assign_notnull("super_attribute_set",$super_attribute_set,true);
 

// get all attribute sets with super set id 
$filter_att_sets = array();
$smarty->assign_notnull("filter_attribute_sets", $filter_att_sets, true);

////////////////////////// filter ends //////////////////////////////////////////////


/* SEO information */  
$content=add_metatags("Best Sellers");
createSeoNavigationLinks($content,$p,'1');

$smarty->renderLayout();
?>