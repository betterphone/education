<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$s_questions = array(
    'what is your father name',
    'what is your spouse name'
);

if (isset($_POST['submit'])) {
    $query = new user;
    $email_exist = $query->email_exist($_POST['forgot']);
    if ($email_exist) {
        $rand = rand();
        $query = new user;
        $query->update_field($email_exist->id, 'forgot_password_token', $rand);
        $subject = SITE_NAME . ': Forgot Password';
        $message = '<a href="' . make_url('recovery&token=' . $rand) . '">Click here to reset you password</a>';
        send_email_by_cron($_POST['forgot'], $message, $subject);
        $admin_user->set_pass_msg('Your password reset information send to your email account');
        Redirect(make_url('sign_in'));
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Email not found');
        Redirect(make_url('forgot&m=e&'));
    }
}

if (isset($_POST['recovery_submit'])) {
    $query = new user;
    $check_self = $query->list_user_using_email($_POST['email']);

    $query = new information();
    $is_correct = $query->question_answer_check($check_self->id, $_POST['question'], $_POST['answer']);
    if (!$is_correct) {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Incorrect');
        Redirect(make_url('forgot&m=a'));
    } else {
        $rand = rand();
        $query = new user;
        $query->update_field($check_self->id, 'forgot_password_token', $rand);
        Redirect(make_url('recovery&token=' . $rand));
    }
}

/* SEO information */
$content = add_metatags("Forgot Password");
?>
