<?php
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');
$reset = isset($_GET['reset'])?$_GET['reset']:'0';
$req_id = isset($_GET['req'])?$_GET['req']:'0';
$smarty->assign_notnull("REQ_ID",$req_id,true);

/*check if user logged in or not*/
if($logged_in):
    $login_session->logout_user();
endif;

if($req_id!='0'):
    $USER_ID = '0';
    $query = new user();
    $user_detail = $query -> getUserFromForgotPasswordCode($req_id);
    if($user_detail):
        $USER_ID = $user_detail->id;
    else:
        $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
        $login_session->set_error();
        $login_session->set_pass_msg();
        Redirect(make_url('forgot_password'));
    endif;
    $smarty->assign_notnull("USER_ID",$USER_ID,true);
endif;

if(isset($_POST['forgot_password']) && $_POST['forgot_password']=='submit'):
    if($_POST['email']!=''):
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)):
            $query = new user();
            $user_info = $query -> checkUserEmailExists($_POST['email']);
	    echo "<pre>";
	    print_r($user_info);
	    exit;		
            if($user_info):
                $code = substr(str_shuffle(str_repeat('0123456789',5)),0,12);
                $query11 = new user();
                if($query11->updateForgotPasswordVerifyCode($user_info->id, $code)):
                    /*send email*/
                    $email_array=array(
                        'firstname'=>$user_info->firstname,
                        'lastname'=>$user_info->lastname,
                        'confirmation_url'=>make_url('forgot_password','req='.$code)
                     );
                
                    /*** user email **/
                    send_db_email_content(EMAIL_FORGOT_PASSWORD, $user_info->username,$email_array);
                    $login_session->pass_msg[]=show(MSG_PASSWORD_RESET_MAIL_SUCCUSS,FALSE);
                else:
                    $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
                    $login_session->set_error();
                endif;  
            else:
                $login_session->pass_msg[]=show(MSG_ACCOUNT_WITH_EMAIL_NOT_EXIST,FALSE);
                $login_session->set_error();
            endif;
         else:
             $login_session->pass_msg[]=show(MSG_WRONG_EMAIL_FORMAT,FALSE);
             $login_session->set_error();
         endif;
    else:
        $login_session->pass_msg[]=show(FILL_REQUIRED_FIELDS,FALSE);
        $login_session->set_error();
    endif;
    $login_session->set_pass_msg();
    Redirect(make_url('forgot_password'));
endif;

/*reset password*/
if(isset($_POST['reset_password']) && $_POST['reset_password']=='submit'):

     if($_POST['password']!='' && $_POST['confirm_password']!=''):
          if($_POST['password']==$_POST['confirm_password']):
              $QueryObj = new user();
              $QueryObj -> Data['id'] = $_POST['uid'];
              $QueryObj -> Data['forgot_password_token'] = '';
              $QueryObj -> Data['password'] = md5($_POST['password']);
              if($QueryObj -> Update()):
                  $login_session->pass_msg[]=show(MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS,false);
                  $login_session->set_pass_msg();
                  Redirect(make_url('login'));
              else:
                  $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
                  $login_session->set_error();
              endif;    
          else: 
              $login_session->pass_msg[]=show(MSG_REGISTER_PASSWORDS_NOT_SAME,FALSE);
              $login_session->set_error();
          endif;    
     else:

        $login_session->pass_msg[]=show(FILL_REQUIRED_FIELDS,FALSE);
        $login_session->set_error();

    endif;
    $login_session->set_pass_msg();
    Redirect(make_url('forgot_password','req='.$_POST['code']));
endif;
/* SEO information */  
$content=add_metatags("Forgot Password");
$smarty->renderLayout();
?>
