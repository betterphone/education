<?php

include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/prizeClass.php');

isset($_GET['cat_id']) ? $cat_id = $_GET['cat_id'] : $cat_id = '';

if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}

$user_id = $_SESSION['user_session']['user_id'];

$query = new category();
$categories = $query->categories();

$query = new category();
$getCategory = $query->getCategory($cat_id);

$query = new user_points();
$all_points = $query->all_points($user_id);


$query = new prize();
$list_prizes = $query->getPrize_user_id($user_id);

$content = add_metatags("Profile");
?>