<?php
include_once(DIR_FS_SITE.'include/functionClass/stateClass.php');
include_once(DIR_FS_SITE.'include/functionClass/cityClass.php');
include_once(DIR_FS_SITE.'include/functionClass/markupClass.php');
include_once(DIR_FS_SITE.'include/functionClass/patientClass.php');
include_once(DIR_FS_SITE.'include/functionClass/formulaClass.php');	
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/newsletterClass.php');

$section = isset($_GET['section'])?$_GET['section']:"dashboard";
$redirect = isset($_GET['redirect'])?$_GET['redirect']:"account";
$resend = isset($_GET['resend'])?$_GET['resend']:"";
$id = isset($_GET['id'])?$_GET['id']:"0";
$p = isset($_GET['p'])?$_GET['p']:"1";

/*if($id!='0'):
    $query_check_address = new user_address();
    if(!$query_check_address->addressBelongsToUser($user_id,$id)):
        Redirect(make_url('account','section=address'));
    endif;
endif;*/

$queryUser = new user();
$user_details = $queryUser ->getUser($user_id);
                        
$smarty->assign_notnull("redirect",$redirect,TRUE);
$smarty->assign_notnull("section",$section,TRUE);
$smarty->assign_notnull("id",$id,TRUE);
if($logged_in):
    $user_check = new user();
    if($user_check -> checkUser($user_id)):
        /* change password */
        if(isset($_POST['change_password'])):
            if($_POST['password']!='' && $_POST['confirm_password']!=''):
                if($_POST['password'] == $_POST['confirm_password']):
                    $new_password = md5($_POST['password']);
                    $ChangeObj = new user();
                    if($ChangeObj->changePassword($user_id,$new_password)):
                        /*send email*/
                        $email_array=array(
                            'USER'=>$user_details->firstname.' '.$user_details->lastname,
                            'USERID'=>$user_details->username,
                            'IP_ADDRESS'=>$_SERVER['REMOTE_ADDR'],
                            'PASSWORD_CHANGE_DATE_TIME'=>date("d-m-Y H:i:s"),
                            'NEWPASSWORD'=>$_POST['password']
                         );
                        /*** user email **/
                        send_db_email_content(EMAIL_PASSWORD_CHANGE_USER, $user_details->username,$email_array);
                        
                        $login_session->pass_msg[]=show(MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS,false);
                    else:
                        $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
                    endif;
                else:
                    $login_session->pass_msg[]=show(MSG_REGISTER_PASSWORDS_NOT_SAME,false);
                endif;
            else:
                $login_session->pass_msg[]=show(FILL_REQUIRED_FIELDS,false);
            endif;
            $login_session->set_pass_msg(); 
            Redirect(make_url('account','section=password'));
        endif;
        /* change password */
        
        /* edit account */
        if(isset($_POST['edit_account']) && $_POST['edit_account']!=''):

            $validation=new user_validation();

            $validation->add('gender', 'req');

            $validation->add('firstname', 'req');
            $validation->add('firstname', 'reg_words');

            $validation->add('lastname', 'req');
            $validation->add('lastname', 'reg_words');
            
            $validation->add('gender', 'req');
            $validation->add('gender', 'reg_words');
            
            $validation->add('birthdate', 'req');
            
            $valid= new valid();
            
            $error=0;
            if($valid->validate($_POST, $validation->get())):
                $error=0;
            else:
                $error=1;/*set error*/
                $error_obj->errorAddArray($valid->error);
            endif;

            if($error!='1'): /*if there is no error*/
                $_POST['id'] = $user_id;
                $userUpdateQuery = new user();
                $userUpdateQuery -> updateUserDetail($_POST);
                
                $login_session->pass_msg[]=show(MSG_ACCOUNT_UPDATE_SUCCESS,false);
                $login_session->set_pass_msg(); 
                Redirect(make_url('account','section=info'));
            endif;
        endif;
        /* edit account */
        
        /* add new user address */
        if(isset($_POST['add_address']) && $_POST['add_address']=='add'):

            $validation=new user_validation();

            $validation->add('address_name', 'req');
            $validation->add('address_name', 'reg_words');

            $validation->add('firstname', 'req');
            $validation->add('firstname', 'reg_words');

            $validation->add('lastname', 'req');
            $validation->add('lastname', 'reg_words');
            
            $validation->add('address1', 'req');
            
            $validation->add('zip_code', 'req');
            $validation->add('zip_code', 'reg_words');
  
            $validation->add('city', 'req');
            $validation->add('city', 'reg_words');
            
            $validation->add('state_id', 'req');
            $validation->add('state_id', 'reg_words');

            $validation->add('country_id', 'req');
            $validation->add('country_id', 'reg_words');
            
            $validation->add('phone', 'req');
            
            $valid= new valid();
            
            $error=0;
            if($valid->validate($_POST, $validation->get())):
                $error=0;
            else:
                $error=1;/*set error*/
                $error_obj->errorAddArray($valid->error);
            endif;

            if($error!='1'): /*if there is no error*/
                if($redirect=='cart'):
                    $_POST['cart_id'] = cart::getCartId();
                endif;
                $_POST['user_id'] = $user_id;
                $_POST['email'] = user::getUserEmail($user_id);

                $addressAddQuery = new user_address();
                if($addressAddQuery -> saveUserAddress($_POST)):
                    $login_session->pass_msg[]=show(MSG_ADDRESSBOOK_UPDATED,false);
                    $login_session->set_pass_msg(); 
                    if($redirect=='checkout'):
                        Redirect(make_url('checkout'));
                    else:
                        Redirect(make_url('account','section=address'));
                    endif;
                else:
                    $login_session->pass_msg[]=show(MSG_ADDRESS_ADD_UNSUCCESS,false);
                    $login_session->set_pass_msg(); 
                    Redirect(make_url('account','section=address'));
                endif;
            endif;
        endif;
        /* add new user account */
        /* set default */
        if($section=='setdefault'):
            if($id!='0'):
                $Query1 = new user_address();
                if($Query1 -> setDefaultAddress($user_id,$id)):
                    $login_session->pass_msg[]=show(MSG_ACCOUNT_UPDATE_SUCCESS,false);
                else:
                    $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
                endif;
            endif;
            $login_session->set_pass_msg(); 
            Redirect(make_url('account','section=address'));
        endif;
        /* set default */
        
        /* delete address */
        if($section=='deladdress'):
            if($id!='0'):
                $Query1 = new user_address();
                if($Query1 -> deleteAddress($id)):
                    $login_session->pass_msg[]=show(MSG_ACCOUNT_UPDATE_SUCCESS,false);
                else:
                    $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
                endif;
            endif;
            $login_session->set_pass_msg(); 
            Redirect(make_url('account','section=address'));
        endif;
        /* delete address */
        
        /* subscribe for newsletter */
        if($section=='subscribe'):
            $errorsubscribe = '1';
            if(newsletter::checkNewsletterAdded($user_details->username)):
                $QueryObjns = new newsletter();
                if($QueryObjns -> setNewsletterActive($user_details->username)):
                    $errorsubscribe = '0';
                endif;
            else:
                $Query1 = new newsletter();
                if($Query1 -> addNewsletter($user_details->username,$user_details->firstname." ".$user_details->lastname,$user_id)):
                    $errorsubscribe = '0';
                endif;
            endif;

            if($errorsubscribe=='0'):
                $email_array=array(
                    'USER'=>$user_details->firstname.' '.$user_details->lastname,
                    'DATE_TIME'=>date("d-m-Y H:i:s")
                 );
                /*** user email **/
                send_db_email_content(EMAIL_NEWSLETTER_SUBSCRIBE_USER, $user_details->username,$email_array);
                
                $login_session->pass_msg[]=show(MSG_ACCOUNT_UPDATE_SUCCESS,false);
            else:
                $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
            endif;
            Redirect(make_url('account','section=newsletter'));
        endif;
        /* subscribe for newsletter */
        
        /* unsubscribe for newsletter */
        if($section=='unsubscribe'):
            $Query1 = new newsletter();
            if($Query1 -> setNewsletterInactive($user_details->username)):
                $email_array=array(
                    'USER'=>$user_details->firstname.' '.$user_details->lastname,
                    'DATE_TIME'=>date("d-m-Y H:i:s")
                 );
                /*** user email **/
                send_db_email_content(EMAIL_NEWSLETTER_UNSUBSCRIBE_USER, $user_details->username,$email_array);
                
                $login_session->pass_msg[]=show(MSG_ACCOUNT_UPDATE_SUCCESS,false);
            else:
                $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,false);
            endif;
            Redirect(make_url('account','section=newsletter'));
        endif;
        /* unsubscribe for newsletter */

        /* resend email confirmation email */
        if($resend=='confirmation'):
            /*send email*/
            $email_array=array(
                'firstname'=>$user_details->firstname,
                'lastname'=>$user_details->lastname,
                'email'=>$user_details->username,
                'confirmation_url'=> make_url('verify_account','key='.$user_details->email_confirmation_token),
                'login_url' => make_url('login')
             );

            /*** user email **/
            if(send_db_email_content(EMAIL_RESEND_EMAIL_CONFIRMATION, $user_details->username,$email_array)):
                $login_session->pass_msg[]=show(MSG_EMAIL_CONFIRMATION_RESEND_EMAIL_SUCCESS,false);
            else:
                $login_session->pass_msg[]=show(MSG_EMAIL_CONFIRMATION_RESEND_EMAIL_FAIL,false);
            endif;
            $login_session->set_pass_msg(); 
            Redirect(make_url('account','section='.$section));
        endif;
        /* resend email confirmation email */

        /* update user email*/
        if(isset($_POST['update_username'])):
            if(filter_var($_POST['username'], FILTER_VALIDATE_EMAIL)):
                if($_POST['username'] != $_SESSION['user_session']['username']):
                    $user_obj = new user();
                    $user = $user_obj->checkUserEmailExists($_POST['username']);
                    if(!$user):
                        $verify_no='';
                        for ($i = 0; $i < 5; $i++) {
                            $verify_no .= chr(rand(48, 57));
                            $verify_no .= chr(rand(97, 122));
                        }
                        $_POST['email_confirmation_token'] = $verify_no;
                        $_POST['is_email_verified'] = '0';
                        $_POST['id'] = $_SESSION['user_session']['user_id'];
                        
                        $update_obj = new user();
                        if($update_obj->updateUserDetail($_POST)):
                            
                            $_SESSION['user_session']['username'] = $_POST['username'];
                        
                            /*send email*/
                            $email_array=array(
                                'firstname'=>$user->firstname,
                                'lastname'=>$user->lastname,
                                'email'=>$_POST['username'],
                                'confirmation_url'=> make_url('verify_account','key='.$verify_no),
                                'login_url' => make_url('login')
                             );

                            /*** user email **/
                            send_db_email_content(EMAIL_USERNAME_UPDATE, $_POST['username'],$email_array);
                            
                            $login_session->pass_msg[]=show(MSG_EMAIL_UPDATE_SUCCESS,FALSE);
                            $login_session->set_pass_msg();
                            Redirect(make_url('account','section=username'));
                        endif;
                    else:
                        $login_session->set_error();
                        $login_session->pass_msg[]=show(MSG_REGISTER_EMAIL_ALREADY_EXIST,FALSE);
                    endif;
                else:
                    $login_session->set_error();
                    $login_session->pass_msg[]=show(MSG_CURRNT_UPDATE_CAN_NOT_SAME,FALSE);  
                endif;
            else:
                $login_session->set_error();
                $login_session->pass_msg[]=show(MSG_WRONG_EMAIL_FORMAT,FALSE);
            endif;  
            $login_session->set_pass_msg();    
            Redirect(make_url('account','section=username'));
        endif;
        /* update user email*/ 
                
        /* get user info */
        $Query = new user();
        $user = $Query->getUser($user_id);
	$country_id=$user->country_id;
	$state_id=$user->state_id;	
	$country = new country();
	$all_countries = $country->Country_records();
	$smarty->assign_notnull("ALL_COUNTRY",$all_countries,true);
	$state = new state();
	$all_state = $state->getStateNameByCountry($country_id);
	$smarty->assign_notnull("ALL_STATE",$all_state,true);	
	$city = new city();
	$all_city = $city->getCityNameByState($state_id);	
	$smarty->assign_notnull("ALL_CITY",$all_city,true);
        $smarty->assign_notnull("user",$user,true);
		
	/* get markup detail *****/
	$markup = new markup();
	$all_markup = $markup->getMarkupByUser($user_id);
	$smarty->assign_notnull("ALL_MARKUP",$all_markup,true);
	
	
        /* get user default address */
        $Query_default = new user_address();
        $default_address = $Query_default->getUserDefaultAddress($user_id);
        $smarty->assign_notnull("default_address",$default_address,true);
        
        /* get user all address */
        $Query_address = new user_address();
        $user_addresses = $Query_address->getUserAddresses($user_id);
        $smarty->assign_notnull("user_addresses",$user_addresses,true);

	/* get user markup */
        
        /* get user orders */
        $Query_orders = new order();
        $user_orders_array = $Query_orders->getUserOrdersWithPagination($user_id,$p);
        
        $user_orders=$user_orders_array['order'];
        $total_records=$user_orders_array['TotalRecords'];
        $total_pages=$user_orders_array['TotalPages'];

        $smarty->assign_notnull("user_orders",$user_orders,true);
        $smarty->assign_notnull("P",$p,true);
        $smarty->assign_notnull("MAX_RECORDS",ORDER_PAGE_SIZE_FRONT,true);
        $smarty->assign_notnull("total_records",$total_records,true);
        $smarty->assign_notnull("TOTAL_PAGES",$total_pages,true);
        
        /*
         * check user newsletter status
         */
        $query = new newsletter();
        $newsletter_subscription = $query -> checkNewsletterStatus($user_details->username);
        $smarty->assign_notnull("newsletter_subscription",$newsletter_subscription,true);
        
        /* edit address */
        $address = array();
        if($section=='editaddress' && $id!='0'):
            $AddressObj = new user_address();
            $address = $AddressObj -> getAddress($id);
            
            $states = array();
            if($address->country_id!='0' && $address->country_id!=''):
                $QueryObj = new state();
                $states = $QueryObj ->getStateNameByCountry($address->country_id);
            endif;
            $smarty->assign_notnull("states",$states,true);
            
        elseif($section=='editaddress' && $id=='0'):
            Redirect(make_url('account','section=address'));
        endif;
        $smarty->assign_notnull("address",$address,true);
    else:
        Redirect(make_url('logout'));
    endif;
else:
    $login_session->pass_msg[]="Please login First.";
    Redirect(make_url('login'));
endif;


/* edit markup */
        $markup = array();
        if($section=='editmarkup' && $id!='0'):
            $MarkupObj = new markup();
            $markup = $MarkupObj -> getMarkup($id);
        $smarty->assign_notnull("markup",$markup,true);
 
endif;

/* delete markup */
if($section=='deletemarkup' && $id!='0'):
		$id;
            $MarkupObj = new markup();
            $markup = $MarkupObj -> deleteMarkup($id);
        Redirect(make_url('account','section=markup'));
 
endif;


/* edit markup */
        if(isset($_POST['edit_markup']) && $_POST['edit_markup']!=''):

                $markup_id=$_POST['markup_id'];
		$markup=$_POST['markup'];                
		$markupUpdateQuery = new markup();
                $markupUpdateQuery -> updateMarkup($markup_id,$markup);
                
                $login_session->pass_msg[]=show(MSG_MARKUP_UPDATE_SUCCESS,false);
                $login_session->set_pass_msg(); 
                Redirect(make_url('account','section=markup'));
         
        endif;
        /* edit markup */

	/* get patients list */
	$Query_address = new patient();
        $patients_list = $Query_address->getPatients($user_id);	
        $smarty->assign_notnull("patients_detail",$patients_list,true);

/************* ADD PATIENT *****************/

if (isset($_POST['add_patient']) && $_POST['add_patient'] == 'add'):


    /* use server side validation */
   $validation = new user_validation();

    $validation->add('firstname', 'req');
    $validation->add('firstname', 'reg_words');

    $validation->add('lastname', 'req');
    $validation->add('lastname', 'reg_words');

    $valid = new valid();
    $error = 0;
    if ($valid->validate($_POST, $validation->get())):
        $error = 0;
    else:
        $error = 1; 
        $error_obj->errorAddArray($valid->error);
    endif;


    if ($error != '1'): /* if there is no error */

      $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
	$s_firstname = $_POST['shipping_firstname'];
	$s_lastname = $_POST['shipping_lastname'];
	$s_address = $_POST['shipping_address1'];
	$s_address1 = $_POST['shipping_address2'];
	$s_country = $_POST['shipping_country'];
	$s_state = $_POST['shipping_state'];
	$s_city = $_POST['shipping_city'];
	$s_zipcode = $_POST['shipping_zipcode'];
	$patient_remark = $_POST['patient_remark'];
	$b_firstname = $_POST['billing_firstname'];
	$b_lastname = $_POST['billing_lastname'];
	$b_address = $_POST['billing_address1'];
	$b_address1 = $_POST['billing_address2'];
	$b_country = $_POST['billing_country'];
	$b_state = $_POST['billing_state'];
	$b_city = $_POST['billing_city'];
	$b_zipcode = $_POST['billing_zipcode'];
	$_POST['user_id']=$user_id;	
	//$file = $_POST['file'];
	/*$create_obj = new user();
        $new_user_id = $create_obj->createUser($_POST);*/

        $patient_obj = new patient();
        $patient = $patient_obj->checkPatientEmailExists($email);
        if (!$patient):
            
            $create_obj = new patient();
            $new_patient_id = $create_obj->createPatient($_POST,$user_id);			
            if ($new_user_id):
                
                $login_session->pass_msg[] = show(MSG_REGISTERSUCCESS, FALSE);
                $login_session->set_pass_msg();
              	Redirect(make_url('account','section=patient'));
            //else:
               // $login_session->set_error();
                //$login_session->pass_msg[] = show(MSG_REGISTR_FAILED, FALSE);
                //$login_session->set_pass_msg();
            endif;
        else:
            $login_session->set_error();
            $login_session->pass_msg[] = show(MSG_REGISTER_EMAIL_ALREADY_EXIST, FALSE);
            $login_session->set_pass_msg();
        endif;
    endif;
   
endif;
/************* ADD PATIENT end *****************/

/************* ADD markup *****************/

if (isset($_POST['add_markup']) && $_POST['add_markup'] == 'add'):


    /* use server side validation */
   $validation = new user_validation();

    $validation->add('firstname', 'req');
    $validation->add('firstname', 'reg_words');

    $validation->add('lastname', 'req');
    $validation->add('lastname', 'reg_words');

    $valid = new valid();
    $error = 0;
    if ($valid->validate($_POST, $validation->get())):
        $error = 0;
    else:
        $error = 1; 
        $error_obj->errorAddArray($valid->error);
    endif;


    if ($error != '1'): /* if there is no error */

      $markup = $_POST['markup'];
        
	//$file = $_POST['file'];
	/*$create_obj = new user();
        $new_user_id = $create_obj->createUser($_POST);*/

        
            
            $create_obj = new markup();
            $new_markup_id = $create_obj->saveMarkup($_POST,$user_id);			
            if ($new_markup_id):
                
                $login_session->pass_msg[] = show(MSG_MARKUP_ADDED_SUCCESSFULLY, FALSE);
                $login_session->set_pass_msg();
              	Redirect(make_url('account','section=markup'));
            else:
                $login_session->set_error();
                $login_session->pass_msg[] = show(MSG_REGISTR_FAILED, FALSE);
                $login_session->set_pass_msg();
            endif;
        else:
            $login_session->set_error();
            $login_session->pass_msg[] = show(MSG_REGISTER_EMAIL_ALREADY_EXIST, FALSE);
            $login_session->set_pass_msg();
        endif;
    
   
endif;
/************* ADD markup end *****************/

/* edit patient */

        $patient = array();
        if($section=='editpatient' && $id!='0'):	
            $PatientObj = new patient();
            $patient = $PatientObj -> getPatient($id);
	    $country_id=$patient->shipping_country;
	    $state_id=$patient->shipping_state;
	    $b_country_id=$patient->billing_country;
	    $b_state_id=$patient->billing_state;
	    $state = new state();
	    $all_state = $state->getStateNameByCountry($country_id);	
	    $smarty->assign_notnull("ALL_STATE",$all_state,true);
	    $state = new state();
	    $all_state_b = $state->getStateNameByCountry($b_country_id);	
	    $smarty->assign_notnull("ALL_STATE_B",$all_state_b,true);	
	    $city = new city();
	    $all_city = $city->getCityNameByState($state_id);	
	    $smarty->assign_notnull("ALL_CITY",$all_city,true);
	    $city = new city();
	    $all_city_b = $city->getCityNameByState($b_state_id);	
	    $smarty->assign_notnull("ALL_CITY_B",$all_city_b,true);						
            $smarty->assign_notnull("patient",$patient,true);
endif;

/* edit patient */
        if(isset($_POST['edit_patient']) && $_POST['edit_patient']!=''):

               	$firstname = $_POST['firstname'];
        	$lastname = $_POST['lastname'];
        	$email = $_POST['email'];
        	$phone = $_POST['phone'];
		$s_firstname = $_POST['shipping_firstname'];
		$s_lastname = $_POST['shipping_lastname'];
		$s_address = $_POST['shipping_address1'];
		$s_address1 = $_POST['shipping_address2'];
		$s_country = $_POST['shipping_country'];
		$s_state = $_POST['shipping_state'];
		$s_city = $_POST['shipping_city'];
		$s_zipcode = $_POST['shipping_zipcode'];
		$patient_remark = $_POST['patient_remark'];
		$b_firstname = $_POST['billing_firstname'];
		$b_lastname = $_POST['billing_lastname'];
		$b_address = $_POST['billing_address1'];
		$b_address1 = $_POST['billing_address2'];
		$b_country = $_POST['billing_country'];
		$b_state = $_POST['billing_state'];
		$b_city = $_POST['billing_city'];
		$b_zipcode = $_POST['billing_zipcode'];                
		$patientUpdateQuery = new patient();
                $patientUpdateQuery -> updatePatient($_POST,$user_id,$id);
                
                $login_session->pass_msg[]=show(MSG_MARKUP_UPDATE_SUCCESS,false);
                $login_session->set_pass_msg(); 
                Redirect(make_url('account','section=patient'));
        endif;
        /* edit patient */
	/* search patient */
	if(isset($_POST['search_patient']) && $_POST['search_patient']!=''):
		$result=array();
               	$name = $_POST['search_patient_name'];             
		$patientSearchQuery = new patient();
                $result=$patientSearchQuery -> searchPatient($user_id,$name);
		$smarty->assign_notnull("patients_search",$result,true);
        endif;


	/* search patient end */

/* view patient detail */
	$Query_address = new patient();
        $view_patient = $Query_address->viewPatient($id);		
        $smarty->assign_notnull("patient_detail",$view_patient,true);

/* delete patient */
if($section=='deletepatient' && $id!='0'):
		$id;
            $PatientObj = new patient();
            $patient = $PatientObj -> deletePatient($id);
        Redirect(make_url('account','section=patient'));
 
endif;


/* get herbs list */
	$Query_herb = new product();
        $herbs_list = $Query_herb->getAllFilterProducts($p,false,false,'',$categories=0,$brands=0,$attributes=array(),$selcted_options='',$sort_by='',$sort_order='',false,false,'herb',false);
	$list=$herbs_list['records'];	
        $smarty->assign_notnull("herbs_detail",$list,true);
/* get herb */
	$herb_query = new product();
        $herb = $herb_query ->getProduct($id,'all');
	$smarty->assign_notnull("herb_detail",$herb,true);
	//echo "<pre>";
	//print_r($herb);
	//exit;

/* get Formulas Template list */
	$Query_formulas_template = new formula();
        $formulas_list = $Query_formulas_template->getFormulas();
	//echo "<pre>";
	//print_r($formulas_list);
	//exit;
        $smarty->assign_notnull("formulas_detail",$formulas_list,true);

/* SEO information */  
$content=add_metatags("My Account");
$smarty->renderLayout();
?>
