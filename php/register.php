<?php
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/newsletterClass.php');

//$redirect = (isset($_POST['redirect'])) ? $_POST['redirect'] : "";
$redirect = (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!='')?$_SERVER['HTTP_REFERER']:"";
if(!validateUrl($redirect)):
    $redirect = make_url('account');
endif;
if ($logged_in):
    Redirect($redirect);
endif;

/* Countries List */
$country = new country();
$all_countries = $country->Country_records();
$smarty->assign_notnull("ALL_COUNTRY",$all_countries,true);
/************* Practitioner Registration *****************/

if (isset($_POST['resigter']) && $_POST['resigter'] == 'new'):


    /* use server side validation */
    $validation = new user_validation();

    $validation->add('firstname', 'req');
    $validation->add('firstname', 'reg_words');

    $validation->add('lastname', 'req');
    $validation->add('lastname', 'reg_words'); 

    $validation->add('username', 'req');
    $validation->add('username', 'email');
    $validation->add('username', 'reg_words');

    $validation->add('password', 'req');
    $validation->add('password', 'minlength', '6');
    $validation->add('password', 'special_chars'); 

    $valid = new valid();
    $error = 0;
    if ($valid->validate($_POST, $validation->get())):
        $error = 0;
    else:
        $error = 1; /* set error */
        $error_obj->errorAddArray($valid->error);
    endif;


    if ($error != '1'): /* if there is no error */

       $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['username'];
        $password = md5($_POST['password']);
	$clinic_name = $_POST['clinic_name'];
	$clinic_web_address = $_POST['web_address'];
	$mobile = $_POST['mobile'];
	$work_phone = $_POST['phone'];
	$education = $_POST['education'];
	$reg_info = $_POST['reg_info'];
	$practitioner_type = $_POST['practitioner_type'];
	$s_firstname = $_POST['shipping_firstname'];
	$s_lastname = $_POST['shipping_lastname'];
	$s_address = $_POST['shipping_address1'];
	$s_address1 = $_POST['shipping_address2'];
	$s_country = $_POST['country_id'];
	$s_state = $_POST['state_id'];
	$s_city = $_POST['city_id'];
	$s_zipcode = $_POST['zip_code'];
	//$file = $_POST['file'];
	$create_obj = new user();
        $new_user_id = $create_obj->createUser($_POST);

        $user_obj = new user();
        $user = $user_obj->checkUserEmailExists($email);
        if (!$user):
            
            $create_obj = new user();
            $new_user_id = $create_obj->createUser($_POST);
            if ($new_user_id):
                user::loginUser($new_user_id);
                

                $login_session->pass_msg[] = show(MSG_REGISTERSUCCESS, FALSE);
                $login_session->set_pass_msg();
                Redirect(make_url('home'));
            else:
                $login_session->set_error();
                $login_session->pass_msg[] = show(MSG_REGISTR_FAILED, FALSE);
                $login_session->set_pass_msg();
            endif;
        else:
            $login_session->set_error();
            $login_session->pass_msg[] = show(MSG_REGISTER_EMAIL_ALREADY_EXIST, FALSE);
            $login_session->set_pass_msg();
        endif;
    endif;
    Redirect(make_url('register'));
endif;
/************* Practitioner Registration end *****************/

/* SEO information */
$content = add_metatags("Register");
$smarty->renderLayout();
?>
