<?php
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/brandClass.php');

/*get all active categories*/ 
$category_obj = new category();
$categories = $category_obj->listRecursiveCategory('0',true);

$all_category_count = count($categories);
$categories_in_one_row = ceil($all_category_count/4);

$smarty->assign_notnull("categories",$categories,true);

/* get all active brands */
$brand_obj = new brand();
$brands = $brand_obj->listBrand(true,'array',true);
$smarty->assign_notnull("brands",$brands,true);

/* SEO information */  
$content=add_metatags("Sitemap");
$smarty->renderLayout();
?>