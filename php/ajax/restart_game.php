<div class="time_over"><br/>
    <h3>You have given <?= $error ?> wrong answer<?= ($error > 1) ? 's' : '' ?></h3>
    <br />
    <div class="btn btn btn-cta btn-cta-primary" onclick="window.location.reload(true);">Play again</div>
    <br/>
    <br/>
</div>