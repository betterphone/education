<div class="panel panel-default">		
    <div class="panel-heading"> Q. <?php echo $startQuestion['name'] ?>  </div>
</div>
<p class='required_ans'>Answer Required</p>
<?php
$query = new answer();
$answers = $query->list_answer_front($startQuestion['id']);

$query = new answer();
$NeedAnswer = $query->total_answer_front_correct($startQuestion['id']);
?>

<input type='hidden' id='question_id' rel="<?= $NeedAnswer ?>" name='question_id' value='<?php echo $startQuestion['id']; ?>'/>  
<?php if ($answers) { ?>
    <?php foreach ($answers as $answer) { ?>
        <div class="form-group">	
            <? if ($startQuestion['question_type'] == 'Multiple True'): ?>
                <label><input class='answer_id' type="checkbox" name="answer[]" value="<?php echo $answer['id'] ?>"> <?php echo $answer['answer'] ?></label>
            <? else: ?>
                <label><input class='answer_id' type="radio" name="answer[0]" value="<?php echo $answer['id'] ?>"> <?php echo $answer['answer'] ?></label>
            <? endif; ?>
        </div> 
        <?php
    }
} else {
    ?>
    <div class="form-group">
        No answer
    </div>
    <?php
}
?>
<input type='hidden' name='user_id' value='<?= $user_id ?>'/>
<?php if ($answers) { ?>
    <div class="footer-group text-center">			
        <input class="btn btn-cta btn-cta-secondary submit_answer" type="submit" name="submit_answer" value="Submit" tabindex="7" />
    </div>
<?php } ?>