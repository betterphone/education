<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$user_id = $_SESSION['user_session']['user_id'];

if (isset($_POST['submit'])) {
    $query = new user;
    $password_exist = $query->password_exist($_POST['old_password']);

    if ($password_exist) {
        if ($_POST['password'] === $_POST['confirm_password']) {
            $query = new user;
            $query->saveUser($_POST);
            $admin_user->set_pass_msg('Password changed Successfully');
            Redirect(make_url('change_password'));
        } else {
             $admin_user->set_error();
            $admin_user->set_pass_msg('password not match');
            Redirect(make_url('change_password'));
        }
    } else {
        $admin_user->set_error();
            $admin_user->set_pass_msg('Old password is incorrect');
            Redirect(make_url('change_password'));
    }
}

/* SEO information */  
$content=add_metatags("Change Password");
?>
