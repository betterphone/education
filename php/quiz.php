<?php

include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/answerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/roundClass.php');
$category_id = isset($_GET['category_id']) ? $_GET['category_id'] : '';
$correct_answer = array();

$user_id = ($_SESSION['user_session']['user_id']);

if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}

$query = new category();
$getCategory = $query->getCategory($category_id);

$QueryObj = new user_points();
$roundNo = $QueryObj->checkPrvRound($user_id, $category_id);

$QueryObj = new round();
$roundDetail = $QueryObj->getRound($roundNo);

if (!$roundDetail) {
    Redirect(make_url('history&cat_id=' . $category_id));
}

unset($_SESSION['user']);

$query = new question;
$question_using_category = $query->question_using_level_category($category_id, $roundNo);

$query = new question;
$total_question = $query->total_question_in_category($category_id);

$query = new question;
$total_round_question_count = $query->total_round_question_count($category_id, $roundNo);


if ($roundDetail->level == 'EASY') {
    $level_start = 0;
    $level_end = 5;
}
if ($roundDetail->level == 'INTERMEDIATE') {
    $level_start = 6;
    $level_end = 10;
}
if ($roundDetail->level == 'CHALLENGING') {
    $level_start = 11;
    $level_end = 15;
}

$query = new question;
$total_level_question_count = $query->total_level_question_count($category_id, $level_start, $level_end);

if (!empty($question_using_category)):
    $startQuestion = $question_using_category[0];
else:
    $startQuestion = '';
endif;

foreach ($question_using_category as $quest) {
    $query = new answer();
    $ans = $query->list_answer_correct($quest['id']);
}

if (isset($_POST['submit'])) {
    foreach ($_POST['result'] as $result) {
        $query = new question_quiz;
        $result['user_id'] = $user_id;
        $result['round'] = 1;
        $result['level_id'] = 'Easy';
        $questions = $query->Save_Quiz($result);
    }
    $query = new question_quiz;
    $selected_answer = $query->fetch();
    $same_correct = array();
    foreach ($selected_answer as $sel) {
        $same_correct['question_' . $sel['question_id']] = $sel['answer_id'];
    }

    $as = array();
    foreach ($correct_answer as $key => $c) {
        foreach ($same_correct as $u) {
            if ($c == $u) {
                $as[$key] = $c;
            }
        }
    }
}
if ($question_using_category) {
    $query = new question_quiz;
    $question_exist = $query->check_question_exist($question_using_category[0]['id'], $user_id, 1);
}

if ($roundDetail->id == 6) {
    $query = new information;
    $my_information = $query->list_user($user_id);
    if ($my_information) {
        if (empty($my_information->sex) || empty($my_information->location) || empty($my_information->dob) || empty($my_information->question) || empty($my_information->answer)) {
            $startQuestion = 'fill_profile';
        }
    } else {
        $startQuestion = 'fill_profile';
    }
}

$content = add_metatags("Play Game");
?>