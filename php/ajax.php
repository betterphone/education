<?php

/*
 * Ajax FIle- this file manages routing of all ajax scripts
 * You are not adviced to make edit  this File.
 *   
 */

/* Handle actions here. */
include_once('../include/config/config.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/answerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/roundClass.php');


/* * ************ INCLUDE BASIC FUNTIONS - VITAL FUNCTIONS *********** */
$include_fucntions = array('http', 'image_manipulation', 'calender', 'url_rewrite', 'email', 'paging');
include_functions($include_fucntions);


$type = (isset($_GET['type'])) ? $_GET['type'] : '';
$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '';
$question_id = (isset($_POST['question_id'])) ? $_POST['question_id'] : '0';

/* Quest Detail */
$QueryObj = new question();
$fullQues = $QueryObj->getQuiz($question_id);

$QueryObj = new round();
$roundDetail = $QueryObj->getRound($fullQues->round);

/* Current Level */
$currentLevel = $roundDetail->level;

/* Current Round */
$currentRound = $roundDetail->id;

/* Current Category_id */
$currentCat = $fullQues->category_id;

/* Set Done Question */
$_SESSION['user'][$currentLevel]['question'][] = $question_id;
$doneQuest = $_SESSION['user'][$currentLevel]['question'];

/* Keep Answers */
$_SESSION['user'][$currentLevel]['answers'][$question_id] = $_POST['answer'];

/* All Question */
$query = new question;
$reMaining = $query->question_using_level_category_remaining($currentCat, $currentRound, $doneQuest);

if ($type == 'quiz'):
    if (!empty($reMaining)):
        $startQuestion = $reMaining[0];
        include_once(DIR_FS_SITE_PHP . 'ajax/new_question.php');
        exit;
    else:
        $error = '0';
        /* Now Check Your Answer against your qustion */
        foreach ($_SESSION['user'][$currentLevel]['question'] as $key => $qust):
            $query = new answer();

            $Or_ans = $query->allCorrectAnswerArray($qust);
            $pr_ans = $_SESSION['user'][$currentLevel]['answers'][$qust];

            $result = array_diff($Or_ans, $pr_ans);

            if (!empty($result)):
                $error++;
            endif;
        endforeach;
        if ($error == 0):
            $points = $roundDetail->points;

            /* Now Save The Score For Current Round */
            $data = array('cat_id' => $currentCat, 'round_id' => $currentRound, 'user_id' => $user_id, 'points' => $points);

            $QueryObj = new user_points();
            $QueryObj->saveUserPoints($data);

            include_once(DIR_FS_SITE_PHP . 'ajax/startNewRound.php');

        else:
            include_once(DIR_FS_SITE_PHP . 'ajax/restart_game.php');
        endif;
        exit;

    endif;
endif;
?>
