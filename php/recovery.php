<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$user_id = $_SESSION['user_session']['user_id'];

$token = isset($_GET['token']) ? $_GET['token'] : Redirect(make_url('forgot'));

$query = new user;
$is_value = $query->is_value('forgot_password_token', $token);

if (!$is_value) {
    Redirect(make_url('sign_in'));
}

if (isset($_POST['submit'])) {
    if ($_POST['password'] === $_POST['confirm_password']) {
        $query = new user;
        $query->update_field_custom('password', $_POST['password'], 'forgot_password_token', $token);
        $admin_user->set_pass_msg('Password changed Successfull');
        Redirect(make_url('sign_in'));
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('password not match');
        Redirect(make_url('forgot'));
    }
}

/* SEO information */
$content = add_metatags("forgot");
?>
