<?php

include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/questionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/answerClass.php');
$category_id = isset($_GET['category_id']) ? $_GET['category_id'] : '';

$user_id = ($_SESSION['user_session']['user_id']);

if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}

$query = new question;
$question_using_category = $query->question_using_level_category($category_id, 'Easy', 2);

foreach ($question_using_category as $quest) {
    $query = new answer();
    $ans = $query->list_answer_correct($quest['id']);
    $correct_answer['question_' . $quest['id']] = $ans->id;
}



if (isset($_POST['submit'])) {
    foreach ($_POST['result'] as $result) {
        $query = new question_quiz;
        $result['user_id'] = $user_id;
        $result['round'] = 2;
        $result['level_id'] = 'Easy';
        $questions = $query->Save_Quiz($result);
    }
    $query = new question_quiz;
    $selected_answer = $query->fetch();
    $same_correct = array();
    foreach ($selected_answer as $sel) {
        $same_correct['question_' . $sel['question_id']] = $sel['answer_id'];
    }

    $as = array();
    foreach ($correct_answer as $key => $c) {
        foreach ($same_correct as $u) {
            if ($c == $u) {
                $as[$key] = $c;
            }
        }
    }
}
$query = new question_quiz;
$question_exist = $query->check_question_exist($question_using_category[0]['id'], $user_id, 2);
?>