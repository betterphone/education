<?php
$key = isset($_GET['key'])?$_GET['key']:0;
if($key!='0'):
    $userObj = new user();
    $user = $userObj->getUserByEmailToken($key);
        /*if id received go for verify email id*/
        if($user):
                $query=new user();
                $user_verify = $query->setEmailVerified($user->id);
                if($user_verify):
                    # set logged in and redirect to needed page.
                    $login_session =new user_session();
                    $login_session->user_id=$user->id;
                    $login_session->set_user_id();
                    $login_session->username=$user->username;
                    $login_session->set_username();

                    #Update total visits
                    $QueryObj1= new user;
                    $QueryObj1->updateTotalVisits($user->id);

                    /*send email*/
                    $email_array=array(
                        'firstname'=>$user->firstname,
                        'lastname'=>$user->lastname
                     );
                
                    /*** user email **/
                    send_db_email_content(EMAIL_VERIFY_ACCOUNT_USER, $user->username,$email_array);
                    
                    $login_session->pass_msg[]=show(MSG_EMAIL_CONFIRMATION_SUCCESS,FALSE);
                    $login_session->set_pass_msg();
                    redirect(make_url('account'));
             else:
                  $login_session->set_error();
                  $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
                  $login_session->set_pass_msg();
             endif;
        else:
            $login_session->set_error();
            $login_session->pass_msg[]=show(MSG_INCORRECT_KEY,FALSE);
            $login_session->set_pass_msg();
        endif;
else:
    $login_session->set_error();
    $login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
    $login_session->set_pass_msg();
endif;

Redirect1(make_url('register'));  

/* SEO information */  
$content=add_metatags("Verify Account");
$smarty->renderLayout();
?>
