<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$r = isset($_GET['r']) ? $_GET['r'] : '';
$category_id = isset($_GET['category_id']) ? $_GET['category_id'] : '';

if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}
$user_id = $_SESSION['user_session']['user_id'];

$query = new user;
$user = $query->list_user($user_id);

$query = new information;
$information = $query->list_user($user_id);

if ($information) {
    $dob_array = explode('-', $information->dob);
    $date = $dob_array[0];
    $month = $dob_array[1];
    $year = $dob_array[2];
} else {
    $date = '';
    $month = '';
    $year = '';
}

$s_questions = array(
    'what is your father name',
    'what is your spouse name'
);

if (isset($_POST['information_submit'])) {
    extract($_POST);
    $dob = $date . '-' . $month . '-' . $year;
    $data = array(
        'sex' => $sex,
        'location' => $location,
        'dob' => $dob,
        'question' => $question,
        'answer' => $answer
    );

    $query = new information;
    $is_exist = $query->is_exist($user_id);

    $data['user_id'] = $user_id;
    if (!$is_exist) {
        $query = new information();
        $query->save($data);
    } else {
        $query = new information();
        $query->update_($data, $user_id);
    }
    if ($r && $category_id) {
        Redirect(make_url('quiz&category_id=' . $category_id));
    } else {
        $admin_user->set_pass_msg('Updated Successfully');
        Redirect(make_url('profile_edit'));
    }
}


$query = new countries;
$countries = $query->countries_all();


if (isset($_POST['submit'])) {

    $error = 0;
    if ($_POST['password'] != $_POST['confirm_password']):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Password and confirm password not matched..!');
    endif;

    $user_obj = new user();
    $user = $user_obj->checkFielExistsOther('username', $_POST['username'], $_POST['id']);
    if (is_object($user)):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry This email already exist..!');
    endif;

    $user_obj = new user();
    $user = $user_obj->checkFielExistsOther('phone', $_POST['phone'], $_POST['id']);
    if (is_object($user)):
        $error = 1; /* set error */
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry This phone no already exist..!');
    endif;

    //checkFielExists
    if ($error == '0') {

        $query = new user;
        $query->saveUser($_POST);
        $admin_user->set_pass_msg('Updated Successfully');
        Redirect(make_url('profile_edit'));
    }
    Redirect(make_url('profile_edit'));
}

/* SEO information */
$content = add_metatags("Edit Profile");
?>