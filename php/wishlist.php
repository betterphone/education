<?php
include_once(DIR_FS_SITE.'include/functionClass/optionClass.php');

$remove = isset($_GET['remove'])?$_GET['remove']:0;
if($remove!=0):
    $removeObj = new user_wishlist();
    if($removeObj -> RemoveFromWishlist($remove,$user_key,$user_id)):
       // $login_session->pass_msg[]=show(MSG_WISHLIST_PRODUCT_UPDATE,FALSE);
        //$login_session->set_pass_msg();
    else:
        $login_session->pass_msg[]=show(MSG_WISHLIST_ERROR_OCCURED,FALSE);
        $login_session->set_error();
        $login_session->set_pass_msg();
    endif;
    Redirect(make_url('wishlist'));
endif;

/*
 * get user wishlist products
 */
$Query = new user_wishlist();
$wishlist = $Query -> getUserWishlist($user_id,$user_key);
$smarty->assign_notnull("allproducts",$wishlist,true);


/* SEO information */  
$content=add_metatags("Wishlist");
$smarty->renderLayout();
?>