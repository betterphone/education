<?php
include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');
$id = isset($_GET['id']) ? $_GET['id'] : 0;

$query = new content;
$content = $query->contents($id);

/* SEO information */  
$contentCont=add_metatags($content->page_name,$content->meta_keyword,$content->meta_description);
?>