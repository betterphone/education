<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$user_id = $_SESSION['user_session']['user_id'];
if (!$_SESSION['user_session']['user_id']) {
    Redirect(make_url('sign_in'));
}
$points = 10;
$data = array(
    'user_id' => $user_id,
    'points' => $points
);
$query = new bonus;
$query->save($data);
Redirect(DIR_WS_SITE);
?>