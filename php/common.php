<?php

/*
 * This file is used for adding the common codes for all the php files
 * like navigation and sliders
 */
/*  get classs  */
include_once(DIR_FS_SITE . 'include/functionClass/navigationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sliderClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/bannerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/productClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/countryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/searchClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/optionClass.php');

/* to get current location */
include_once(DIR_FS_SITE . 'include/location/ip.codehelper.io.php');
include_once(DIR_FS_SITE . 'include/location/php_fast_cache.php');


/*  top navigation  */
$MAIN_NAVIGATION = array();
if (is_numeric(MAIN_NAVIGATION_ID)):
    $query_nav = new navigationItem();
    $MAIN_NAVIGATION = $query_nav->getFullNavigation(MAIN_NAVIGATION_ID, '', TRUE);
endif;
$smarty->assign_notnull("MAIN_NAVIGATION", $MAIN_NAVIGATION, true);

$BOTTOM_NAVIGATION = array();
if (is_numeric(BOTTOM_NAVIGATION_ID)):
    $query_nav = new navigationItem();
    $BOTTOM_NAVIGATION = $query_nav->getFullNavigation(BOTTOM_NAVIGATION_ID, '', TRUE);
endif;
$smarty->assign_notnull("BOTTOM_NAVIGATION", $BOTTOM_NAVIGATION, true);

// GET mobile navigation

$MOBILE_NAVIGATION = array();
if (defined('MOBILE_NAVIGATION_ID') && is_numeric(MOBILE_NAVIGATION_ID)):
    $query_nav = new navigationItem();
    $MOBILE_NAVIGATION = $query_nav->getFullNavigation(MOBILE_NAVIGATION_ID, '', TRUE);
endif;
$smarty->assign_notnull("MOBILE_NAVIGATION", $MOBILE_NAVIGATION, true);

//navigation Bnners
$NavigationBanners = array();
$banner_obj = new banner;
$NavigationBanners = $banner_obj->getAllMegaMenuBanners();
$nav_banners = array();
if (!empty($NavigationBanners)) {
    foreach ($NavigationBanners as $Banner) {
        $nav_banners[$Banner->type_id] = $Banner;
    }
}
$NavigationBanners = $nav_banners;
$smarty->assign_notnull("NavigationBanners", $NavigationBanners, true);
/* navigation portion ends here */

/* Get slides */
$photos = array();
if (SLIDER_ACTIVE):
    $query = new sliderImage();
    $query->setParentId('1');
    $photos = $query->getAllSlides();
endif;
$smarty->assign_notnull("PHOTOS", $photos, true);
/* slider portion ends here */


/* check if user logged in or not */
$login_session = new user_session();
$logged_in = $user_id = 0;
$firstname = '';
if ($login_session->is_logged_in()):
    $logged_in = 1;
    $firstname = getUserFirstName();
    $user_id = $login_session->get_user_id();
endif;
$smarty->assign_notnull("FIRSTNAME", $firstname, true);
$smarty->assign_notnull("LOGGED_IN", $logged_in, true);
$smarty->assign_notnull("user_id", $user_id, true);
$smarty->assign_notnull("login_session", $login_session, true);


// **************** get user unique key ********************
$user_key = '';
if (isset($_COOKIE['user_unique_key']) && $_COOKIE['user_unique_key'] != ''):
    $user_key = $_COOKIE['user_unique_key'];
else:
    $user_key = session_id();
    /* make cookie to save user unique key */
    setcookie("user_unique_key", $user_key, time() + (10 * 365 * 24 * 60 * 60));
endif;
$smarty->assign_notnull("user_key", $user_key, true);

/* get cart object * */
global $CartObject;
$cartObj = new cart();
$CartObject = $cartObj->getCart();
$smarty->assign_notnull("CartObject", $CartObject, true);

/* cart items */
$cart_content_obj = new cart_detail();
$cart_items = $cart_content_obj->getCartItems();
$smarty->assign_notnull("cart_items", $cart_items, true);

/* get cart total items */
$cart_detail_obj = new cart_detail();
$cart_total_items = $cart_detail_obj->getCartTotalItems();
$smarty->assign_notnull("cart_total_items", $cart_total_items, true);

/* get cart total price */
$cart_price_obj = new cart_detail();
$cart_total_price = $cart_price_obj->getCartTotal();
$smarty->assign_notnull("cart_total_price", $cart_total_price, true);

/* get banners */
$banner_module_id = (isset($_GET['id'])) ? $_GET['id'] : "0";
foreach ($banner_locations_on_each_page as $kk => $vv):
    $bannerObj = new banner();
    $bannerr = 'banner_' . $kk;
    $$bannerr = $bannerObj->getActivePageBanners($page, $kk, $banner_module_id);
    $smarty->assign_notnull('banner_' . $kk, $$bannerr, true);

    /* increase banner views */
    if (!empty($$bannerr)):
        foreach ($$bannerr as $kk => $vv):
            $banner_obj = new banner();
            $banner_obj->IncreaseBannerViews($vv->id);
        endforeach;
    endif;

endforeach;
$smarty->assign_notnull('banner_locations_on_each_page', $banner_locations_on_each_page, true);

/*
 * auto detect
 * get user country
 */
/*
  $_ip = new ip_codehelper();

  // Detect Real IP Address & Location
  $real_client_ip_address = $_ip->getRealIP();

  if($real_client_ip_address=='127.0.0.1'):
  $real_client_ip_address='173.252.110.27'; //United States
  endif;

  $visitor_location = $_ip->ip_info($real_client_ip_address);
  $user_country=array();
  if($visitor_location['country_code']!=''):
  $user_country=country::getCountryFromISO($visitor_location['country_code']);
  if($user_country):
  $user_country_id=$user_country->id;
  endif;
  endif;

  if($login_session->get_country_id()==''):

  $user_country_id=0;
  $visitor_location = file_get_contents('http://api.hostip.info/country.php?ip='.$real_client_ip_address);
  $visitor_location = strtolower($visitor_location);
  $user_country=array();
  if($visitor_location!='' || $visitor_location!='xx'):
  $user_country=country::getCountryFromISO($visitor_location);
  if($user_country):
  $user_country_id = $user_country->id;
  endif;
  endif;

  $login_session->country_id = $user_country_id;
  $login_session->set_country_id();

  else:

  $user_country_id=$login_session->get_country_id();

  endif;
 */

$user_country_id = 236;
$smarty->assign_notnull("user_country_id", $user_country_id, true);

/* facebook login */
$loginUrl = '';
//include_once('facebook_login.php');
//$smarty->assign_notnull("facebook_login_url",'#'); 

/*
 * show popular search terms on footer
 */
$popularSearchObj = new search();
$popular_searches = $popularSearchObj->getSearchTerms('', '', '10');
$smarty->assign_notnull("popular_searches", $popular_searches, true);

/*
 * get current page url for login page
 */
if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
    $pro = 'https';
} else {
    $pro = 'http';
}
$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
$current_url = $pro . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$smarty->assign_notnull("current_url", $current_url, true);

// check if mobile device
require_once (DIR_FS_SITE_INCLUDE . 'mobile_detect/Mobile_Detect.php');
$is_mobile = 0;
$detect = new Mobile_Detect;
if ($detect->isMobile() && !$detect->isTablet()) {
    $is_mobile = 1;
}
$smarty->assign_notnull("is_mobile", $is_mobile, true);


/*
 * remove tld from sitename
 * for show in header title
 */
//$pos = strpos(SITE_NAME, '.');
//$SITE_NAME = (substr(SITE_NAME, 0, $pos));
$smarty->assign_notnull("SITE_NAME", SITE_NAME, true);


/*
 * get selected options in an array
 */
$color_option_id = option::checkOptionByName('color');
$size_option_id = option::checkOptionByName('size');
$smarty->assign_notnull("color_option_id", $color_option_id, true);
$smarty->assign_notnull("size_option_id", $size_option_id, true);
?>
