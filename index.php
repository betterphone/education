<?php
die;
include_once("include/config/config.php");

/* SSDN */
/*
 *   WEBSITE DEVELOPMENT BY cWebConsultants India
 *   Shopping cart software - cWebCart (c) of cWebConsultants India
 *   for support and sales enquiries call - info@cWebConsultants.com
 *   This is a proprietary software - unauthorized distribution & modification is strictly prohibited
 */

/* include External File which include the function to check the site expire or block */
//$site_folder=basename(__DIR__);
//include_once("include/config/external.php");

/* * ********* INCLUDE FILES *************** */
include_once("include/config/config.php");
//session_destroy();
/* include smarty class */


/* WEBSITE STATISTICS */
if (!isset($_SESSION['visitor'])):
    $_SESSION['visitor'] = 1;
    $qu = new query('web_stat');
    $qu->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
    $qu->Insert();
endif;


/* * ************ INCLUDE BASIC FUNTIONS - VITAL FUNCTIONS *********** */
$include_fucntions = array('http', 'image_manipulation', 'calender', 'url_rewrite', 'email', 'paging');
include_functions($include_fucntions);


/* * ********** FOR URL REWRITE *************** */
load_url();

/* * *********** website statistic recorded. *********************** */
$page = isset($_GET['page']) ? $_GET['page'] : "start";

if ($page == 'hrb'):
    $page = 'home';
endif;
// check site in maintance mode
// if admin logged in then open site even in maintance mode

if (MAINTANCE_MODE && !$admin_user->is_logged_in()):
    $page = 'maintance';
endif;


// check if user ip blocked from admin
//$blocked_ips = explode(',', BLACKLIST_IP_ADDRESS);
//if(count($blocked_ips) && in_array($_SERVER['REMOTE_ADDR'],$blocked_ips)):
//     $page = 'restricted';
//endif;



if (file_exists(DIR_FS_SITE_PHP . "$page.php"))
    require_once(DIR_FS_SITE_PHP . "$page.php");

if (file_exists(DIR_FS_SITE_HTML . "$page.php"))
    require_once(DIR_FS_SITE_HTML . "$page.php");
else
    require_once(DIR_FS_SITE_HTML . "404.php");
?>
