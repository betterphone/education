-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2016 at 06:07 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `education`
--

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_access_log`
--

CREATE TABLE IF NOT EXISTS `cwebc_access_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) DEFAULT NULL,
  `action` text,
  `ip` varchar(20) DEFAULT NULL,
  `dt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=436 ;

--
-- Dumping data for table `cwebc_access_log`
--

INSERT INTO `cwebc_access_log` (`id`, `user`, `action`, `ip`, `dt`) VALUES
(1, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:06:52'),
(2, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:07:10'),
(3, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:09:43'),
(4, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:13:14'),
(5, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:18:48'),
(6, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:19:21'),
(7, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:19:27'),
(8, 'admin', 'Action update performed on  in level module', '::1', '2016-01-28 21:19:33'),
(9, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:06:55'),
(10, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:07:46'),
(11, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:08:06'),
(12, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:08:31'),
(13, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:08:38'),
(14, 'admin', 'Action update performed on 30 in category module', '::1', '2016-01-28 23:09:07'),
(15, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:10:45'),
(16, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:11:30'),
(17, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:11:36'),
(18, 'admin', 'Action update performed on  in setting module', '::1', '2016-01-28 23:15:47'),
(19, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:16:06'),
(20, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:16:49'),
(21, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:16:56'),
(22, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:17:12'),
(23, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:17:16'),
(24, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:17:46'),
(25, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:18:53'),
(26, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:19:03'),
(27, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:19:12'),
(28, 'admin', 'Action update performed on 28 in category module', '::1', '2016-01-28 23:19:16'),
(29, 'admin', 'Action update performed on 13 in content module', '::1', '2016-01-28 23:42:30'),
(30, 'admin', 'Action update performed on 13 in content module', '::1', '2016-01-28 23:43:12'),
(31, 'admin', 'Action update performed on  in question module', '127.0.0.1', '2016-02-07 21:08:19'),
(32, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-07 21:47:38'),
(33, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-07 22:54:50'),
(34, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-07 23:27:11'),
(35, 'admin', 'Action update performed on 29 in question module', '127.0.0.1', '2016-02-08 03:35:57'),
(36, 'admin', 'Action update performed on 29 in question module', '127.0.0.1', '2016-02-08 03:36:13'),
(37, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:25:22'),
(38, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:33:02'),
(39, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:41:40'),
(40, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:43:00'),
(41, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:44:13'),
(42, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:44:29'),
(43, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:45:37'),
(44, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:46:29'),
(45, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:52:21'),
(46, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:53:57'),
(47, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:54:03'),
(48, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 04:54:17'),
(49, 'admin', 'Action update performed on 28 in question module', '127.0.0.1', '2016-02-08 06:25:19'),
(50, 'admin', 'Action update performed on 32 in question module', '127.0.0.1', '2016-02-08 22:51:53'),
(51, 'admin', 'Action update performed on  in answer module', '127.0.0.1', '2016-02-09 03:41:44'),
(52, 'admin', 'Action update performed on  in answer module', '127.0.0.1', '2016-02-09 03:42:48'),
(53, 'admin', 'Action update performed on  in answer module', '127.0.0.1', '2016-02-09 03:42:53'),
(54, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:46:23'),
(55, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:46:32'),
(56, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:46:39'),
(57, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:46:53'),
(58, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:47:04'),
(59, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 03:47:52'),
(60, 'admin', 'Action update performed on 10 in answer module', '127.0.0.1', '2016-02-09 03:48:24'),
(61, 'admin', 'Action update performed on 32 in question module', '127.0.0.1', '2016-02-09 03:50:04'),
(62, 'admin', 'Action update performed on 3 in answer module', '127.0.0.1', '2016-02-09 03:51:12'),
(63, 'admin', 'Action update performed on 8 in answer module', '127.0.0.1', '2016-02-09 05:24:36'),
(64, 'admin', 'Action update performed on 36 in question module', '127.0.0.1', '2016-02-09 05:29:22'),
(65, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 05:36:17'),
(66, 'admin', 'Action update performed on 9 in answer module', '127.0.0.1', '2016-02-09 05:51:54'),
(67, 'admin', 'Action update performed on 32 in question module', '127.0.0.1', '2016-02-10 07:21:46'),
(68, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 19:53:19'),
(69, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-09 19:56:46'),
(70, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 21:05:56'),
(71, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 21:06:07'),
(72, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 21:16:12'),
(73, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 21:26:33'),
(74, 'admin', 'Action update performed on 15 in answer module', '127.0.0.1', '2016-02-09 21:38:53'),
(75, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 21:49:26'),
(76, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 22:02:10'),
(77, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 22:13:57'),
(78, 'admin', 'Action update performed on 13 in answer module', '127.0.0.1', '2016-02-09 22:31:56'),
(79, 'admin', 'Action update performed on 13 in answer module', '127.0.0.1', '2016-02-09 22:39:25'),
(80, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 22:39:37'),
(81, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 22:40:03'),
(82, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 22:40:18'),
(83, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-09 22:42:54'),
(84, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 22:44:45'),
(85, 'admin', 'Action update performed on 42 in question module', '127.0.0.1', '2016-02-09 22:50:26'),
(86, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 23:24:23'),
(87, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 23:38:36'),
(88, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 23:40:42'),
(89, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-09 23:51:52'),
(90, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 23:52:36'),
(91, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 23:53:56'),
(92, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-09 23:57:21'),
(93, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-10 00:02:10'),
(94, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:05:17'),
(95, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:06:29'),
(96, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:06:34'),
(97, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:06:42'),
(98, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:08:28'),
(99, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:08:48'),
(100, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:09:57'),
(101, 'admin', 'Action update performed on 27 in answer module', '127.0.0.1', '2016-02-10 00:10:02'),
(102, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:16:18'),
(103, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:16:29'),
(104, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:22:07'),
(105, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-10 00:25:10'),
(106, 'admin', 'Action update performed on 32 in answer module', '127.0.0.1', '2016-02-10 00:27:15'),
(107, 'admin', 'Action update performed on 34 in answer module', '127.0.0.1', '2016-02-10 00:29:40'),
(108, 'admin', 'Action update performed on 35 in answer module', '127.0.0.1', '2016-02-10 00:29:45'),
(109, 'admin', 'Action update performed on 37 in answer module', '127.0.0.1', '2016-02-10 00:30:03'),
(110, 'admin', 'Action update performed on 40 in answer module', '127.0.0.1', '2016-02-10 00:32:20'),
(111, 'admin', 'Action update performed on 44 in answer module', '127.0.0.1', '2016-02-10 00:34:54'),
(112, 'admin', 'Action update performed on 47 in answer module', '127.0.0.1', '2016-02-10 00:37:07'),
(113, 'admin', 'Action update performed on 53 in answer module', '127.0.0.1', '2016-02-10 00:39:19'),
(114, 'admin', 'Action update performed on 51 in answer module', '127.0.0.1', '2016-02-10 00:39:39'),
(115, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:40:01'),
(116, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 00:43:17'),
(117, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:46:13'),
(118, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-10 00:48:52'),
(119, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:58:31'),
(120, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 00:58:57'),
(121, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 01:37:45'),
(122, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:07:26'),
(123, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:07:33'),
(124, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-10 03:07:40'),
(125, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-10 03:07:50'),
(126, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:12:12'),
(127, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:12:25'),
(128, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:14:09'),
(129, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:14:13'),
(130, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:14:24'),
(131, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:14:40'),
(132, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-10 03:14:48'),
(133, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 03:14:52'),
(134, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-10 03:15:02'),
(135, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-10 03:15:29'),
(136, 'admin', 'Action update performed on  in question module', '127.0.0.1', '2016-02-10 04:11:40'),
(137, 'admin', 'Action update performed on  in question module', '127.0.0.1', '2016-02-10 04:11:55'),
(138, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:13:38'),
(139, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:13:47'),
(140, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:13:54'),
(141, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:14:07'),
(142, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:14:10'),
(143, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:14:31'),
(144, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:14:57'),
(145, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:15:02'),
(146, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:15:13'),
(147, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:16:45'),
(148, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-10 04:16:48'),
(149, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-10 04:17:01'),
(150, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-10 04:17:11'),
(151, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-10 04:49:06'),
(152, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-10 04:49:14'),
(153, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-10 04:49:21'),
(154, 'admin', 'Action update performed on 47 in question module', '127.0.0.1', '2016-02-10 04:50:36'),
(155, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-10 04:54:18'),
(156, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-10 04:54:47'),
(157, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-10 04:54:53'),
(158, 'admin', 'Action update performed on 53 in question module', '127.0.0.1', '2016-02-10 05:00:44'),
(159, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-10 05:14:37'),
(160, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-10 05:47:21'),
(161, 'admin', 'Action update performed on 30 in answer module', '127.0.0.1', '2016-02-10 05:59:02'),
(162, 'admin', 'Action update performed on 54 in question module', '127.0.0.1', '2016-02-10 06:22:10'),
(163, 'admin', 'Action update performed on 55 in question module', '127.0.0.1', '2016-02-10 06:22:26'),
(164, 'admin', 'Action update performed on 56 in question module', '127.0.0.1', '2016-02-10 06:23:06'),
(165, 'admin', 'Action update performed on 57 in question module', '127.0.0.1', '2016-02-10 06:24:10'),
(166, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-11 06:37:39'),
(167, 'admin', 'Action update performed on 58 in question module', '127.0.0.1', '2016-02-11 06:40:59'),
(168, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-11 06:41:06'),
(169, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-11 06:42:40'),
(170, 'admin', 'Action update performed on 6 in user module', '127.0.0.1', '2016-02-10 21:14:06'),
(171, 'admin', 'Action update performed on 6 in user module', '127.0.0.1', '2016-02-10 21:17:08'),
(172, 'admin', 'Action update performed on 6 in user module', '127.0.0.1', '2016-02-10 21:17:19'),
(173, 'admin', 'Action update performed on 7 in user module', '127.0.0.1', '2016-02-10 21:37:30'),
(174, 'admin', 'Action update performed on 8 in user module', '127.0.0.1', '2016-02-10 21:41:32'),
(175, 'admin', 'Action update performed on 8 in user module', '127.0.0.1', '2016-02-10 21:42:39'),
(176, 'admin', 'Action update performed on 11 in user module', '127.0.0.1', '2016-02-10 21:57:42'),
(177, 'admin', 'Action update performed on 10 in user module', '127.0.0.1', '2016-02-10 21:59:13'),
(178, 'admin', 'Action update performed on 10 in user module', '127.0.0.1', '2016-02-10 22:00:05'),
(179, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-10 22:34:51'),
(180, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-10 22:36:54'),
(181, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-10 23:47:34'),
(182, 'admin', 'Action update performed on 13 in user module', '127.0.0.1', '2016-02-10 23:48:33'),
(183, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:04:04'),
(184, 'admin', 'Action update performed on  in user module', '127.0.0.1', '2016-02-11 00:06:22'),
(185, 'admin', 'Action update performed on 0 in user module', '127.0.0.1', '2016-02-11 00:10:59'),
(186, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:11:18'),
(187, 'admin', 'Action update performed on  in user module', '127.0.0.1', '2016-02-11 00:12:00'),
(188, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:16:00'),
(189, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:19:20'),
(190, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:27:52'),
(191, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:29:09'),
(192, 'admin', 'Action update performed on 20 in user module', '127.0.0.1', '2016-02-11 00:29:48'),
(193, 'admin', 'Action update performed on 5 in user module', '127.0.0.1', '2016-02-11 00:37:22'),
(194, 'admin', 'Action update performed on  $id in user module', '127.0.0.1', '2016-02-11 00:40:03'),
(195, 'admin', 'Action update performed on 13 in user module', '127.0.0.1', '2016-02-11 03:04:56'),
(196, 'admin', 'Action update performed on 23 in user module', '127.0.0.1', '2016-02-11 03:12:14'),
(197, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 05:34:22'),
(198, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 05:37:45'),
(199, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-12 07:11:39'),
(200, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-12 07:12:07'),
(201, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-12 07:14:03'),
(202, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-12 07:16:07'),
(203, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-12 07:19:37'),
(204, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:08:41'),
(205, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:08:51'),
(206, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:13:49'),
(207, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:13:54'),
(208, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:15:04'),
(209, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:21:18'),
(210, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:22:32'),
(211, 'admin', 'Action update performed on  in prize module', '127.0.0.1', '2016-02-11 21:25:17'),
(212, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:32:19'),
(213, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:33:47'),
(214, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:33:54'),
(215, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:34:03'),
(216, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:35:48'),
(217, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:37:05'),
(218, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:38:51'),
(219, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 21:52:36'),
(220, 'admin', 'Action update performed on 6 in prize module', '127.0.0.1', '2016-02-11 22:02:36'),
(221, 'admin', 'Action update performed on 6 in prize module', '127.0.0.1', '2016-02-11 22:02:46'),
(222, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 22:03:53'),
(223, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 22:04:33'),
(224, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 22:04:41'),
(225, 'admin', 'Action update performed on 4 in prize module', '127.0.0.1', '2016-02-11 22:04:46'),
(226, 'admin', 'Action update performed on 3 in prize module', '127.0.0.1', '2016-02-11 22:23:23'),
(227, 'admin', 'Action update performed on 8 in prize module', '127.0.0.1', '2016-02-11 22:25:35'),
(228, 'admin', 'Action update performed on 1 in prize module', '127.0.0.1', '2016-02-11 22:36:48'),
(229, 'admin', 'Action update performed on 15 in prize module', '127.0.0.1', '2016-02-11 22:47:12'),
(230, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-11 23:02:40'),
(231, 'admin', 'Action update performed on 18 in prize module', '127.0.0.1', '2016-02-11 23:02:51'),
(232, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-14 03:32:00'),
(233, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-14 03:32:04'),
(234, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-14 03:34:03'),
(235, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-14 04:07:19'),
(236, 'admin', 'Action update performed on 12 in answer module', '127.0.0.1', '2016-02-14 21:45:10'),
(237, 'admin', 'Action update performed on 29 in answer module', '127.0.0.1', '2016-02-14 22:03:02'),
(238, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-14 23:43:54'),
(239, 'admin', 'Action update performed on 52 in question module', '127.0.0.1', '2016-02-15 00:17:24'),
(240, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 00:17:57'),
(241, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 03:02:14'),
(242, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-15 03:03:23'),
(243, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-15 03:03:32'),
(244, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-15 03:03:40'),
(245, 'admin', 'Action update performed on 47 in question module', '127.0.0.1', '2016-02-15 03:03:48'),
(246, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-15 03:03:55'),
(247, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-15 03:04:04'),
(248, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 03:04:12'),
(249, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 04:34:30'),
(250, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-15 04:34:43'),
(251, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-15 04:34:51'),
(252, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 05:08:12'),
(253, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-15 05:08:17'),
(254, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-15 05:08:25'),
(255, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 05:08:41'),
(256, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-15 05:08:53'),
(257, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-15 05:09:13'),
(258, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-15 05:09:30'),
(259, 'admin', 'Action update performed on 60 in question module', '127.0.0.1', '2016-02-15 05:10:21'),
(260, 'admin', 'Action update performed on 60 in question module', '127.0.0.1', '2016-02-15 05:10:34'),
(261, 'admin', 'Action update performed on 60 in question module', '127.0.0.1', '2016-02-15 05:10:58'),
(262, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-15 05:14:40'),
(263, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-15 05:14:50'),
(264, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-15 05:15:05'),
(265, 'admin', 'Action update performed on 60 in question module', '127.0.0.1', '2016-02-15 05:15:29'),
(266, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 05:31:50'),
(267, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 06:00:52'),
(268, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-15 06:05:39'),
(269, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-15 06:05:46'),
(270, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 06:23:15'),
(271, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-16 06:51:47'),
(272, 'admin', 'Action update performed on 47 in question module', '127.0.0.1', '2016-02-16 06:51:59'),
(273, 'admin', 'Action update performed on 47 in question module', '127.0.0.1', '2016-02-16 07:06:49'),
(274, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-15 19:49:52'),
(275, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 23:53:32'),
(276, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 23:53:53'),
(277, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-15 23:54:03'),
(278, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-16 00:11:34'),
(279, 'admin', 'Action update performed on 16 in prize module', '127.0.0.1', '2016-02-16 05:18:07'),
(280, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-16 05:18:18'),
(281, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-16 05:18:29'),
(282, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-16 05:18:47'),
(283, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-16 05:18:59'),
(284, 'admin', 'Action update performed on 37 in answer module', '127.0.0.1', '2016-02-16 05:19:22'),
(285, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-16 05:19:32'),
(286, 'admin', 'Action update performed on 18 in prize module', '127.0.0.1', '2016-02-16 05:35:27'),
(287, 'admin', 'Action update performed on 39 in category module', '127.0.0.1', '2016-02-16 05:37:03'),
(288, 'admin', 'Action update performed on 17 in prize module', '127.0.0.1', '2016-02-16 06:13:32'),
(289, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:40:12'),
(290, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:40:20'),
(291, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:46:00'),
(292, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:46:04'),
(293, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 05:46:35'),
(294, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 05:46:39'),
(295, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:46:59'),
(296, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:47:55'),
(297, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:48:37'),
(298, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 05:48:40'),
(299, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 06:08:10'),
(300, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 06:10:19'),
(301, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 06:10:23'),
(302, 'admin', 'Action update performed on 44 in question module', '127.0.0.1', '2016-02-17 06:10:35'),
(303, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 06:10:41'),
(304, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 06:10:47'),
(305, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 06:27:37'),
(306, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 06:27:46'),
(307, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 06:27:57'),
(308, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-18 06:35:50'),
(309, 'admin', 'Action update performed on 58 in answer module', '127.0.0.1', '2016-02-18 06:48:45'),
(310, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 19:36:08'),
(311, 'admin', 'Action update performed on 55 in answer module', '127.0.0.1', '2016-02-17 21:13:42'),
(312, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-17 21:15:53'),
(313, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-17 21:16:04'),
(314, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 21:27:09'),
(315, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-17 21:58:10'),
(316, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-17 22:23:58'),
(317, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-17 22:24:02'),
(318, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-17 22:24:20'),
(319, 'admin', 'Action update performed on 45 in question module', '127.0.0.1', '2016-02-17 22:56:48'),
(320, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 22:57:02'),
(321, 'admin', 'Action update performed on 38 in question module', '127.0.0.1', '2016-02-17 22:57:06'),
(322, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-17 23:06:13'),
(323, 'admin', 'Action update performed on 28 in category module', '127.0.0.1', '2016-02-18 20:00:23'),
(324, 'admin', 'Action update performed on 28 in category module', '127.0.0.1', '2016-02-18 21:35:21'),
(325, 'admin', 'Action update performed on 28 in category module', '127.0.0.1', '2016-02-18 23:01:36'),
(326, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-18 23:28:35'),
(327, 'admin', 'Action update performed on 50 in question module', '127.0.0.1', '2016-02-18 23:28:50'),
(328, 'admin', 'Action update performed on 42 in category module', '127.0.0.1', '2016-02-21 04:30:07'),
(329, 'admin', 'Action update performed on 42 in category module', '127.0.0.1', '2016-02-21 04:30:46'),
(330, 'admin', 'Action update performed on 46 in question module', '127.0.0.1', '2016-02-21 04:56:56'),
(331, 'admin', 'Action update performed on 49 in question module', '127.0.0.1', '2016-02-21 04:57:09'),
(332, 'admin', 'Action update performed on 48 in question module', '127.0.0.1', '2016-02-21 04:59:21'),
(333, 'admin', 'Action update performed on 66 in answer module', '127.0.0.1', '2016-02-22 05:38:57'),
(334, 'admin', 'Action update performed on 70 in answer module', '127.0.0.1', '2016-02-22 05:41:12'),
(335, 'admin', 'Action update performed on 9 in content module', '127.0.0.1', '2016-02-23 06:44:53'),
(336, 'admin', 'Action update performed on 44 in category module', '127.0.0.1', '2016-02-23 06:46:09'),
(337, 'admin', 'Action update performed on 24 in category module', '127.0.0.1', '2016-02-23 04:57:01'),
(338, 'admin', 'Action update performed on 18 in prize module', '122.176.82.67', '2016-03-04 08:28:20'),
(339, 'admin', 'Action update performed on 1 in content module', '122.173.175.131', '2016-03-06 13:46:57'),
(340, 'admin', 'Action update performed on 4 in content module', '122.176.82.67', '2016-03-06 13:47:31'),
(341, 'admin', 'Action update performed on 5 in content module', '122.176.82.67', '2016-03-06 13:48:18'),
(342, 'admin', 'Action update performed on 6 in content module', '122.176.82.67', '2016-03-06 13:48:36'),
(343, 'admin', 'Action update performed on 22 in prize module', '122.173.175.131', '2016-03-06 13:49:18'),
(344, 'admin', 'Action update performed on 7 in user module', '122.176.82.67', '2016-03-06 13:50:13'),
(345, 'admin', 'Action update performed on 10 in content module', '122.173.175.131', '2016-03-06 14:21:54'),
(346, 'admin', 'Action update performed on 39 in category module', '122.173.49.155', '2016-03-09 11:21:18'),
(347, 'admin', 'Action update performed on 30 in category module', '122.176.82.67', '2016-03-09 11:21:31'),
(348, 'admin', 'Action update performed on 38 in question module', '122.176.82.67', '2016-03-09 11:22:51'),
(349, 'admin', 'Action update performed on 44 in question module', '122.176.82.67', '2016-03-09 11:22:58'),
(350, 'admin', 'Action update performed on 45 in question module', '122.176.82.67', '2016-03-09 11:23:07'),
(351, 'admin', 'Action update performed on 46 in question module', '122.176.82.67', '2016-03-09 11:23:16'),
(352, 'admin', 'Action update performed on 46 in question module', '122.176.82.67', '2016-03-09 11:23:25'),
(353, 'admin', 'Action update performed on 47 in question module', '122.176.82.67', '2016-03-09 11:23:34'),
(354, 'admin', 'Action update performed on 48 in question module', '122.173.49.155', '2016-03-09 11:23:49'),
(355, 'admin', 'Action update performed on 49 in question module', '122.173.49.155', '2016-03-09 11:24:03'),
(356, 'admin', 'Action update performed on 48 in question module', '122.176.82.67', '2016-03-09 11:24:18'),
(357, 'admin', 'Action update performed on 45 in question module', '122.173.49.155', '2016-03-09 11:24:36'),
(358, 'admin', 'Action update performed on 50 in question module', '122.173.49.155', '2016-03-09 11:24:58'),
(359, 'admin', 'Action update performed on 51 in question module', '122.173.49.155', '2016-03-09 11:25:09'),
(360, 'admin', 'Action update performed on 52 in question module', '122.176.82.67', '2016-03-09 11:25:24'),
(361, 'admin', 'Action update performed on 46 in question module', '122.173.206.135', '2016-03-10 08:52:17'),
(362, 'admin', 'Action update performed on 47 in question module', '122.173.206.135', '2016-03-10 08:52:30'),
(363, 'admin', 'Action update performed on 48 in question module', '122.173.206.135', '2016-03-10 08:52:37'),
(364, 'admin', 'Action update performed on 48 in question module', '122.173.206.135', '2016-03-10 08:52:43'),
(365, 'admin', 'Action update performed on 49 in question module', '122.173.206.135', '2016-03-10 08:52:54'),
(366, 'admin', 'Action update performed on 50 in question module', '122.176.82.67', '2016-03-10 08:53:06'),
(367, 'admin', 'Action update performed on 51 in question module', '122.176.82.67', '2016-03-10 08:53:26'),
(368, 'admin', 'Action update performed on 52 in question module', '122.173.206.135', '2016-03-10 08:53:40'),
(369, 'admin', 'Action update performed on 75 in answer module', '122.173.206.135', '2016-03-10 08:55:27'),
(370, 'admin', 'Action update performed on 83 in answer module', '122.173.206.135', '2016-03-10 08:59:18'),
(371, 'admin', 'Action update performed on 61 in answer module', '122.176.82.67', '2016-03-10 10:14:26'),
(372, 'admin', 'Action update performed on 94 in answer module', '122.176.82.67', '2016-03-10 10:16:40'),
(373, 'admin', 'Action update performed on 38 in question module', '122.176.82.67', '2016-03-11 10:46:32'),
(374, 'admin', 'Action update performed on 44 in question module', '122.176.82.67', '2016-03-11 10:46:41'),
(375, 'admin', 'Action update performed on 45 in question module', '122.176.82.67', '2016-03-11 10:46:47'),
(376, 'admin', 'Action update performed on 46 in question module', '122.176.82.67', '2016-03-11 10:46:53'),
(377, 'admin', 'Action update performed on 47 in question module', '122.176.82.67', '2016-03-11 10:47:00'),
(378, 'admin', 'Action update performed on 48 in question module', '122.176.82.67', '2016-03-11 10:47:06'),
(379, 'admin', 'Action update performed on 49 in question module', '122.176.82.67', '2016-03-11 10:47:11'),
(380, 'admin', 'Action update performed on 50 in question module', '122.176.82.67', '2016-03-11 10:47:19'),
(381, 'admin', 'Action update performed on 51 in question module', '122.176.82.67', '2016-03-11 10:47:27'),
(382, 'admin', 'Action update performed on 52 in question module', '122.176.82.67', '2016-03-11 10:47:33'),
(383, 'admin', 'Action update performed on 53 in question module', '122.176.82.67', '2016-03-11 10:47:41'),
(384, 'admin', 'Action update performed on 54 in question module', '122.176.82.67', '2016-03-11 10:47:47'),
(385, 'admin', 'Action update performed on 55 in question module', '122.176.82.67', '2016-03-11 10:47:54'),
(386, 'admin', 'Action update performed on 56 in question module', '122.176.82.67', '2016-03-11 10:48:04'),
(387, 'admin', 'Action update performed on 57 in question module', '122.176.82.67', '2016-03-11 10:48:18'),
(388, 'admin', 'Action update performed on 58 in question module', '122.176.82.67', '2016-03-11 10:48:24'),
(389, 'admin', 'Action update performed on 59 in question module', '122.176.82.67', '2016-03-11 10:48:32'),
(390, 'admin', 'Action update performed on 60 in question module', '122.176.82.67', '2016-03-11 10:48:38'),
(391, 'admin', 'Action update performed on 60 in question module', '122.176.82.67', '2016-03-11 11:04:04'),
(392, 'admin', 'Action update performed on 38 in question module', '::1', '2016-03-31 07:18:59'),
(393, 'admin', 'Action update performed on  in setting module', '::1', '2016-04-05 20:51:54'),
(394, 'admin', 'Action update performed on  in setting module', '::1', '2016-04-07 06:59:40'),
(395, 'admin', 'Action update performed on  in level module', '::1', '2016-04-10 20:05:57'),
(396, 'admin', 'Action update performed on  in level module', '::1', '2016-04-10 20:07:55'),
(397, 'admin', 'Action update performed on 45 in question module', '::1', '2016-05-18 22:38:47'),
(398, 'admin', 'Action update performed on 16 in price module', '::1', '2016-05-19 00:16:26'),
(399, 'admin', 'Action update performed on 16 in prize module', '::1', '2016-05-19 00:16:44'),
(400, 'admin', 'Action update performed on 3 in level module', '::1', '2016-05-19 00:18:47'),
(401, 'admin', 'Action update performed on 3 in level module', '::1', '2016-05-19 00:19:16'),
(402, 'admin', 'Action update performed on 3 in level module', '::1', '2016-05-19 00:19:23'),
(403, 'admin', 'Action update performed on 6 in level module', '::1', '2016-05-19 00:26:16'),
(404, 'admin', 'Action update performed on 7 in level module', '::1', '2016-05-19 00:26:22'),
(405, 'admin', 'Action update performed on 8 in level module', '::1', '2016-05-19 00:26:28'),
(406, 'admin', 'Action update performed on 8 in level module', '::1', '2016-05-19 00:26:35'),
(407, 'admin', 'Action update performed on 9 in level module', '::1', '2016-05-19 00:26:43'),
(408, 'admin', 'Action update performed on 10 in level module', '::1', '2016-05-19 00:26:54'),
(409, 'admin', 'Action update performed on 11 in level module', '::1', '2016-05-19 00:27:05'),
(410, 'admin', 'Action update performed on 14 in level module', '::1', '2016-05-19 00:27:10'),
(411, 'admin', 'Action update performed on 12 in level module', '::1', '2016-05-19 00:27:37'),
(412, 'admin', 'Action update performed on 14 in level module', '::1', '2016-05-19 00:27:42'),
(413, 'admin', 'Action update performed on 15 in level module', '::1', '2016-05-19 00:27:49'),
(414, 'admin', 'Action update performed on 13 in level module', '::1', '2016-05-19 00:27:54'),
(415, 'admin', 'Action update performed on 11 in level module', '::1', '2016-05-19 00:28:00'),
(416, 'admin', 'Action update performed on 62 in question module', '::1', '2016-05-19 00:36:37'),
(417, 'admin', 'Action update performed on 62 in question module', '::1', '2016-05-19 00:37:33'),
(418, 'admin', 'Action update performed on 38 in question module', '::1', '2016-05-19 00:38:14'),
(419, 'admin', 'Action update performed on 44 in question module', '::1', '2016-05-19 00:38:23'),
(420, 'admin', 'Action update performed on 45 in question module', '::1', '2016-05-19 00:38:52'),
(421, 'admin', 'Action update performed on 48 in question module', '::1', '2016-05-19 00:39:19'),
(422, 'admin', 'Action update performed on 60 in question module', '::1', '2016-05-19 00:39:33'),
(423, 'admin', 'Action update performed on 61 in question module', '::1', '2016-05-19 00:39:46'),
(424, 'admin', 'Action update performed on 46 in question module', '::1', '2016-05-19 00:40:30'),
(425, 'admin', 'Action update performed on 47 in question module', '::1', '2016-05-19 00:40:35'),
(426, 'admin', 'Action update performed on 46 in question module', '::1', '2016-05-19 00:40:44'),
(427, 'admin', 'Action update performed on 47 in question module', '::1', '2016-05-19 00:40:47'),
(428, 'admin', 'Action update performed on 50 in question module', '::1', '2016-05-19 00:40:49'),
(429, 'admin', 'Action update performed on 51 in question module', '::1', '2016-05-19 00:40:59'),
(430, 'admin', 'Action update performed on 60 in question module', '::1', '2016-05-19 00:41:38'),
(431, 'admin', 'Action update performed on 48 in question module', '::1', '2016-05-19 01:32:35'),
(432, 'admin', 'Action update performed on 60 in question module', '::1', '2016-05-19 01:32:37'),
(433, 'admin', 'Action update performed on 61 in question module', '::1', '2016-05-19 01:32:44'),
(434, 'admin', 'Action update performed on 46 in question module', '::1', '2016-05-19 01:33:14'),
(435, 'admin', 'Action update performed on 61 in question module', '::1', '2016-05-19 01:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_admin_user`
--

CREATE TABLE IF NOT EXISTS `cwebc_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_role_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_sign_in` date DEFAULT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `is_deleted` enum('1','0') NOT NULL DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `last_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `allow_pages` varchar(255) NOT NULL DEFAULT '*',
  `is_loggedin` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `admin_role_id` (`admin_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cwebc_admin_user`
--

INSERT INTO `cwebc_admin_user` (`id`, `admin_role_id`, `username`, `password`, `phone`, `email`, `last_sign_in`, `is_active`, `is_deleted`, `last_update_date`, `ip_address`, `last_access`, `allow_pages`, `is_loggedin`) VALUES
(4, NULL, 'admin', 'admin', NULL, 'admin@gmail.com', NULL, '0', '0', NULL, NULL, '2016-02-29 05:41:11', '*', '0'),
(5, NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, '1', '0', NULL, NULL, '2016-05-18 04:38:20', '*', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_admin_user_activity_log`
--

CREATE TABLE IF NOT EXISTS `cwebc_admin_user_activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_user_id` int(11) NOT NULL,
  `module` varchar(50) NOT NULL,
  `module_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `action_comment` longtext NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `admin_user_id` (`admin_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_admin_user_role`
--

CREATE TABLE IF NOT EXISTS `cwebc_admin_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cwebc_admin_user_role`
--

INSERT INTO `cwebc_admin_user_role` (`id`, `name`, `date_upd`) VALUES
(1, 'Super User', '2015-02-11 08:48:23');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_category`
--

CREATE TABLE IF NOT EXISTS `cwebc_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `urlname` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` enum('1','0') NOT NULL DEFAULT '0',
  `meta_title` varchar(255) NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `cwebc_category`
--

INSERT INTO `cwebc_category` (`id`, `name`, `urlname`, `image`, `description`, `position`, `parent_id`, `is_active`, `is_deleted`, `meta_title`, `meta_desc`, `meta_keyword`, `date_add`, `date_upd`) VALUES
(30, 'History', 'history', '', '&lt;p&gt;Test information&lt;/p&gt;\r\n', 2, 0, 1, '0', '', 'T', '', '2016-03-09 06:21:31', '2016-01-29 12:26:07'),
(39, 'Sports', 'sports', '', '', 0, 0, 1, '0', '', '', '', '2016-03-09 06:21:18', '2016-02-19 12:35:39');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_city`
--

CREATE TABLE IF NOT EXISTS `cwebc_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `state_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `state_id` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_contactus`
--

CREATE TABLE IF NOT EXISTS `cwebc_contactus` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(255) NOT NULL,
  `massage` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cwebc_contactus`
--

INSERT INTO `cwebc_contactus` (`id`, `firstname`, `lastname`, `email`, `phone`, `massage`) VALUES
(1, 'user', 'user', 'user@gmail.com', 2147483647, 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_content`
--

CREATE TABLE IF NOT EXISTS `cwebc_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `urlname` varchar(255) DEFAULT NULL,
  `page` longtext,
  `short_description` text,
  `meta_keyword` text,
  `meta_description` text,
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_name` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `meta_robots_index` enum('global','index','noindex') NOT NULL DEFAULT 'global',
  `meta_robots_follow` enum('global','follow','nofollow') NOT NULL DEFAULT 'follow',
  `include_xml_sitemap` enum('global','include','exclude') NOT NULL DEFAULT 'global',
  `sitemap_priority` varchar(11) NOT NULL DEFAULT 'global',
  `canonical` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `cwebc_content`
--

INSERT INTO `cwebc_content` (`id`, `name`, `urlname`, `page`, `short_description`, `meta_keyword`, `meta_description`, `publish_date`, `page_name`, `position`, `is_deleted`, `image`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`) VALUES
(1, 'How it works ', 'how-it-works-', '&lt;h2&gt;Shopping&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is&lt;img alt=&quot;&quot; /&gt;&amp;nbsp;that it has a more-or-less normal&lt;/p&gt;\r\n', '', '', '', '2016-03-07 04:46:57', 'Home Page Main Content', 0, 0, NULL, 'index', 'follow', 'global', '', ''),
(2, 'About Us', 'about-us', '&lt;h3 style=&quot;margin: 10px 0px;&quot;&gt;Learn more about Shopping &lt;/h3&gt;\n\n&lt;h5 style=&quot;margin: 5px 0px 10px;&quot;&gt;Selection. Prices. Service.&lt;/h5&gt;\n\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Whether this is visit #1 or visit #1000, Shopping  has always prided itself on giving you a selection of the hottest products at the most competitive prices on the net. It is our goal that you enjoy a discreet and efficient shopping experience.&lt;/p&gt;\n\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;We have the belief that a premium shopping experience is an end-to-end journey. From the moment you begin searching for products to the checkout process to the delivery of your goods, we aim to have an informative, relaxed and convenient atmosphere.&lt;/p&gt;\n\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Shopping  has been an online retailer since 1999, serving over 500,000 customers and counting. Every product presented in our store is carefully selected so that you can feel confident that you&amp;#39;re picking the right product for your particular sexual experience.&lt;/p&gt;\n\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Explore the many categories contained within Shopping &lt;/p&gt;\n\n&lt;ul style=&quot;line-height: 20.7999992370605px;&quot;&gt;\n	&lt;li style=&quot;box-sizing: border-box;&quot;&gt;Enjoy role play with our selection of lingerie and clothing&lt;/li&gt;\n	&lt;li style=&quot;box-sizing: border-box;&quot;&gt;Turn your bedroom into an adventurous playground with our sex swings and aids&lt;/li&gt;\n	&lt;li style=&quot;box-sizing: border-box;&quot;&gt;Dabble in the world of fetish with our selection of light bondage products&lt;/li&gt;\n	&lt;li style=&quot;box-sizing: border-box;&quot;&gt;Have a favorite brand? We serve products from some of the biggest names&lt;/li&gt;\n	&lt;li style=&quot;box-sizing: border-box;&quot;&gt;We have popular vibes that will rock you from the G-Spot to the Prostate&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;No matter your sexual preference, Shopping  believes that every sexual experience can be a safe, consensual and confident event. We appreciate you stopping by our store and invite you to continue taking a look around. Who knows: what you find here may allure and entice you, just like what could happen when you use it could delight and surprise you.&lt;/p&gt;\n', '', '', '', '2015-11-26 08:59:45', 'About Us', 0, 0, NULL, 'index', 'follow', 'global', '', ''),
(3, 'Contact Us', 'contact-us', '', '', '', '', '2016-03-14 14:13:25', 'Contact Us', 0, 1, NULL, 'index', 'follow', 'global', '', ''),
(4, 'Tit Bits from Around the World', 'tit-bits-from-around-the-world', '&lt;div class=&quot;page-title&quot;&gt;\r\n&lt;h1&gt;Customer Service&lt;/h1&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;ul class=&quot;disc&quot;&gt;\r\n	&lt;li&gt;&lt;a href=&quot;/privacy-policy/&quot;&gt;Privacy &amp;amp; Security&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;/shipping-rates/&quot;&gt;Shipping Rates&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;/satisfaction-guarantee/&quot;&gt;Returns &amp;amp; Exchange Policy&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;/shipping-rates/&quot;&gt;Shipping &amp;amp; Handling&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;/satisfaction-guarantee/&quot;&gt;Our 100% Satisfaction Guarantee&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '', '', '2016-03-07 04:47:31', 'Help', 0, 0, NULL, 'index', 'follow', 'global', '', ''),
(5, 'Community', 'community', '&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;We respect your privacy and guarantee the information you give us will never be sold, rented or given away. Our site&amp;#39;s order form asks you to give us contact information, including your billing address, shipping address, credit card information, and email address. We use the information provided to facilitate any subsequent orders. We do not make our list of registered customers available to other companies. Our promise to you is outlined in the following privacy statement:&lt;/p&gt;\r\n\r\n&lt;ul style=&quot;line-height: 20.7999992370605px;&quot;&gt;\r\n	&lt;li&gt;We do not read your private online communications.&lt;/li&gt;\r\n	&lt;li&gt;We do not use any information about where you personally go on the Web, and we do not give it out to others.&lt;/li&gt;\r\n	&lt;li&gt;We do not give out your telephone number, credit card information or screen names. And we give you the opportunity to correct your personal contact and billing information at any time.&lt;/li&gt;\r\n	&lt;li&gt;We do not give out this purchase data to others.&lt;/li&gt;\r\n	&lt;li&gt;We take extra steps to protect the safety and privacy.&lt;/li&gt;\r\n	&lt;li&gt;We use secure technology, privacy protection controls and restrictions on employee access in order to safeguard your personal information.&lt;/li&gt;\r\n	&lt;li&gt;Websites linking to this page have chosen to adopt this generic privacy policy as their own. This means that they agree to abide by the principles laid out below.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;h2&gt;Information that is gathered from visitors&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;In common with other websites, log files are stored on the web server saving details such as the visitor&amp;#39;s IP address, browser type, referring page and time of visit. Cookies may be used to remember visitor preferences when interacting with the website. Where registration is required, the visitor&amp;#39;s email and a username will be stored on the server.&lt;/p&gt;\r\n\r\n&lt;h2&gt;How the Information is used&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;The information is used to enhance the visitor&amp;rsquo;s experience when using the website to display personalized content and possibly advertising. E-mail addresses will not be sold, rented or leased to 3rd parties. E-mail may be sent to inform you of news of our services or offers by our affiliates or us.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Visitor Options&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;If you have subscribed to one of our services, you may unsubscribe by following the instructions, which are included in e-mail that you receive. You may be able to block cookies via your browser settings but this may prevent you from access to certain features of the website.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Cookies&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Cookies are small digital signature files that are stored by your web browser that allow your preferences to be recorded when visiting the website. Also they may be used to track your return visits to the website. 3rd party advertising companies may also use cookies for tracking purposes.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Google Ads&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Google, as a third party vendor, uses cookies to serve ads. Google&amp;#39;s use of the DART cookie enables it to serve ads to visitors based on their visit to sites they visit on the Internet. Website visitors may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Google Analytics&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;This website uses Google Analytics, a web analytics service provided by Google, Inc. (&amp;quot;Google&amp;quot;). Google Analytics uses &amp;quot;cookies&amp;quot;, which are text files placed on your computer, to help the website analyze how users use the site. The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google&amp;#39;s behalf. Google will not associate your IP address with any other data held by Google. You may refuse the use of cookies by selecting the appropriate settings on your browser, however please note that if you do this you may not be able to use the full functionality of this website. By using this website, you consent to the processing of data about you by Google in the manner and for the purposes set out above.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Policy Changes&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;By submitting your information you consent to the use of that information as set out in this Policy. If we change our Privacy Policy we will post the changes on this page, and may place notices on other pages of the web site, so that you may be aware of the information we collect and how we use it. Continued use of the service will signify that you agree to any such changes.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Contact Us&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;If you want to contact us regarding this Privacy Policy, or our collection and use of your personal information, please&amp;nbsp;&lt;a href=&quot;/contact&quot;&gt;contact us&lt;/a&gt;.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n', '', '', '', '2016-03-07 04:48:18', 'Privacy Statement', 0, 0, NULL, 'index', 'follow', 'global', '', ''),
(6, 'Terms &amp; Conditions ', 'terms---conditions-', '&lt;h2 style=&quot;margin-top: 0px;&quot;&gt;Disclaimer of Liability&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Shopping makes no warranty, expressed or implied, as to the safety of any of our products. All of our products are sold as novelties only. Furthermore, we accept no responsibility for injuries sustained while using our products. The purchaser assumes all risk and liability of use for all our products.&lt;/p&gt;\r\n\r\n&lt;h2&gt;18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;All models, actors, actresses and other persons that appear in any visual depiction of actual sexually explicit conduct appearing or otherwise contained in this Website were over the age of eighteen years at the time of the creation of such depictions. All other visual depictions displayed on this Website are exempt from the provision of 18 U.S.C. section 2257 and 28 C.F.R. 75 because said visual depictions do not consist of depictions of conduct as specifically listed in 18 U.S.C section 2256 (2) (A) through (D), but are merely depictions of non-sexually explicit nudity, or are depictions of simulated sexual conduct, or are otherwise exempt because the visual depictions were created prior to July 3, 1995. With respect to all visual depictions displayed on this website, whether of actual sexually explicit conduct, simulated sexual content or otherwise, all persons in said visual depictions were at least 18 years of age when said visual depictions were created.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;The records required pursuant to 18 USC 2257 pertaining to this website and all materials depicted hereon, are on file with the Custodian of Records:&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;Ari Suss 15251 Pipeline Lane Huntington Beach CA, 92649&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;The owners and operators of this Website are not the primary producer (as that term is defined in 18 USC section 2257) of any of the videos or streaming video content sold through or linked through this website.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Local Community Standards Information&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;The content of this site falls easily within the bounds of what is acceptable in the district of Orange County. Moreover, in the past 10 years, there has been no prosecution of an internet site based on obscenity. We hence believe that the content of this site falls within what is acceptable based on the local community standard of online space.&lt;/p&gt;\r\n', '', '', '', '2016-03-07 04:48:36', 'Terms of Use', 0, 0, NULL, 'index', 'follow', 'global', '', ''),
(7, 'hg', 'hg-7', NULL, NULL, NULL, NULL, '2016-02-22 05:43:06', NULL, 0, 1, NULL, 'global', 'follow', 'global', 'global', ''),
(8, ' cxc', '-cxc-8', NULL, NULL, NULL, NULL, '2016-02-23 08:12:26', NULL, 0, 1, NULL, 'global', 'follow', 'global', 'global', ''),
(9, 'dxsadcdscd', 'dxsadcdscd', '', NULL, '', '', '2016-02-23 08:14:57', '', 0, 1, NULL, 'global', 'follow', 'global', 'global', ''),
(10, 'Career', 'career', '&lt;p&gt;&lt;span style=&quot;line-height: 20.8px;&quot;&gt;The content of this site falls easily within the bounds of what is acceptable in the district of Orange County. Moreover, in the past 10 years, there has been no prosecution of an internet site based on obscenity. We hence believe that the content of this site falls within what is acceptable based on the local community standard of online space.&lt;/span&gt;&lt;/p&gt;\r\n', NULL, '', '', '2016-03-07 05:21:54', '', 0, 0, NULL, 'global', 'follow', 'global', 'global', '');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_country`
--

CREATE TABLE IF NOT EXISTS `cwebc_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso2` char(2) NOT NULL,
  `short_name` varchar(80) NOT NULL,
  `long_name` varchar(80) NOT NULL,
  `iso3` char(3) NOT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `flag` varchar(5) DEFAULT NULL,
  `contains_states` enum('0','1') NOT NULL DEFAULT '1',
  `need_zip_code` enum('0','1') NOT NULL DEFAULT '1',
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `cwebc_country`
--

INSERT INTO `cwebc_country` (`id`, `iso2`, `short_name`, `long_name`, `iso3`, `numcode`, `calling_code`, `flag`, `contains_states`, `need_zip_code`, `is_active`, `date_add`) VALUES
(1, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', '93', 'af', '1', '1', '0', '2015-11-26 08:59:45'),
(2, 'AX', 'Aland Islands', '&Aring;land Islands', 'ALA', '248', '358', 'ax', '1', '1', '0', '2015-11-26 08:59:45'),
(4, 'DZ', 'Algeria', 'People''s Democratic Republic of Algeria', 'DZA', '012', '213', 'dz', '1', '1', '0', '2015-11-26 08:59:45'),
(5, 'AS', 'American Samoa', 'American Samoa', 'ASM', '016', '1+684', 'as', '1', '1', '0', '2015-11-26 08:59:45'),
(6, 'AD', 'Andorra', 'Principality of Andorra', 'AND', '020', '376', 'ad', '1', '1', '0', '2015-11-26 08:59:45'),
(7, 'AO', 'Angola', 'Republic of Angola', 'AGO', '024', '244', 'ao', '1', '1', '0', '2015-11-26 08:59:45'),
(8, 'AI', 'Anguilla', 'Anguilla', 'AIA', '660', '1+264', 'ai', '1', '1', '0', '2015-11-26 08:59:45'),
(9, 'AQ', 'Antarctica', 'Antarctica', 'ATA', '010', '672', 'aq', '1', '1', '0', '2015-11-26 08:59:45'),
(10, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', '1+268', 'ag', '1', '1', '0', '2015-11-26 08:59:45'),
(11, 'AR', 'Argentina', 'Argentine Republic', 'ARG', '032', '54', 'ar', '1', '1', '0', '2015-11-26 08:59:45'),
(13, 'AW', 'Aruba', 'Aruba', 'ABW', '533', '297', 'aw', '1', '1', '0', '2015-11-26 08:59:45'),
(14, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', '61', 'au', '1', '1', '0', '2015-11-26 08:59:45'),
(15, 'AT', 'Austria', 'Republic of Austria', 'AUT', '040', '43', 'at', '1', '1', '0', '2015-11-26 08:59:45'),
(17, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', 'BHS', '044', '1+242', 'bs', '1', '1', '0', '2015-11-26 08:59:45'),
(19, 'BD', 'Bangladesh', 'People''s Republic of Bangladesh', 'BGD', '050', '880', 'bd', '1', '1', '0', '2015-11-26 08:59:45'),
(20, 'BB', 'Barbados', 'Barbados', 'BRB', '052', '1+246', 'bb', '1', '1', '0', '2015-11-26 08:59:45'),
(22, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', '32', 'be', '1', '1', '0', '2015-11-26 08:59:45'),
(23, 'BZ', 'Belize', 'Belize', 'BLZ', '084', '501', 'bz', '1', '1', '0', '2015-11-26 08:59:45'),
(24, 'BJ', 'Benin', 'Republic of Benin', 'BEN', '204', '229', 'bj', '1', '1', '0', '2015-11-26 08:59:45'),
(25, 'BM', 'Bermuda', 'Bermuda Islands', 'BMU', '060', '1+441', 'bm', '1', '1', '0', '2015-11-26 08:59:45'),
(26, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', '975', 'bt', '1', '1', '0', '2015-11-26 08:59:45'),
(27, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', '591', 'bo', '1', '1', '0', '2015-11-26 08:59:45'),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', '599', 'bq', '1', '1', '0', '2015-11-26 08:59:45'),
(30, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', '267', 'bw', '1', '1', '0', '2015-11-26 08:59:45'),
(31, 'BV', 'Bouvet Island', 'Bouvet Island', 'BVT', '074', 'NONE', 'bv', '1', '1', '0', '2015-11-26 08:59:45'),
(32, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', '55', 'br', '1', '1', '0', '2015-11-26 08:59:45'),
(33, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IOT', '086', '246', 'io', '1', '1', '0', '2015-11-26 08:59:45'),
(34, 'BN', 'Brunei', 'Brunei Darussalam', 'BRN', '096', '673', 'bn', '1', '1', '0', '2015-11-26 08:59:45'),
(36, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', '226', 'bf', '1', '1', '0', '2015-11-26 08:59:45'),
(37, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', '257', 'bi', '1', '1', '0', '2015-11-26 08:59:45'),
(38, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', '855', 'kh', '1', '1', '0', '2015-11-26 08:59:45'),
(39, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', '237', 'cm', '1', '1', '0', '2015-11-26 08:59:45'),
(40, 'CA', 'Canada', 'Canada', 'CAN', '124', '1', 'ca', '1', '1', '1', '2015-11-26 08:59:45'),
(41, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', '132', '238', 'cv', '1', '1', '0', '2015-11-26 08:59:45'),
(42, 'KY', 'Cayman Islands', 'The Cayman Islands', 'CYM', '136', '1+345', 'ky', '1', '1', '0', '2015-11-26 08:59:45'),
(44, 'TD', 'Chad', 'Republic of Chad', 'TCD', '148', '235', 'td', '1', '1', '0', '2015-11-26 08:59:45'),
(45, 'CL', 'Chile', 'Republic of Chile', 'CHL', '152', '56', 'cl', '1', '1', '0', '2015-11-26 08:59:45'),
(47, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', '61', 'cx', '1', '1', '0', '2015-11-26 08:59:45'),
(48, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', '61', 'cc', '1', '1', '0', '2015-11-26 08:59:45'),
(49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', '170', '57', 'co', '1', '1', '0', '2015-11-26 08:59:45'),
(50, 'KM', 'Comoros', 'Union of the Comoros', 'COM', '174', '269', 'km', '1', '1', '0', '2015-11-26 08:59:45'),
(51, 'CG', 'Congo', 'Republic of the Congo', 'COG', '178', '242', 'cg', '1', '1', '0', '2015-11-26 08:59:45'),
(52, 'CK', 'Cook Islands', 'Cook Islands', 'COK', '184', '682', 'ck', '1', '1', '0', '2015-11-26 08:59:45'),
(53, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', '506', 'cr', '1', '1', '0', '2015-11-26 08:59:45'),
(54, 'CI', 'Cote d''ivoire (Ivory Coast)', 'Republic of C&ocirc;te D''Ivoire (Ivory Coast)', 'CIV', '384', '225', 'ci', '1', '1', '0', '2015-11-26 08:59:45'),
(56, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', '53', 'cu', '1', '1', '0', '2015-11-26 08:59:45'),
(57, 'CW', 'Curacao', 'Cura&ccedil;ao', 'CUW', '531', '599', 'cw', '1', '1', '0', '2015-11-26 08:59:45'),
(60, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'COD', '180', '243', 'cd', '1', '1', '0', '2015-11-26 08:59:45'),
(61, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', '45', 'dk', '1', '1', '0', '2015-11-26 08:59:45'),
(62, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', '253', 'dj', '1', '1', '0', '2015-11-26 08:59:45'),
(63, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', '1+767', 'dm', '1', '1', '0', '2015-11-26 08:59:45'),
(64, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', '1+809, 8', 'do', '1', '1', '0', '2015-11-26 08:59:45'),
(65, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', '593', 'ec', '1', '1', '0', '2015-11-26 08:59:45'),
(67, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', '503', 'sv', '1', '1', '0', '2015-11-26 08:59:45'),
(68, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', '240', 'gq', '1', '1', '0', '2015-11-26 08:59:45'),
(69, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', '291', 'er', '1', '1', '0', '2015-11-26 08:59:45'),
(70, 'EE', 'Estonia', 'Republic of Estonia', 'EST', '233', '372', 'ee', '1', '1', '0', '2015-11-26 08:59:45'),
(71, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', '251', 'et', '1', '1', '0', '2015-11-26 08:59:45'),
(72, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FLK', '238', '500', 'fk', '1', '1', '0', '2015-11-26 08:59:45'),
(73, 'FO', 'Faroe Islands', 'The Faroe Islands', 'FRO', '234', '298', 'fo', '1', '1', '0', '2015-11-26 08:59:45'),
(74, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', '679', 'fj', '1', '1', '0', '2015-11-26 08:59:45'),
(75, 'FI', 'Finland', 'Republic of Finland', 'FIN', '246', '358', 'fi', '1', '1', '0', '2015-11-26 08:59:45'),
(76, 'FR', 'France', 'French Republic', 'FRA', '250', '33', 'fr', '1', '1', '0', '2015-11-26 08:59:45'),
(77, 'GF', 'French Guiana', 'French Guiana', 'GUF', '254', '594', 'gf', '1', '1', '0', '2015-11-26 08:59:45'),
(78, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', '689', 'pf', '1', '1', '0', '2015-11-26 08:59:45'),
(79, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', NULL, 'tf', '1', '1', '0', '2015-11-26 08:59:45'),
(80, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', '241', 'ga', '1', '1', '0', '2015-11-26 08:59:45'),
(81, 'GM', 'Gambia', 'Republic of The Gambia', 'GMB', '270', '220', 'gm', '1', '1', '0', '2015-11-26 08:59:45'),
(83, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', '49', 'de', '1', '1', '0', '2015-11-26 08:59:45'),
(85, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', '350', 'gi', '1', '1', '0', '2015-11-26 08:59:45'),
(86, 'GR', 'Greece', 'Hellenic Republic', 'GRC', '300', '30', 'gr', '1', '1', '0', '2015-11-26 08:59:45'),
(87, 'GL', 'Greenland', 'Greenland', 'GRL', '304', '299', 'gl', '1', '1', '0', '2015-11-26 08:59:45'),
(88, 'GD', 'Grenada', 'Grenada', 'GRD', '308', '1+473', 'gd', '1', '1', '0', '2015-11-26 08:59:45'),
(89, 'GP', 'Guadaloupe', 'Guadeloupe', 'GLP', '312', '590', 'gp', '1', '1', '0', '2015-11-26 08:59:45'),
(90, 'GU', 'Guam', 'Guam', 'GUM', '316', '1+671', 'gu', '1', '1', '0', '2015-11-26 08:59:45'),
(91, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', '502', 'gt', '1', '1', '0', '2015-11-26 08:59:45'),
(92, 'GG', 'Guernsey', 'Guernsey', 'GGY', '831', '44', 'gg', '1', '1', '0', '2015-11-26 08:59:45'),
(93, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', '224', 'gn', '1', '1', '0', '2015-11-26 08:59:45'),
(94, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', '245', 'gw', '1', '1', '0', '2015-11-26 08:59:45'),
(95, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', '592', 'gy', '1', '1', '0', '2015-11-26 08:59:45'),
(96, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', '509', 'ht', '1', '1', '0', '2015-11-26 08:59:45'),
(97, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'NONE', 'hm', '1', '1', '0', '2015-11-26 08:59:45'),
(98, 'HN', 'Honduras', 'Republic of Honduras', 'HND', '340', '504', 'hn', '1', '1', '0', '2015-11-26 08:59:45'),
(99, 'HK', 'Hong Kong', 'Hong Kong', 'HKG', '344', '852', 'hk', '1', '1', '0', '2015-11-26 08:59:45'),
(101, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', '354', 'is', '1', '1', '0', '2015-11-26 08:59:45'),
(102, 'IN', 'India', 'Republic of India', 'IND', '356', '91', 'in', '1', '1', '0', '2015-11-26 08:59:45'),
(106, 'IE', 'Ireland', 'Ireland', 'IRL', '372', '353', 'ie', '1', '1', '0', '2015-11-26 08:59:45'),
(107, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', '44', 'im', '1', '1', '0', '2015-11-26 08:59:45'),
(109, 'IT', 'Italy', 'Italian Republic', 'ITA', '380', '39', 'jm', '1', '1', '0', '2015-11-26 08:59:45'),
(110, 'JM', 'Jamaica', 'Jamaica', 'JAM', '388', '1+876', 'jm', '1', '1', '0', '2015-11-26 08:59:45'),
(111, 'JP', 'Japan', 'Japan', 'JPN', '392', '81', 'jp', '1', '1', '0', '2015-11-26 08:59:45'),
(112, 'JE', 'Jersey', 'The Bailiwick of Jersey', 'JEY', '832', '44', 'je', '1', '1', '0', '2015-11-26 08:59:45'),
(114, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', '7', 'kz', '1', '1', '0', '2015-11-26 08:59:45'),
(115, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', '254', 'ke', '1', '1', '0', '2015-11-26 08:59:45'),
(116, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', '686', 'ki', '1', '1', '0', '2015-11-26 08:59:45'),
(117, 'XK', 'Kosovo', 'Republic of Kosovo', '---', '---', '381', '', '1', '1', '0', '2015-11-26 08:59:45'),
(119, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', '417', '996', 'kg', '1', '1', '0', '2015-11-26 08:59:45'),
(120, 'LA', 'Laos', 'Lao People''s Democratic Republic', 'LAO', '418', '856', 'la', '1', '1', '0', '2015-11-26 08:59:45'),
(121, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', '371', 'lv', '1', '1', '0', '2015-11-26 08:59:45'),
(123, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', '266', 'ls', '1', '1', '0', '2015-11-26 08:59:45'),
(124, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', '231', 'lr', '1', '1', '0', '2015-11-26 08:59:45'),
(125, 'LY', 'Libya', 'Libya', 'LBY', '434', '218', 'ly', '1', '1', '0', '2015-11-26 08:59:45'),
(126, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', '423', 'li', '1', '1', '0', '2015-11-26 08:59:45'),
(128, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', '352', 'lu', '1', '1', '0', '2015-11-26 08:59:45'),
(129, 'MO', 'Macao', 'The Macao Special Administrative Region', 'MAC', '446', '853', 'mo', '1', '1', '0', '2015-11-26 08:59:45'),
(131, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', '261', 'mg', '1', '1', '0', '2015-11-26 08:59:45'),
(132, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', '265', 'mw', '1', '1', '0', '2015-11-26 08:59:45'),
(134, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', '960', 'mv', '1', '1', '0', '2015-11-26 08:59:45'),
(135, 'ML', 'Mali', 'Republic of Mali', 'MLI', '466', '223', 'ml', '1', '1', '0', '2015-11-26 08:59:45'),
(136, 'MT', 'Malta', 'Republic of Malta', 'MLT', '470', '356', 'mt', '1', '1', '0', '2015-11-26 08:59:45'),
(137, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', '692', 'mh', '1', '1', '0', '2015-11-26 08:59:45'),
(138, 'MQ', 'Martinique', 'Martinique', 'MTQ', '474', '596', 'mq', '1', '1', '0', '2015-11-26 08:59:45'),
(139, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', '222', 'mr', '1', '1', '0', '2015-11-26 08:59:45'),
(140, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', '230', 'mu', '1', '1', '0', '2015-11-26 08:59:45'),
(141, 'YT', 'Mayotte', 'Mayotte', 'MYT', '175', '262', 'yt', '1', '1', '0', '2015-11-26 08:59:45'),
(142, 'MX', 'Mexico', 'United Mexican States', 'MEX', '484', '52', 'mx', '1', '1', '0', '2015-11-26 08:59:45'),
(143, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', '691', 'fm', '1', '1', '0', '2015-11-26 08:59:45'),
(145, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', '377', 'mc', '1', '1', '0', '2015-11-26 08:59:45'),
(146, 'MN', 'Mongolia', 'Mongolia', 'MNG', '496', '976', 'mn', '1', '1', '0', '2015-11-26 08:59:45'),
(148, 'MS', 'Montserrat', 'Montserrat', 'MSR', '500', '1+664', 'ms', '1', '1', '0', '2015-11-26 08:59:45'),
(149, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', '212', 'ma', '1', '1', '0', '2015-11-26 08:59:45'),
(150, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', '258', 'mz', '1', '1', '0', '2015-11-26 08:59:45'),
(151, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MMR', '104', '95', 'mm', '1', '1', '0', '2015-11-26 08:59:45'),
(152, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', '264', 'na', '1', '1', '0', '2015-11-26 08:59:45'),
(153, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', '674', 'nr', '1', '1', '0', '2015-11-26 08:59:45'),
(154, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', '977', 'np', '1', '1', '0', '2015-11-26 08:59:45'),
(155, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', '31', 'nl', '1', '1', '0', '2015-11-26 08:59:45'),
(156, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', '687', 'nc', '1', '1', '0', '2015-11-26 08:59:45'),
(157, 'NZ', 'New Zealand', 'New Zealand', 'NZL', '554', '64', 'nz', '1', '1', '0', '2015-11-26 08:59:45'),
(158, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', '505', 'ni', '1', '1', '0', '2015-11-26 08:59:45'),
(159, 'NE', 'Niger', 'Republic of Niger', 'NER', '562', '227', 'ne', '1', '1', '0', '2015-11-26 08:59:45'),
(161, 'NU', 'Niue', 'Niue', 'NIU', '570', '683', 'nu', '1', '1', '0', '2015-11-26 08:59:45'),
(162, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', '672', 'nf', '1', '1', '0', '2015-11-26 08:59:45'),
(163, 'KP', 'North Korea', 'Democratic People''s Republic of Korea', 'PRK', '408', '850', 'kp', '1', '1', '0', '2015-11-26 08:59:45'),
(164, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', 'MNP', '580', '1+670', 'mp', '1', '1', '0', '2015-11-26 08:59:45'),
(165, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', '47', 'no', '1', '1', '0', '2015-11-26 08:59:45'),
(168, 'PW', 'Palau', 'Republic of Palau', 'PLW', '585', '680', 'pw', '1', '1', '0', '2015-11-26 08:59:45'),
(170, 'PA', 'Panama', 'Republic of Panama', 'PAN', '591', '507', 'pa', '1', '1', '0', '2015-11-26 08:59:45'),
(171, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', '675', 'pg', '1', '1', '0', '2015-11-26 08:59:45'),
(172, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', '595', 'py', '1', '1', '0', '2015-11-26 08:59:45'),
(173, 'PE', 'Peru', 'Republic of Peru', 'PER', '604', '51', 'pe', '1', '1', '0', '2015-11-26 08:59:45'),
(174, 'PH', 'Phillipines', 'Republic of the Philippines', 'PHL', '608', '63', 'ph', '1', '1', '0', '2015-11-26 08:59:45'),
(175, 'PN', 'Pitcairn', 'Pitcairn', 'PCN', '612', 'NONE', 'pn', '1', '1', '0', '2015-11-26 08:59:45'),
(177, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', '351', 'pt', '1', '1', '0', '2015-11-26 08:59:45'),
(178, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', '1+939', 'pr', '1', '1', '0', '2015-11-26 08:59:45'),
(180, 'RE', 'Reunion', 'R&eacute;union', 'REU', '638', '262', 're', '1', '1', '0', '2015-11-26 08:59:45'),
(183, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', '250', 'rw', '1', '1', '0', '2015-11-26 08:59:45'),
(184, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BLM', '652', '590', 'bl', '1', '1', '0', '2015-11-26 08:59:45'),
(185, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', '290', 'sh', '1', '1', '0', '2015-11-26 08:59:45'),
(186, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KNA', '659', '1+869', 'kn', '1', '1', '0', '2015-11-26 08:59:45'),
(187, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', '1+758', 'lc', '1', '1', '0', '2015-11-26 08:59:45'),
(188, 'MF', 'Saint Martin', 'Saint Martin', 'MAF', '663', '590', 'mf', '1', '1', '0', '2015-11-26 08:59:45'),
(189, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', '508', 'pm', '1', '1', '0', '2015-11-26 08:59:45'),
(190, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', '1+784', 'vc', '1', '1', '0', '2015-11-26 08:59:45'),
(191, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', '685', 'ws', '1', '1', '0', '2015-11-26 08:59:45'),
(192, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', '378', 'sm', '1', '1', '0', '2015-11-26 08:59:45'),
(193, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'STP', '678', '239', 'st', '1', '1', '0', '2015-11-26 08:59:45'),
(195, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', '221', 'sn', '1', '1', '0', '2015-11-26 08:59:45'),
(197, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', '248', 'sc', '1', '1', '0', '2015-11-26 08:59:45'),
(198, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', '232', 'sl', '1', '1', '0', '2015-11-26 08:59:45'),
(199, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', '65', 'sg', '1', '1', '0', '2015-11-26 08:59:45'),
(200, 'SX', 'Sint Maarten', 'Sint Maarten', 'SXM', '534', '1+721', 'sx', '1', '1', '0', '2015-11-26 08:59:45'),
(203, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', '677', 'sb', '1', '1', '0', '2015-11-26 08:59:45'),
(204, 'SO', 'Somalia', 'Somali Republic', 'SOM', '706', '252', 'so', '1', '1', '0', '2015-11-26 08:59:45'),
(207, 'KR', 'South Korea', 'Republic of Korea', 'KOR', '410', '82', 'kr', '1', '1', '0', '2015-11-26 08:59:45'),
(208, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', '211', 'ss', '1', '1', '0', '2015-11-26 08:59:45'),
(209, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', '34', 'es', '1', '1', '0', '2015-11-26 08:59:45'),
(210, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', '94', 'lk', '1', '1', '0', '2015-11-26 08:59:45'),
(211, 'SD', 'Sudan', 'Republic of the Sudan', 'SDN', '729', '249', 'sd', '1', '1', '0', '2015-11-26 08:59:45'),
(212, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', '597', 'sr', '1', '1', '0', '2015-11-26 08:59:45'),
(213, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJM', '744', '47', 'sj', '1', '1', '0', '2015-11-26 08:59:45'),
(214, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', '748', '268', 'sz', '1', '1', '0', '2015-11-26 08:59:45'),
(215, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', '46', 'se', '1', '1', '0', '2015-11-26 08:59:45'),
(216, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', '41', 'ch', '1', '1', '0', '2015-11-26 08:59:45'),
(218, 'TW', 'Taiwan', 'Republic of China (Taiwan)', 'TWN', '158', '886', 'tw', '1', '1', '0', '2015-11-26 08:59:45'),
(219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', '992', 'tj', '1', '1', '0', '2015-11-26 08:59:45'),
(220, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', '255', 'tz', '1', '1', '0', '2015-11-26 08:59:45'),
(221, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', '66', 'th', '1', '1', '0', '2015-11-26 08:59:45'),
(222, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TLS', '626', '670', 'tl', '1', '1', '0', '2015-11-26 08:59:45'),
(223, 'TG', 'Togo', 'Togolese Republic', 'TGO', '768', '228', 'tg', '1', '1', '0', '2015-11-26 08:59:45'),
(224, 'TK', 'Tokelau', 'Tokelau', 'TKL', '772', '690', 'tk', '1', '1', '0', '2015-11-26 08:59:45'),
(225, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', '676', 'to', '1', '1', '0', '2015-11-26 08:59:45'),
(226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', '1+868', 'tt', '1', '1', '0', '2015-11-26 08:59:45'),
(227, 'TN', 'Tunisia', 'Republic of Tunisia', 'TUN', '788', '216', 'tn', '1', '1', '0', '2015-11-26 08:59:45'),
(229, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', '993', 'tm', '1', '1', '0', '2015-11-26 08:59:45'),
(230, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', '1+649', 'tc', '1', '1', '0', '2015-11-26 08:59:45'),
(231, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', '688', 'tv', '1', '1', '0', '2015-11-26 08:59:45'),
(232, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', '256', 'ug', '1', '1', '0', '2015-11-26 08:59:45'),
(234, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', '971', 'ae', '1', '1', '0', '2015-11-26 08:59:45'),
(235, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GBR', '826', '44', 'uk', '1', '1', '0', '2015-11-26 08:59:45'),
(236, 'US', 'United States', 'United States of America', 'USA', '840', '1', 'us', '1', '1', '1', '2015-11-26 08:59:45'),
(237, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'NONE', 'NONE', '1', '1', '0', '2015-11-26 08:59:45'),
(238, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', '598', 'uy', '1', '1', '0', '2015-11-26 08:59:45'),
(239, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', '998', 'uz', '1', '1', '0', '2015-11-26 08:59:45'),
(240, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', '678', 'vu', '1', '1', '0', '2015-11-26 08:59:45'),
(241, 'VA', 'Vatican City', 'State of the Vatican City', 'VAT', '336', '39', 'va', '1', '1', '0', '2015-11-26 08:59:45'),
(242, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', '58', 've', '1', '1', '0', '2015-11-26 08:59:45'),
(243, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', '84', 'vn', '1', '1', '0', '2015-11-26 08:59:45'),
(244, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', '092', '1+284', 'vg', '1', '1', '0', '2015-11-26 08:59:45'),
(245, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', 'VIR', '850', '1+340', 'vi', '1', '1', '0', '2015-11-26 08:59:45'),
(246, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', '681', 'wf', '1', '1', '0', '2015-11-26 08:59:45'),
(247, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', '212', 'eh', '1', '1', '0', '2015-11-26 08:59:45'),
(249, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', '260', 'zm', '1', '1', '0', '2015-11-26 08:59:45'),
(250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', '263', 'zw', '1', '1', '0', '2015-11-26 08:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_email`
--

CREATE TABLE IF NOT EXISTS `cwebc_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_name` varchar(255) DEFAULT NULL,
  `email_fields` longtext,
  `email_subject` varchar(255) DEFAULT NULL,
  `email_text` text,
  `email_type` varchar(255) DEFAULT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `from_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `cwebc_email`
--

INSERT INTO `cwebc_email` (`id`, `email_name`, `email_fields`, `email_subject`, `email_text`, `email_type`, `is_deleted`, `from_name`) VALUES
(1, 'New Registration - Confirm email - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}', 'Welcome to {SITE_NAME}', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}.&lt;br /&gt;\r\n&lt;br /&gt;\r\nA new&amp;nbsp;account has been created using your email address.&lt;br /&gt;\r\n&lt;br /&gt;\r\nConfirm this by clicking the&amp;nbsp;following link.&lt;br /&gt;\r\n&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;Click Here&lt;/a&gt;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(2, 'New Registration - ADMIN', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}', 'New account created', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNEW account has been created on site..&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nFirst Name: {FIRSTNAME}&lt;br /&gt;\r\nLast Name: {LASTNAME}&lt;br /&gt;\r\nEmail: {EMAIL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(3, 'Verify Account - User', '{FIRSTNAME}, {LASTNAME}', 'Account Verified', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}..&lt;br /&gt;\r\n&lt;br /&gt;\r\nYour account has been activated successfully.\r\n&lt;/p&gt;\r\n\r\n', NULL, '0', '{SITE_NAME}'),
(4, 'Contact Us - Admin', '{NAME}, {EMAIL}, {SUBJECT}, {MESSAGE}, {DATE}, {TIME}, {IP_ADDRESS}', 'Contact Us Form Submitted', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNew contact us page has been submitted on website.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(5, 'Order Success - Paypal Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Order placed successfully&lt;/strong&gt;. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;br /&gt;\r\nOrder Amount: #{ORDER_AMOUNT}&lt;br /&gt;\r\nPayment Method: #{PAYMENT_METHOD}&lt;br /&gt;\r\nPayment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order will be shipped as soon as possible. check your&amp;nbsp;emails regularly for further updates.&lt;/p&gt;\r\n', NULL, '1', '{SITE_NAME}'),
(6, 'Order Success - Admin', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'New Order', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNew order has been placed on site. Find the order details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: #{ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: #{PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;Please check..&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(7, 'Forgot Password - User', '{FIRSTNAME}, {LASTNAME}, {CONFIRMATION_URL}', 'Forgot Password Request', '&lt;p&gt;Hi {FIRSTNAME}&amp;nbsp;{LASTNAME}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Your request has been&amp;nbsp;accepted for forgot password, Please follow the link below&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONFIRMATION_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(8, 'New Account Created - User', '{FIRSTNAME}, {LASTNAME}, {PASSWORD}', 'New Account Created', '&lt;p&gt;Hi {FIRSTNAME}&amp;nbsp;{LASTNAME}..&lt;/p&gt;\r\n\r\n&lt;p&gt;A new account has been created using you email address.&lt;/p&gt;\r\n\r\n&lt;p&gt;Password: {PASSWORD}&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have not created this account, Please contact us immediately.&amp;nbsp;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(9, 'Low Stock Notification - Admin', '{TABLE}', 'Low Stock Notification', '&lt;p&gt;Hi Admin..&lt;/p&gt;\r\n\r\n&lt;p&gt;Stock has been gone low for the following products.&lt;/p&gt;\r\n\r\n&lt;p&gt;{TABLE}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(10, 'Password Change - Customer', '{USER}, {USERID},{NEWPASSWORD},{IP_ADDRESS},{PASSWORD_CHANGE_DATE_TIME}', 'Password Change', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Your password has been recently updated on&amp;nbsp;&amp;nbsp;{PASSWORD_CHANGE_DATE_TIME} using&amp;nbsp;{IP_ADDRESS} IP Address&lt;/p&gt;\r\n\r\n&lt;p&gt;Find&amp;nbsp;the new login details below:&lt;/p&gt;\r\n\r\n&lt;p&gt;User Name&amp;nbsp;: {USERID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password: {NEWPASSWORD}&lt;/p&gt;\r\n\r\n&lt;p&gt;If&amp;nbsp;you had not changed the password, please login and change the password again&amp;nbsp;as soon as possible, also contact us for this&amp;nbsp;immediately.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(11, 'Newsletter Subscribed - Customer', '{USER}, {DATE_TIME}', 'Newsletter Subscribed', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;You have successfully subscribed for our newsletter.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(12, 'Newsletter Unsubscribed - Customer', '{USER}, {DATE_TIME}', 'Newsletter Unsubscribed', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;You have successfully unsubscribed for our newsletter.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(13, 'Order Shipping Details - Customer', '{USER},{ORDER_ID}, {SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Shipment Details ', '&lt;h3&gt;&lt;strong&gt;YOUR ORDER HAS BEEN SHIPPED.&lt;/strong&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipment details have been updated for your order ({ORDER_ID})&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Date : {SHIPPING_DATETIME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Approx Delivery Date : {APPROX_DELIVERY_DATE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Carrier : {SHIPPING_CARRIER}&lt;/p&gt;\r\n\r\n&lt;p&gt;Tracking Number : {TRACKING_NUMBER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{SHIPPING_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(14, 'Password Update - Admin', '{USERNAME},{NEWPASSWORD}', 'Password Update', '&lt;p&gt;Hi admin..&lt;/p&gt;\r\n\r\n&lt;p&gt;Admin password has been updated successfully.&lt;/p&gt;\r\n\r\n&lt;p&gt;Find new login details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Username :&amp;nbsp;{USERNAME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password :&amp;nbsp;{NEWPASSWORD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(15, 'Order Status Update Shipping - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update Shipped', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order has been updated {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Find the Order Details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Status:&amp;nbsp;{CURRENT_STATE}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Date&amp;nbsp;:&amp;nbsp;{SHIPPING_DATETIME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Approx&amp;nbsp;Date&amp;nbsp;:&amp;nbsp;{APPROX_DELIVERY_DATE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Carrier : {SHIPPING_CARRIER}&lt;/p&gt;\r\n\r\n&lt;p&gt;Tracking Number : {TRACKING_NUMBER}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(16, 'Order Status Update - Confirmed- User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Confirmed', '&lt;h2&gt;&lt;strong&gt;ORDER HAS BEEN CONFIRMED&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order {ORDER_ID}&amp;nbsp;has been confirmed.&lt;/p&gt;\r\n\r\n&lt;p&gt;{SHIPPING_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order wiil be dispatched as soon as possible.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(21, 'Order Status Update - Invoiced - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Invoiced', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order has been invoiced&amp;nbsp;{ORDER_ID},&lt;/p&gt;\r\n\r\n&lt;p&gt;Invoice No : {INVOICE_NUMBER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{COMMENT}&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have any query, Please contact us at&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(17, 'Order Status Update - Canceled - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Canceled', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order ({ORDER_ID}) has been canceled due to some reasons.&lt;/p&gt;\r\n\r\n&lt;p&gt;you can ask about your queries at:&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(23, 'Order Success - By cheque - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT},{DATE}, {TIME},{CHEQUE_IN_ORDER_OF},{CHEQUE_SEND_ADDRESS},{PAYMENT_METHOD}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method : {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Cheque in favor of : {CHEQUE_IN_ORDER_OF}&lt;/p&gt;\r\n\r\n&lt;p&gt;Send us cheque at this address: {CHEQUE_SEND_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;After receiving cheque, we will process your order as soon as possible.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(18, 'Order Status Update - Payment Received - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Payment Received', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Received for your order ({ORDER_ID})&lt;/p&gt;\r\n\r\n&lt;p&gt;Find the Order Details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Status:&amp;nbsp;{CURRENT_STATE}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method : {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span class=&quot;help-block&quot;&gt;{COMMENT}&lt;/span&gt;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(19, 'Order Status Update - Completed - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Completed', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order has been completed&amp;nbsp;{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Come back soon to check new offers..&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(22, 'Order Success - Cash On Delivery - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD}, {DATE}, {TIME}, {PRODUCT_TABLE}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(20, 'Order Status Update - Download Permissions Granted - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Download Permissions Granted', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Permissions has been made for your order {ORDER_ID},&lt;/p&gt;\r\n\r\n&lt;p&gt;You can download your products in your account section.&lt;/p&gt;\r\n\r\n&lt;p&gt;or if you have query about how to download product Please contact us&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(24, 'Order Success - Paypal Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(25, 'Share Wishlist - User', '{USERNAME}, {SPECIAL_NOTES_BY_CUSTOMER}, {PRODUCT_DATA}', 'Wishlist Share', '&lt;p&gt;Hi &amp;nbsp;..&lt;/p&gt;\r\n\r\n&lt;p&gt;{USERNAME}&amp;nbsp;has share wishlist with you.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;{SPECIAL_NOTES_BY_CUSTOMER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_DATA}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(26, 'Common Header', '{LOGO_URL}, {SITE_NAME}, {STORE_NAME},{ADDRESS_LINE_1},{ADDRESS_LINE_2},{STORE_CITY},{STORE_STATE},{STORE_COUNTRY},{STORE_ZIP_CODE},{STORE_PHONE_NUMBER}', 'Header for emails', '&lt;div style=&quot;width:100%;height:auto;&quot;&gt;\r\n&lt;h2 style=&quot;margin-bottom:5px;&quot;&gt;&lt;img alt=&quot;{SITE_NAME}&quot; src=&quot;{LOGO_URL}&quot; style=&quot;width:200px&quot; /&gt;&lt;/h2&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;clear:both;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;hr /&gt;', NULL, '0', '{SITE_NAME}'),
(27, 'Common Footer', '{LOGO_URL}, {SITE_NAME}, {STORE_NAME},{ADDRESS_LINE_1},{ADDRESS_LINE_2},{STORE_CITY},{STORE_STATE},{STORE_COUNTRY},{STORE_ZIP_CODE},{STORE_PHONE_NUMBER},{SITE_URL}', 'Footer for emails', '&lt;hr /&gt;\r\n&lt;p&gt;{STORE_NAME}&lt;br /&gt;\r\n{STORE_PHONE_NUMBER}&lt;br /&gt;\r\n{SITE_URL}&lt;/p&gt;\r\n\r\n&lt;div style=&quot;clear:both;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(29, 'New Registration - Success - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}, {LOGIN_URL}', 'Registration Successful ', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nNEW account created with your email.&lt;br /&gt;\r\nUse the below credentials for logging in the account.&lt;/p&gt;\r\n\r\n&lt;p&gt;Login URL : {LOGIN_URL}&lt;/p&gt;\r\n\r\n&lt;p&gt;Username :&amp;nbsp;{EMAIL}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password :&amp;nbsp;{PASSWORD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(28, 'Order Success - Card Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
(30, 'Resend email confirmation - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL}, {CONFIRMATION_URL}, {LOGIN_URL}', 'Email confirmation', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nVerify your email by clicking on the link below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;{CONFIRMATION_URL}&lt;/a&gt;&lt;/p&gt;\r\n', 'html', '0', '{SITE_NAME}'),
(31, 'Email update- user', '{FIRSTNAME}, {LASTNAME}, {EMAIL}, {CONFIRMATION_URL}, {LOGIN_URL}', 'Email Updated', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nYou had successfully update your account email address.&lt;/p&gt;\r\n\r\n&lt;p&gt;Verify new email by clicking the link below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;{CONFIRMATION_URL}&lt;/a&gt;&lt;/p&gt;\r\n', 'html', '0', '{SITE_NAME}'),
(32, 'Password reminder email', '{NAME},{LAST_LOGIN},{USERNAME},{PASSWORD_RESET_LINK}', 'Password Reminder email from {SITE_NAME}', '&lt;p&gt;Hi, {NAME}&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;You have not logged in from the last &lt;strong&gt;{LAST_LOGIN} days.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;Please Login to your Account with&lt;br /&gt;\n&lt;b&gt;Username&lt;/b&gt; - {USERNAME}&lt;br /&gt;\n&lt;b&gt;Password&lt;/b&gt; - &lt;small&gt;As Defined By You.&lt;/small&gt;&lt;/p&gt;\n\n&lt;p&gt;If you have any problem logging in,&amp;nbsp;Please reset your Password at&lt;br /&gt;\n{PASSWORD_RESET_LINK}&lt;/p&gt;\n', '', '0', '{SITE_NAME}'),
(33, 'Product Availability Notification', '{SITE_NAME},{NAME},{PRODUCT_NAME},{PRODUCT_LINK}', 'Your Favourite Product is Available at {SITE_NAME}', '&lt;p&gt;Hi, {NAME}&lt;/p&gt;\r\n\r\n&lt;p&gt;It is to inform you that &lt;strong&gt;{PRODUCT_NAME}&lt;/strong&gt; is made Available at {SITE_NAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;You can buy the same at - &lt;span style=&quot;line-height: 1.6em;&quot;&gt;{PRODUCT_LINK}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Happy Shopping.&lt;/p&gt;\r\n', '', '0', '{SITE_NAME}'),
(34, 'Gift certificate email', '{NAME},{USERNAME},{GIFT_PRICE},{GIFT_CODE},{COMMENTS},{SENDER_NAME},{SENDER_EMAIL}', 'Gift certificate from {SITE_NAME}', '<p>Hi, {NAME}</p>\n\n<p><span style="line-height: 1.6em;"><strong>{SENDER_NAME}</strong>(</span><a href="{SENDER_EMAIL}" style="line-height: 20.7999992370605px;">{SENDER_EMAIL}</a>)<span style="line-height: 1.6em;"> have purchased a gift certificate from&nbsp;<strong>{SITE_NAME} </strong>worth <strong>{GIFT_PRICE}</strong>&nbsp;.</span></p>\n\n<p><span style="line-height: 1.6em;">PLease Use this code to credit </span><strong style="line-height: 20.7999992370605px;">${GIFT_PRICE}</strong><span style="line-height: 1.6em;"> in your wallet.</span></p>\n\n<p><b>Code</b> - {GIFT_CODE}</p>\n\n<p><b>If you want to use it now ,&nbsp;</b><a href="{SITE_NAME}">Click Here</a></p>\n', '', '0', '{SITE_NAME}');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_prize`
--

CREATE TABLE IF NOT EXISTS `cwebc_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `cwebc_prize`
--

INSERT INTO `cwebc_prize` (`id`, `prize`, `cost`) VALUES
(16, 'First Prize', '350'),
(17, 'Second Prize', '200$');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_question`
--

CREATE TABLE IF NOT EXISTS `cwebc_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `urlname` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `question_type` varchar(255) NOT NULL,
  `level` varchar(20) DEFAULT NULL,
  `round` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` enum('1','0') NOT NULL DEFAULT '0',
  `meta_title` varchar(255) NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `cwebc_question`
--

INSERT INTO `cwebc_question` (`id`, `name`, `urlname`, `description`, `position`, `category_id`, `question_type`, `level`, `round`, `is_active`, `is_deleted`, `meta_title`, `meta_desc`, `meta_keyword`, `date_add`, `date_upd`) VALUES
(38, 'Who is Sachin Tendulkar?', 'who-is-sachin-tendulkar?', '', 0, 39, 'Single True', 'Easy', 1, 0, '0', '', '', '', NULL, '2016-02-10 09:22:13'),
(44, 'The Homolographic projection has the correct representation of ?', 'the-homolographic-projection-has-the-correct-representation-of-?', '', 0, 30, 'Multiple True', 'Easy', 1, 0, '0', '', '', '', NULL, '2016-02-10 13:54:45'),
(45, 'The hazards of radiation belts include ?', 'the-hazards-of-radiation-belts-include-?', '', 0, 39, 'Single True', 'Easy', 1, 0, '0', '', '', '', NULL, '2016-02-10 13:57:49'),
(46, 'The great Victoria Desert is located in ?', 'the-great-victoria-desert-is-located-in-?', '', 0, 39, 'Multiple True', 'Easy', 3, 0, '0', '', '', '', NULL, '2016-02-10 14:00:34'),
(47, 'The intersecting lines drawn on maps and globes are ?', 'the-intersecting-lines-drawn-on-maps-and-globes-are-?', '', 0, 39, 'Single True', 'Intermediate', 4, 0, '0', '', '', '', NULL, '2016-02-10 14:02:56'),
(48, 'The greatest variety of animal and plant species is fund in ?', 'the-greatest-variety-of-animal-and-plant-species-is-fund-in-?', '', 0, 39, 'Multiple True', 'Intermediate', 2, 0, '0', '', '', '', NULL, '2016-02-10 14:05:33'),
(49, 'The highest mountains in Africa, which is not part of any mountains chain, is ?', 'the-highest-mountains-in-africa,-which-is-not-part-of-any-mountains-chain,-is-?', '', 0, 30, 'Single True', 'Intermediate', 2, 0, '0', '', '', '', NULL, '2016-02-10 14:07:51'),
(50, 'who is bill gates?', 'who-is-bill-gates?', '', 0, 39, 'Multiple True', 'Intermediate', 2, 0, '0', '', '', '', NULL, '2016-02-11 05:23:36'),
(51, 'GNLF stands for?', 'gnlf-stands-for?', '', 0, 39, 'Single True', 'Intermediate', 6, 0, '0', '', '', '', NULL, '2016-02-23 07:06:52'),
(52, 'Excessive secretion from the pituitary gland in the children results in', 'excessive-secretion-from-the-pituitary-gland-in-the-children-results-in', '', 0, 30, 'Multiple True', 'Intermediate', 3, 0, '0', '', '', '', NULL, '2016-02-23 07:09:53'),
(53, 'The Battle of Plassey was fought in', 'the-battle-of-plassey-was-fought-in', '', 0, 30, 'Multiple True', 'Challenging', 1, 0, '0', '', '', '', NULL, '2016-03-10 11:54:06'),
(54, 'The territory of Porus who offered strong resistance to Alexander was situated between the rivers of', 'the-territory-of-porus-who-offered-strong-resistance-to-alexander-was-situated-between-the-rivers-of', '', 0, 30, 'Single True', 'Challenging', 1, 0, '0', '', '', '', NULL, '2016-03-10 11:56:06'),
(55, 'The largest gulf in the world is', 'the-largest-gulf-in-the-world-is', '', 0, 30, 'Multiple True', 'Challenging', 2, 0, '0', '', '', '', NULL, '2016-03-10 11:58:34'),
(56, 'The hunting and gathering economy can support only', 'the-hunting-and-gathering-economy-can-support-only', '', 0, 30, 'Single True', 'Challenging', 2, 0, '0', '', '', '', NULL, '2016-03-10 12:20:02'),
(57, 'The knowledge about the topography of the ocean basins has been derived from', 'the-knowledge-about-the-topography-of-the-ocean-basins-has-been-derived-from', '', 0, 30, 'Single True', 'Challenging', 3, 0, '0', '', '', '', NULL, '2016-03-10 13:13:39'),
(58, 'The headquarters of the Commonwealth of Independent States (CIS), formed out of erstwhile USSR, is at', 'the-headquarters-of-the-commonwealth-of-independent-states-(cis),-formed-out-of-erstwhile-ussr,-is-at', '', 0, 30, 'Multiple True', 'Challenging', 4, 0, '0', '', '', '', NULL, '2016-03-10 13:15:20'),
(59, 'The headquarters of the Commonwealth of Independent States (CIS), formed out of erstwhile USSR, is at', 'the-headquarters-of-the-commonwealth-of-independent-states-(cis),-formed-out-of-erstwhile-ussr,-is-at', '', 0, 30, 'Multiple True', 'Challenging', 5, 0, '0', '', '', '', NULL, '2016-03-10 13:17:09'),
(60, 'In which year was Pulitzer Prize established?', 'in-which-year-was-pulitzer-prize-established?', '', 0, 39, 'Multiple True', 'Easy', 5, 0, '0', '', '', '', NULL, '2016-03-10 13:56:03'),
(61, 'B. C. Roy Award is given in the field of', 'b.-c.-roy-award-is-given-in-the-field-of', '', 0, 39, 'Single True', 'Easy', 3, 0, '0', '', '', '', NULL, '2016-03-11 13:51:51'),
(62, 'New One', 'new-one', '', 0, 30, 'Single True', '3', 2, 0, '0', '', '', '', NULL, '2016-05-19 13:05:21');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_question_quiz`
--

CREATE TABLE IF NOT EXISTS `cwebc_question_quiz` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `question_id` int(255) NOT NULL,
  `answer_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `level_id` varchar(255) NOT NULL,
  `time` time NOT NULL,
  `round` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_quest_ans`
--

CREATE TABLE IF NOT EXISTS `cwebc_quest_ans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quest_id` int(11) NOT NULL DEFAULT '0',
  `answer` text,
  `is_correct` enum('1','0') NOT NULL DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `cwebc_quest_ans`
--

INSERT INTO `cwebc_quest_ans` (`id`, `quest_id`, `answer`, `is_correct`, `position`, `date_add`) VALUES
(9, 32, 'cricket', '1', NULL, '2016-02-10 06:55:12'),
(10, 32, 'hockey player', '0', NULL, '2016-02-10 07:21:39'),
(11, 32, 'Football Player', '0', NULL, '2016-02-10 08:06:49'),
(12, 38, 'Crickter ', '1', 3, '2016-02-10 09:22:29'),
(29, 38, 'Hockey player', '0', 2, '2016-02-10 13:44:40'),
(30, 44, 'shape', '0', 1, '2016-02-10 13:55:28'),
(31, 44, 'baring', '0', 3, '2016-02-10 13:55:55'),
(32, 44, 'area', '1', 4, '2016-02-10 13:56:19'),
(33, 44, 'distance', '0', 2, '2016-02-10 13:56:39'),
(34, 45, 'deterioration of electronic circuits ', '0', 4, '2016-02-10 13:58:28'),
(35, 45, 'damage of solar cells of spacecraft ', '0', 1, '2016-02-10 13:58:49'),
(36, 45, 'adverse effect on living organisms', '0', 3, '2016-02-10 13:59:07'),
(37, 45, 'All of the above', '1', 2, '2016-02-10 13:59:29'),
(38, 46, 'Canada', '0', 1, '2016-02-10 14:01:00'),
(39, 46, 'West Africa', '0', 2, '2016-02-10 14:01:17'),
(40, 46, 'Australia', '1', 3, '2016-02-10 14:01:38'),
(41, 46, 'North America', '0', 4, '2016-02-10 14:02:00'),
(42, 47, 'latitudes', '0', 3, '2016-02-10 14:03:20'),
(43, 47, 'longitudes', '0', 1, '2016-02-10 14:03:44'),
(44, 47, 'geographic grids', '1', 4, '2016-02-10 14:04:05'),
(45, 47, 'None of the above', '0', 2, '2016-02-10 14:04:28'),
(46, 48, 'temperate grasslands', '0', 3, '2016-02-10 14:05:53'),
(47, 48, 'tropical moist forests', '1', 2, '2016-02-10 14:06:10'),
(48, 48, 'tundra regions', '0', 1, '2016-02-10 14:06:30'),
(49, 48, 'in hot deserts', '0', 4, '2016-02-10 14:06:47'),
(50, 49, 'Mt. Aconcagua', '0', 2, '2016-02-10 14:08:18'),
(51, 49, 'Mr. Kilimanjaro', '1', 1, '2016-02-10 14:08:30'),
(52, 49, 'Mt. Kosciusco', '0', 4, '2016-02-10 14:08:46'),
(53, 49, 'Mont Blanc', '0', 3, '2016-02-10 14:09:05'),
(54, 38, 'Football Player', '0', 1, '2016-02-10 14:10:44'),
(55, 38, 'actor', '0', 4, '2016-02-10 14:38:14'),
(58, 57, 'cdscds', '1', 3, '2016-02-18 08:17:04'),
(59, 0, 'hkjk', '1', 2, '2016-02-18 08:19:28'),
(60, 0, 'jkj', '0', 2, '2016-02-18 08:19:39'),
(61, 57, '[p', '0', 2, '2016-02-18 08:20:34'),
(62, 57, 'oiu', '0', 2, '2016-02-18 08:20:39'),
(65, 0, 'jjj', '0', 5, '2016-02-18 08:23:38'),
(66, 51, 'Gorkha National Liberation Front', '1', 2, '2016-02-23 07:07:36'),
(67, 51, 'Gross National Liberation Form', '0', 1, '2016-02-23 07:07:58'),
(68, 51, 'Both', '0', 3, '2016-02-23 07:08:17'),
(69, 51, 'None of the above', '0', 4, '2016-02-23 07:08:36'),
(70, 52, 'increased height', '1', 1, '2016-02-23 07:10:15'),
(71, 52, 'retarded growth', '0', 2, '2016-02-23 07:10:30'),
(72, 52, 'weakening of bones', '0', 3, '2016-02-23 07:10:42'),
(73, 52, 'None of the above', '0', 4, '2016-02-23 07:10:57'),
(75, 53, '1757', '1', 1, '2016-03-10 11:54:29'),
(76, 53, '1782', '0', 2, '2016-03-10 11:54:41'),
(77, 53, '1748', '0', 3, '2016-03-10 11:54:56'),
(78, 53, '1764', '0', 4, '2016-03-10 11:55:09'),
(79, 54, 'Sutlej and Beas', '0', 1, '2016-03-10 11:56:21'),
(80, 54, 'Jhelum and Chenab', '1', 2, '2016-03-10 11:56:38'),
(81, 54, 'Ravi and Chenab', '0', 3, '2016-03-10 11:56:53'),
(82, 54, 'Ganga and Yamuna', '0', 4, '2016-03-10 11:57:06'),
(83, 55, 'Gulf of Mexico', '1', 1, '2016-03-10 11:58:51'),
(84, 55, 'Persian Gulf', '0', 2, '2016-03-10 11:59:13'),
(85, 55, 'Gulf of Carpentaria', '0', 3, '2016-03-10 12:17:14'),
(86, 55, 'Gulf of Mannar', '0', 4, '2016-03-10 12:19:38'),
(87, 56, '1 person per sq. km', '1', 1, '2016-03-10 13:12:18'),
(88, 56, '3 persons per sq. km', '0', 2, '2016-03-10 13:12:32'),
(89, 56, '5 persons per sq. km', '0', 3, '2016-03-10 13:12:46'),
(90, 56, '7 persons per sq. km', '0', 4, '2016-03-10 13:13:00'),
(91, 57, 'seismic surveying', '0', 1, '2016-03-10 13:14:01'),
(92, 58, 'Minsk in Byelorussia', '1', 1, '2016-03-10 13:15:46'),
(93, 58, 'Moscow in Russia', '0', 2, '2016-03-10 13:16:01'),
(94, 58, 'Kiev in Ukraine', '0', 3, '2016-03-10 13:16:16'),
(95, 58, 'All of the above', '0', 4, '2016-03-10 13:16:31'),
(96, 61, 'Music', '0', 1, '2016-03-11 13:52:13'),
(97, 61, 'Journalism', '0', 2, '2016-03-11 13:52:26'),
(98, 61, 'Medicine', '1', 3, '2016-03-11 13:52:40'),
(99, 61, 'Environment', '0', 4, '2016-03-11 13:52:54'),
(100, 60, 'A', '0', 0, '2016-05-19 07:21:38'),
(101, 60, 'B', '1', NULL, '2016-05-19 07:21:49'),
(102, 60, 'C', '1', NULL, '2016-05-19 07:21:56'),
(103, 50, 'common man', '0', NULL, '2016-05-19 14:01:13'),
(104, 50, 'facebook founder', '0', NULL, '2016-05-19 14:01:23'),
(105, 50, 'apple founder', '1', NULL, '2016-05-19 14:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_round`
--

CREATE TABLE IF NOT EXISTS `cwebc_round` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `points` float NOT NULL,
  `level` varchar(20) NOT NULL,
  `total_time` int(50) NOT NULL DEFAULT '50',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `cwebc_round`
--

INSERT INTO `cwebc_round` (`id`, `points`, `level`, `total_time`) VALUES
(1, 100, 'EASY', 50),
(2, 100, 'EASY', 50),
(3, 100, 'EASY', 50),
(4, 100, 'EASY', 50),
(5, 100, 'EASY', 50),
(6, 100, 'INTERMEDIATE', 50),
(7, 100, 'INTERMEDIATE', 50),
(8, 100, 'INTERMEDIATE', 50),
(9, 100, 'INTERMEDIATE', 50),
(10, 100, 'INTERMEDIATE', 50),
(11, 100, 'CHALLENGING', 50),
(12, 100, 'CHALLENGING', 50),
(13, 100, 'CHALLENGING', 50),
(14, 100, 'CHALLENGING', 50),
(15, 100, 'CHALLENGING', 50);

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_setting`
--

CREATE TABLE IF NOT EXISTS `cwebc_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'select',
  `options` longtext NOT NULL,
  `hint` longtext NOT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=187 ;

--
-- Dumping data for table `cwebc_setting`
--

INSERT INTO `cwebc_setting` (`id`, `key`, `value`, `name`, `title`, `type`, `options`, `hint`, `position`, `status`) VALUES
(1, 'ABOUT_US_PAGE_ID', '2', 'pages', 'About us page Id', 'text', '', '', 0, 0),
(3, 'ADDRESS1', '', 'address', 'Company Address 1', 'text', '', '', 3, 1),
(4, 'ADDRESS2', '', 'address', 'Company Address 2', 'text', '', '', 5, 1),
(6, 'ADMIN_EMAIL', 'gomer.developer@gmail.com', 'email', 'admin email adress', 'text', '', '', 1, 1),
(9, 'BACKEND_STICKY_NAV_BAR', '0', 'design', 'Sticky navigation bar in admin', 'select', '', 'sticky navigation bar in admin section', 2, 1),
(23, 'CITY', '', 'address', 'City', 'text', '', '', 7, 1),
(25, 'CONTACT_US_PAGE', '3', 'pages', 'Contact Us Page Id', 'text', '', '', 0, 0),
(30, 'COUNTRY', '', 'address', 'Country', 'text', '', '', 11, 1),
(34, 'CURRENCY_SYMBOL', '$', 'general', 'currency symbol', 'text', '', '', 9, 1),
(38, 'DEFAULT_PAGE_META_TITLE', '%%PAGE_NAME%% | %%SITENAME%%', 'meta', 'Pages Default Meta Title', 'text', '', 'You can use %%PAGE_NAME%%, %%SITENAME%% for pages like sale, bestsellers<br/>\n%%PAGE_NAME%% - Name of the page.<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 26, 1),
(47, 'FACEBOOK_PAGE', '', 'social', 'Facebook Page ', 'text', '', '', 8, 1),
(49, 'FILL_META_DESC_AUTOMATICALLY', '1', 'meta', 'Auto generate meta description', 'select', '', 'Dynamically generate meta description from the page content.<br/>i.e. using short description from product', 4, 1),
(52, 'GOOGLE_AC', 'UA-53433480-1', 'seo', 'Google Analytic Code', 'text', '', '', 2, 1),
(53, 'GOOGLE_PLUS_PAGE', '', 'social', 'Google Plus Page ', 'text', '', '', 2, 1),
(54, 'GOOGLE_VC', '', 'seo', 'Google Verify Code', 'text', '', '', 4, 1),
(68, 'IS_SMTP', 'PHP_mail', 'SMTP', 'Mailer', 'select', 'PHP_mail,SMTP', 'use smpt or php mail function', 1, 1),
(74, 'MAINTANCE_MODE', '0', 'general', 'site in maintance mode', 'select', '', 'admin can visit site after logging in admin even in maintenance mode', 3, 1),
(75, 'MAIN_NAVIGATION_ID', '1', 'navigation', 'select navigation for main menu', 'text', '', '', 0, 1),
(76, 'MAXIMUM_META_DESC_LENGHT', '160', 'meta', 'Maximum Meta Description Length', 'text', '', 'Maximum meta description length for all modules', 3, 1),
(77, 'MAXIMUM_META_TITLE_LENGHT', '60', 'meta', 'Maximum Meta Title Length', 'text', '', 'Maximum meta title length for all modules', 1, 1),
(87, 'ORDER_PAGE_SIZE_ADMIN', '30', 'general', 'Orders per page in admin', 'text', '', '', 18, 1),
(92, 'PHONE_NUMBER', '9815107881', 'address', 'Sales Phone Number', 'text', '', '', 12, 1),
(99, 'SALES_EMAIL', 'gomer.developer@gmail.com', 'email', 'sales email address', 'text', '', '', 3, 1),
(115, 'SITE_NAME', 'Easy Access Education', 'general', 'website name', 'text', '', '', 1, 1),
(116, 'SITE_TAG_LINE', '', 'general', 'tag line of your site', 'textarea', '', '', 0, 0),
(118, 'SMTP_AUTH', '1', 'SMTP', 'SMTP Authentication', 'select', '', '', 10, 1),
(119, 'SMTP_HOST', 'smtp.gmail.com', 'SMTP', 'SMTP Host', 'text', '', '', 2, 1),
(120, 'SMTP_PASSWORD', 'developer123', 'SMTP', 'SMTP Password', 'text', '', '', 6, 1),
(121, 'SMTP_PORT', '465', 'SMTP', 'SMTP Port', 'text', '', '', 8, 1),
(122, 'SMTP_SECURITY', 'SSL', 'SMTP', 'SMTP Security', 'select', 'none,SSL,TLS', '', 9, 1),
(123, 'SMTP_USERNAME', 'gomer.developer@gmail.com', 'SMTP', 'SMTP Username', 'text', '', '', 3, 1),
(124, 'STATE', '', 'address', 'State', 'text', '', '', 8, 1),
(126, 'STORE_NAME', 'Shopping ', 'address', 'Store Name', 'text', '', '', 1, 1),
(127, 'SUPPORT_EMAIL', 'gomer.developer@gmail.com', 'email', 'support email address', 'text', '', '', 5, 1),
(129, 'TERMS_CONDITIONS_PAGE', '6', 'pages', 'Term and conditions page Id', 'text', '', '', 0, 0),
(130, 'TWITTER_PAGE', 'https://twitter.com/developers', 'social', 'Twitter Page', 'text', '', '', 6, 1),
(133, 'URL_REWRITE', '0', 'seo', 'Search Engine Friendly URL', 'select', '', 'set url rewrite', 1, 1),
(142, 'YOUTUBE_PAGE', '', 'social', 'Youtube Page ', 'text', '', '', 4, 1),
(143, 'ZIP_CODE', '', 'address', 'Zip Code', 'text', '', '', 9, 1),
(152, 'DATE_FORMAT_SETTINGS', 'F j, Y', 'general', 'date format', 'select', 'Y-m-d,m/d/Y,d/m/Y,d-M-Y,F j&#44; Y', 'this format will be used for all date values on frontend<br/>\r\nY-m-d - 2014-05-01<br/>\r\nm/d/Y - 05/01/2014<br/>\r\nd/m/Y - 01/05/2014<br/>\r\nd-M-Y - 01-May-2014<br/>\r\nF j&#44; Y - May 01&#44; 2014', 25, 1),
(153, 'TIME_FORMAT_SETTINGS', 'g:i A', 'general', 'time format', 'select', 'g:i a,g:i A,H:i', 'this time format will be used on frontend<br/>\r\ng:i a - 10:11 pm<br/>\r\ng:i A - 10:11 PM<br/>\r\nH:i - 22:11<br/>', 27, 1),
(175, 'EASY_TOTAL_ROUND', '2', 'easy', 'Total Rounds', 'text', '', 'Total Round in this level', 0, 1),
(176, 'EASY_QUESTION_PER_ROUND', '2', 'easy', 'Total Questions In Each Round', 'text', '', 'Total Question in each round', 0, 1),
(177, 'EASY_TOTAL_TIME', '50', 'easy', 'Total Time Of Round', 'text', '', 'Time to complete this level (In Secs)', 0, 1),
(178, 'EASY_SOCRE_PER_ROUND', '100', 'easy', 'Score Per Round', 'text', '', 'Total Score will be the score for one round in this level', 0, 1),
(179, 'INTERMEDIATE_TOTAL_ROUND', '3', 'intermediate', 'Total Rounds', 'text', '', 'Total Round in this level', 0, 1),
(180, 'INTERMEDIATE_QUESTION_PER_ROUND', '2', 'intermediate', 'Total Questions In Each Round', 'text', '', 'Total Question in each round', 0, 1),
(181, 'INTERMEDIATE_TOTAL_TIME', '10', 'intermediate', 'Total Time Of Round', 'text', '', 'Time to complete this level (In Secs)', 0, 1),
(182, 'INTERMEDIATE_SOCRE_PER_ROUND', '100', 'intermediate', 'Score Per Round', 'text', '', 'Total Score will be the score for one round in this level', 0, 1),
(183, 'CHALLENGING_TOTAL_ROUND', '5', 'challenging', 'Total Rounds', 'text', '', 'Total Round in this level', 0, 1),
(184, 'CHALLENGING_QUESTION_PER_ROUND', '2', 'challenging', 'Total Questions In Each Round', 'text', '', 'Total Question in each round', 0, 1),
(185, 'CHALLENGING_TOTAL_TIME', '10', 'challenging', 'Total Time Of Round', 'text', '', 'Time to complete this level (In Secs)', 0, 1),
(186, 'CHALLENGING_SOCRE_PER_ROUND', '500', 'challenging', 'Score Per Round', 'text', '', 'Total Score will be the score for one round in this level', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_state`
--

CREATE TABLE IF NOT EXISTS `cwebc_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `iso_code` varchar(255) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `state_name` (`name`),
  KEY `iso_code` (`iso_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `cwebc_state`
--

INSERT INTO `cwebc_state` (`id`, `country_id`, `name`, `iso_code`, `is_active`, `is_deleted`, `date_add`) VALUES
(1, 40, 'Alberta', 'AB', '1', '0', '2015-11-26 08:59:45'),
(2, 40, 'British Columbia', 'BC', '1', '0', '2015-11-26 08:59:45'),
(3, 40, 'Manitoba', 'MB', '1', '0', '2015-11-26 08:59:45'),
(4, 40, 'New Brunswick', 'NB', '1', '0', '2015-11-26 08:59:45'),
(5, 40, 'Newfoundland and Labrador', 'NL', '1', '0', '2015-11-26 08:59:45'),
(6, 40, 'Northwest Territories', 'NT', '1', '0', '2015-11-26 08:59:45'),
(7, 40, 'Nova Scotia', 'NS', '1', '0', '2015-11-26 08:59:45'),
(8, 40, 'Nunavut', 'NU', '1', '0', '2015-11-26 08:59:45'),
(9, 40, 'Ontario', 'ON', '1', '0', '2015-11-26 08:59:45'),
(10, 40, 'Prince Edward Island', 'PE', '1', '0', '2015-11-26 08:59:45'),
(11, 40, 'Quebec', 'QC', '1', '0', '2015-11-26 08:59:45'),
(12, 40, 'Saskatchewan', 'SK', '1', '0', '2015-11-26 08:59:45'),
(13, 40, 'Yukon', 'YT', '1', '0', '2015-11-26 08:59:45'),
(14, 236, 'Alabama', 'AL', '1', '0', '2015-11-26 08:59:45'),
(15, 236, 'Alaska', 'AK', '1', '0', '2015-11-26 08:59:45'),
(16, 236, 'Arizona', 'AZ', '1', '0', '2015-11-26 08:59:45'),
(17, 236, 'Arkansas', 'AR', '1', '0', '2015-11-26 08:59:45'),
(18, 236, 'California', 'CA', '1', '0', '2015-11-26 08:59:45'),
(19, 236, 'Colorado', 'CO', '1', '0', '2015-11-26 08:59:45'),
(20, 236, 'Connecticut', 'CT', '1', '0', '2015-11-26 08:59:45'),
(21, 236, 'Delaware', 'DE', '1', '0', '2015-11-26 08:59:45'),
(22, 236, 'District of Columbia', 'DC', '1', '0', '2015-11-26 08:59:45'),
(23, 236, 'Florida', 'FL', '1', '0', '2015-11-26 08:59:45'),
(24, 236, 'Georgia', 'GA', '1', '0', '2015-11-26 08:59:45'),
(25, 236, 'Hawaii', 'HI', '1', '0', '2015-11-26 08:59:45'),
(26, 236, 'Idaho', 'ID', '1', '0', '2015-11-26 08:59:45'),
(27, 236, 'Illinois', 'IL', '1', '0', '2015-11-26 08:59:45'),
(28, 236, 'Indiana', 'IN', '1', '0', '2015-11-26 08:59:45'),
(29, 236, 'Iowa', 'IA', '1', '0', '2015-11-26 08:59:45'),
(30, 236, 'Kansas', 'KS', '1', '0', '2015-11-26 08:59:45'),
(31, 236, 'Kentucky', 'KY', '1', '0', '2015-11-26 08:59:45'),
(32, 236, 'Louisiana', 'LA', '1', '0', '2015-11-26 08:59:45'),
(33, 236, 'Maine', 'ME', '1', '0', '2015-11-26 08:59:45'),
(34, 236, 'Maryland', 'MD', '1', '0', '2015-11-26 08:59:45'),
(35, 236, 'Massachusetts', 'MA', '1', '0', '2015-11-26 08:59:45'),
(36, 236, 'Michigan', 'MI', '1', '0', '2015-11-26 08:59:45'),
(37, 236, 'Minnesota', 'MN', '1', '0', '2015-11-26 08:59:45'),
(38, 236, 'Mississippi', 'MS', '1', '0', '2015-11-26 08:59:45'),
(39, 236, 'Missouri', 'MO', '1', '0', '2015-11-26 08:59:45'),
(40, 236, 'Montana', 'MT', '1', '0', '2015-11-26 08:59:45'),
(41, 236, 'Nebraska', 'NE', '1', '0', '2015-11-26 08:59:45'),
(42, 236, 'Nevada', 'NV', '1', '0', '2015-11-26 08:59:45'),
(43, 236, 'New Hampshire', 'NH', '1', '0', '2015-11-26 08:59:45'),
(44, 236, 'New Jersey', 'NJ', '1', '0', '2015-11-26 08:59:45'),
(45, 236, 'New Mexico', 'NM', '1', '0', '2015-11-26 08:59:45'),
(46, 236, 'New York', 'NY', '1', '0', '2015-11-26 08:59:45'),
(47, 236, 'North Carolina', 'NC', '1', '0', '2015-11-26 08:59:45'),
(48, 236, 'North Dakota', 'ND', '1', '0', '2015-11-26 08:59:45'),
(49, 236, 'Ohio', 'OH', '1', '0', '2015-11-26 08:59:45'),
(50, 236, 'Oklahoma', 'OK', '1', '0', '2015-11-26 08:59:45'),
(51, 236, 'Oregon', 'OR', '1', '0', '2015-11-26 08:59:45'),
(52, 236, 'Pennsylvania', 'PA', '1', '0', '2015-11-26 08:59:45'),
(53, 236, 'Rhode Island', 'RI', '1', '0', '2015-11-26 08:59:45'),
(54, 236, 'South Carolina', 'SC', '1', '0', '2015-11-26 08:59:45'),
(55, 236, 'South Dakota', 'SD', '1', '0', '2015-11-26 08:59:45'),
(56, 236, 'Tennessee', 'TN', '1', '0', '2015-11-26 08:59:45'),
(57, 236, 'Texas', 'TX', '1', '0', '2015-11-26 08:59:45'),
(58, 236, 'Utah', 'UT', '1', '0', '2015-11-26 08:59:45'),
(59, 236, 'Vermont', 'VT', '1', '0', '2015-11-26 08:59:45'),
(60, 236, 'Virginia', 'VA', '1', '0', '2015-11-26 08:59:45'),
(61, 236, 'Washington', 'WA', '1', '0', '2015-11-26 08:59:45'),
(62, 236, 'West Virginia', 'WV', '1', '0', '2015-11-26 08:59:45'),
(63, 236, 'Wisconsin', 'WI', '1', '0', '2015-11-26 08:59:45'),
(64, 236, 'Wyoming', 'WY', '1', '0', '2015-11-26 08:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_user`
--

CREATE TABLE IF NOT EXISTS `cwebc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `total_visit` int(11) NOT NULL DEFAULT '0',
  `last_visit_ip_address` varchar(40) DEFAULT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `is_email_verified` enum('1','0') NOT NULL DEFAULT '0',
  `clinic_name` varchar(255) NOT NULL,
  `web_address` varchar(1000) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `education` varchar(255) DEFAULT NULL,
  `reg_info` varchar(1000) NOT NULL,
  `practitioner_type` varchar(255) NOT NULL,
  `shipping_firstname` varchar(255) DEFAULT NULL,
  `shipping_lastname` varchar(255) NOT NULL,
  `shipping_address1` varchar(255) NOT NULL,
  `shipping_address2` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `zip_code` varchar(255) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `is_newsletter` enum('1','0') NOT NULL DEFAULT '0',
  `newsletter_add_date` datetime DEFAULT NULL,
  `forgot_password_token` varchar(255) NOT NULL,
  `email_confirmation_token` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_login` (`username`,`password`),
  KEY `city_id` (`city_id`),
  KEY `state_id` (`state_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `cwebc_user`
--

INSERT INTO `cwebc_user` (`id`, `username`, `password`, `ip_address`, `last_visit`, `total_visit`, `last_visit_ip_address`, `is_active`, `is_deleted`, `is_email_verified`, `clinic_name`, `web_address`, `firstname`, `lastname`, `education`, `reg_info`, `practitioner_type`, `shipping_firstname`, `shipping_lastname`, `shipping_address1`, `shipping_address2`, `phone`, `mobile`, `city_id`, `zip_code`, `state_id`, `country_id`, `is_newsletter`, `newsletter_add_date`, `forgot_password_token`, `email_confirmation_token`, `file`, `date_add`, `date_upd`) VALUES
(1, 'test@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '127.0.0.1', NULL, 0, NULL, '1', '1', '1', 'xdbdfb', '', 'sgs', 'dfgbdbh', '23', '', '', 'male', '', '', '', '', 0, NULL, '', NULL, NULL, '0', '2015-12-23 01:14:49', '', NULL, '1992-12-10', '2015-12-23 01:14:49', '2016-02-11 09:11:21'),
(2, 'sudhir@cwebconsultants.com', 'd8578edf8458ce06fbc5bb76a58c5ca4', '127.0.0.1', '2016-01-11 06:25:44', 21, '::1', '1', '1', '0', 'zxcvbnm', 'http://yahoo.com', 'qwerty', 'asdfg', 'qwertyu', 'srgr ertheeheh', 'demo1', 'test', 'test', 'we4gwv we54 yge5 yhe tjhe tjhdrtj dtjtyj t', 'd htrjh yjrtdyjrtyj rtj6rtyj7rtyjdrtyjerths ser wey ery ert ye5u', '34567899', 1234567890, NULL, '12345', 23, 236, '0', NULL, '474941272181', '4c2f1n9o2q', NULL, '2015-12-24 04:36:30', '2016-02-11 12:00:07'),
(5, 'rocky.developer@gmail.com', '', '122.176.82.67', '2016-01-05 00:27:05', 5, '122.176.82.67', '1', '0', '1', '', '', 'Rocky', 'Singh', NULL, '', '', NULL, '', '', '', '', 0, NULL, '', NULL, NULL, '0', '2016-02-11 06:07:22', '', NULL, NULL, '2016-01-04 07:01:10', '2016-02-26 06:21:53'),
(6, 'rocky.developer001@gmail.com', '098f6bcd4621d373cade4e832627b4f6', '127.0.0.1', NULL, 0, NULL, '1', '1', '1', '', '', 'rocky', 'test', NULL, '', '', NULL, '', '', '', '', 0, NULL, '', NULL, NULL, '1', '2016-02-11 02:47:19', '', NULL, NULL, '2016-02-11 02:43:33', '2016-02-11 12:05:35'),
(63, 'admin@gmail.com', 'admin', NULL, NULL, 0, NULL, '1', '1', '0', '', '', 'admin', 'admin', NULL, '', '', NULL, '', '', '', '1234567890', 0, NULL, '', NULL, NULL, '0', NULL, '', NULL, NULL, NULL, '2016-03-07 05:08:37'),
(64, 'dd@gmai.com', 'ddfsdfdsf', NULL, NULL, 0, NULL, '1', '1', '0', '', '', '', '', NULL, '', '', NULL, '', '', '', '', 0, NULL, '', NULL, NULL, '0', NULL, '', NULL, NULL, NULL, '2016-02-25 13:10:52'),
(65, 'admin11@gmail.com', '123456', '127.0.0.1', NULL, 0, NULL, '1', '0', '1', '', '', 'admin', 'test', NULL, '', '', NULL, '', '', '', '', 0, NULL, '', NULL, NULL, '0', '2016-02-25 05:10:43', '', NULL, NULL, '2016-02-25 05:10:43', '2016-03-31 08:00:28'),
(66, 'aks@gmail.com', '098f6bcd4621d373cade4e832627b4f6', '127.0.0.1', NULL, 0, NULL, '0', '1', '1', '', '', 'akkk', 'test', NULL, '', '', NULL, '', '', '', '', 0, NULL, '', NULL, NULL, '0', '2016-02-25 05:22:18', '', NULL, NULL, '2016-02-25 05:22:18', '2016-02-25 13:22:25'),
(68, 'ambika@cwebconsultants.com', 'ambika', NULL, NULL, 0, NULL, '1', '0', '0', '', '', 'ambika', 'sharma', NULL, '', '', NULL, '', '', '', '1212121212', 0, NULL, '', NULL, NULL, '0', NULL, '', NULL, NULL, NULL, '2016-03-02 13:48:14'),
(70, 'pawan@cwebconsultants.com', '123456', NULL, NULL, 0, NULL, '1', '0', '0', '', '', 'pawan', 'kumar', NULL, '', '', NULL, '', '', '', '456789123', 0, NULL, '', NULL, NULL, '0', NULL, '', NULL, NULL, NULL, '2016-04-06 08:58:42'),
(71, 'clay.developer@gmaill.com', 'developer', NULL, NULL, 0, NULL, '1', '0', '0', '', '', 'Clay', 'Dev', NULL, '', '', NULL, '', '', '', '546545454', 0, NULL, '', NULL, NULL, '0', NULL, '', NULL, NULL, NULL, '2016-05-19 05:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_user_points`
--

CREATE TABLE IF NOT EXISTS `cwebc_user_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `round_id` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `points` float DEFAULT NULL,
  `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Dumping data for table `cwebc_user_points`
--

INSERT INTO `cwebc_user_points` (`id`, `cat_id`, `round_id`, `user_id`, `points`, `date_add`) VALUES
(113, 39, 1, 71, 100, '2016-05-19 01:54:16'),
(114, 39, 2, 71, 100, '2016-05-19 01:56:25'),
(115, 39, 3, 71, 100, '2016-05-19 01:56:33'),
(116, 39, 4, 71, 100, '2016-05-19 01:57:11'),
(117, 39, 5, 71, 100, '2016-05-19 01:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `cwebc_web_stat`
--

CREATE TABLE IF NOT EXISTS `cwebc_web_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) DEFAULT NULL,
  `on_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=162 ;

--
-- Dumping data for table `cwebc_web_stat`
--

INSERT INTO `cwebc_web_stat` (`id`, `ip_address`, `on_date`) VALUES
(1, '127.0.0.1', '2016-02-19 05:43:28'),
(2, '127.0.0.1', '2016-02-19 10:32:11'),
(3, '127.0.0.1', '2016-02-22 04:32:01'),
(4, '127.0.0.1', '2016-02-22 10:32:57'),
(5, '127.0.0.1', '2016-02-23 04:38:03'),
(6, '127.0.0.1', '2016-02-23 10:32:00'),
(7, '127.0.0.1', '2016-02-23 10:49:40'),
(8, '127.0.0.1', '2016-02-24 04:41:24'),
(9, '127.0.0.1', '2016-02-24 05:09:37'),
(10, '127.0.0.1', '2016-02-24 06:05:48'),
(11, '127.0.0.1', '2016-02-24 08:20:14'),
(12, '127.0.0.1', '2016-02-24 09:00:35'),
(13, '127.0.0.1', '2016-02-24 11:14:22'),
(14, '127.0.0.1', '2016-02-24 11:27:55'),
(15, '127.0.0.1', '2016-02-24 12:41:31'),
(16, '127.0.0.1', '2016-02-25 04:44:46'),
(17, '127.0.0.1', '2016-02-25 06:53:05'),
(18, '127.0.0.1', '2016-02-25 09:28:15'),
(19, '127.0.0.1', '2016-02-25 09:28:15'),
(20, '127.0.0.1', '2016-02-25 09:28:16'),
(21, '127.0.0.1', '2016-02-25 09:28:18'),
(22, '127.0.0.1', '2016-02-25 09:28:20'),
(23, '127.0.0.1', '2016-02-25 09:30:56'),
(24, '127.0.0.1', '2016-02-25 10:32:30'),
(25, '127.0.0.1', '2016-02-25 11:40:01'),
(26, '127.0.0.1', '2016-02-25 11:41:44'),
(27, '127.0.0.1', '2016-02-25 11:42:31'),
(28, '127.0.0.1', '2016-02-25 11:44:38'),
(29, '127.0.0.1', '2016-02-25 11:49:31'),
(30, '127.0.0.1', '2016-02-25 11:50:50'),
(31, '127.0.0.1', '2016-02-25 11:56:14'),
(32, '127.0.0.1', '2016-02-25 11:56:43'),
(33, '127.0.0.1', '2016-02-25 11:58:39'),
(34, '127.0.0.1', '2016-02-25 12:00:57'),
(35, '127.0.0.1', '2016-02-25 12:05:00'),
(36, '127.0.0.1', '2016-02-25 12:08:03'),
(37, '127.0.0.1', '2016-02-25 12:35:01'),
(38, '127.0.0.1', '2016-02-25 12:39:29'),
(39, '127.0.0.1', '2016-02-25 12:49:07'),
(40, '127.0.0.1', '2016-02-25 12:52:54'),
(41, '127.0.0.1', '2016-02-25 12:53:00'),
(42, '127.0.0.1', '2016-02-25 12:58:45'),
(43, '127.0.0.1', '2016-02-25 13:04:40'),
(44, '127.0.0.1', '2016-02-25 13:04:54'),
(45, '127.0.0.1', '2016-02-25 13:06:56'),
(46, '127.0.0.1', '2016-02-25 13:20:31'),
(47, '127.0.0.1', '2016-02-25 13:20:49'),
(48, '127.0.0.1', '2016-02-25 13:23:57'),
(49, '127.0.0.1', '2016-02-25 13:24:07'),
(50, '127.0.0.1', '2016-02-25 13:31:48'),
(51, '127.0.0.1', '2016-02-25 13:47:01'),
(52, '127.0.0.1', '2016-02-25 13:52:57'),
(53, '127.0.0.1', '2016-02-25 13:53:22'),
(54, '127.0.0.1', '2016-02-26 04:36:07'),
(55, '127.0.0.1', '2016-02-26 04:39:53'),
(56, '127.0.0.1', '2016-02-26 04:52:28'),
(57, '127.0.0.1', '2016-02-26 05:12:20'),
(58, '122.173.210.122', '2016-02-26 05:16:13'),
(59, '122.176.82.67', '2016-02-26 05:16:49'),
(60, '122.176.82.67', '2016-02-26 06:17:50'),
(61, '122.173.210.122', '2016-02-26 08:11:14'),
(62, '122.173.210.122', '2016-02-26 10:38:55'),
(63, '122.173.210.122', '2016-02-26 12:32:03'),
(64, '122.173.210.122', '2016-02-26 12:32:15'),
(65, '122.173.210.122', '2016-02-26 12:33:33'),
(66, '122.173.210.122', '2016-02-26 12:34:55'),
(67, '122.176.82.67', '2016-02-26 13:30:09'),
(68, '122.173.210.122', '2016-02-26 13:30:34'),
(69, '122.173.210.122', '2016-02-26 13:36:15'),
(70, '122.176.82.67', '2016-02-26 14:15:13'),
(71, '122.173.210.122', '2016-02-26 14:18:37'),
(72, '122.176.82.67', '2016-02-26 14:24:57'),
(73, '122.176.82.67', '2016-02-26 14:32:22'),
(74, '122.173.230.121', '2016-02-29 04:38:25'),
(75, '122.173.230.121', '2016-02-29 05:46:46'),
(76, '122.173.230.121', '2016-02-29 06:04:30'),
(77, '122.173.230.121', '2016-02-29 06:15:00'),
(78, '122.176.82.67', '2016-02-29 09:21:46'),
(79, '122.176.82.67', '2016-02-29 10:31:44'),
(80, '122.173.230.121', '2016-02-29 10:50:58'),
(81, '122.176.82.67', '2016-02-29 13:18:05'),
(82, '122.176.82.67', '2016-02-29 14:23:53'),
(83, '122.176.82.67', '2016-02-29 14:49:05'),
(84, '122.173.230.121', '2016-02-29 15:00:12'),
(85, '122.176.82.67', '2016-03-01 04:44:10'),
(86, '122.176.82.67', '2016-03-01 05:19:27'),
(87, '122.173.20.188', '2016-03-02 13:45:10'),
(88, '122.173.20.188', '2016-03-02 13:45:32'),
(89, '122.173.20.188', '2016-03-02 13:46:20'),
(90, '122.173.20.188', '2016-03-02 13:46:36'),
(91, '122.173.20.188', '2016-03-02 14:15:22'),
(92, '122.173.20.188', '2016-03-02 14:24:32'),
(93, '122.176.82.67', '2016-03-02 14:25:46'),
(94, '122.173.20.188', '2016-03-02 14:30:20'),
(95, '122.176.82.67', '2016-03-03 11:12:53'),
(96, '122.176.82.67', '2016-03-03 11:13:53'),
(97, '122.176.82.67', '2016-03-03 12:31:11'),
(98, '122.176.82.67', '2016-03-03 12:31:22'),
(99, '122.176.82.67', '2016-03-03 12:31:38'),
(100, '122.176.82.67', '2016-03-03 14:02:55'),
(101, '122.176.82.67', '2016-03-03 14:11:38'),
(102, '122.176.82.67', '2016-03-03 14:14:04'),
(103, '122.176.82.67', '2016-03-03 14:14:15'),
(104, '122.176.82.67', '2016-03-03 14:17:33'),
(105, '122.176.82.67', '2016-03-03 14:28:54'),
(106, '122.176.82.67', '2016-03-03 14:34:13'),
(107, '122.176.82.67', '2016-03-03 14:34:19'),
(108, '203.134.198.90', '2016-03-03 22:03:15'),
(109, '122.176.82.67', '2016-03-04 04:38:13'),
(110, '122.173.114.102', '2016-03-04 04:58:48'),
(111, '122.173.114.102', '2016-03-04 05:16:51'),
(112, '1.39.32.56', '2016-03-04 10:26:56'),
(113, '122.173.114.102', '2016-03-04 11:26:13'),
(114, '122.176.82.67', '2016-03-04 14:18:11'),
(115, '122.176.82.67', '2016-03-04 14:37:41'),
(116, '1.39.32.56', '2016-03-04 14:55:53'),
(117, '122.173.115.124', '2016-03-05 10:57:15'),
(118, '122.173.175.131', '2016-03-07 04:37:28'),
(119, '122.176.82.67', '2016-03-07 05:01:04'),
(120, '122.173.175.131', '2016-03-07 05:16:25'),
(121, '122.176.82.67', '2016-03-07 09:13:56'),
(122, '122.176.82.67', '2016-03-08 08:43:36'),
(123, '122.173.49.155', '2016-03-09 13:40:36'),
(124, '122.173.49.155', '2016-03-09 14:19:52'),
(125, '122.173.49.155', '2016-03-09 15:27:14'),
(126, '122.173.206.135', '2016-03-10 04:50:57'),
(127, '122.173.206.135', '2016-03-10 08:47:03'),
(128, '122.176.82.67', '2016-03-10 11:33:16'),
(129, '122.176.82.67', '2016-03-10 11:35:51'),
(130, '122.176.82.67', '2016-03-10 11:53:22'),
(131, '122.173.172.254', '2016-03-10 12:54:27'),
(132, '122.176.82.67', '2016-03-10 13:33:04'),
(133, '122.176.82.67', '2016-03-10 13:54:31'),
(134, '122.176.82.67', '2016-03-10 14:14:46'),
(135, '122.173.34.81', '2016-03-11 09:26:05'),
(136, '122.176.82.67', '2016-03-11 13:44:53'),
(137, '122.173.34.81', '2016-03-11 14:17:06'),
(138, '1.39.33.186', '2016-03-12 15:25:17'),
(139, '1.39.33.186', '2016-03-12 15:27:31'),
(140, '1.39.33.186', '2016-03-12 15:31:02'),
(141, '1.39.33.40', '2016-03-14 11:33:56'),
(142, '122.176.82.67', '2016-03-14 14:05:39'),
(143, '1.39.33.190', '2016-03-14 14:38:15'),
(144, '1.39.32.190', '2016-03-14 16:06:29'),
(145, '1.39.32.190', '2016-03-14 16:10:45'),
(146, '122.173.23.150', '2016-03-18 14:06:27'),
(147, '122.173.187.254', '2016-03-28 12:19:15'),
(148, '122.173.187.254', '2016-03-28 12:19:36'),
(149, '122.176.82.67', '2016-03-28 14:13:34'),
(150, '122.173.217.101', '2016-03-29 11:57:20'),
(151, '122.176.82.67', '2016-03-30 06:16:44'),
(152, '122.176.82.67', '2016-03-30 13:27:04'),
(153, '::1', '2016-03-31 07:44:49'),
(154, '::1', '2016-04-06 05:04:29'),
(155, '::1', '2016-04-07 05:23:52'),
(156, '::1', '2016-04-07 08:04:42'),
(157, '::1', '2016-04-07 08:37:20'),
(158, '::1', '2016-04-11 04:56:57'),
(159, '::1', '2016-05-11 05:09:53'),
(160, '::1', '2016-05-19 05:07:05'),
(161, '::1', '2016-05-19 14:27:53');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cwebc_admin_user`
--
ALTER TABLE `cwebc_admin_user`
  ADD CONSTRAINT `cwebc_admin_user_ibfk_1` FOREIGN KEY (`admin_role_id`) REFERENCES `cwebc_admin_user_role` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `cwebc_admin_user_activity_log`
--
ALTER TABLE `cwebc_admin_user_activity_log`
  ADD CONSTRAINT `cwebc_admin_user_activity_log_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `cwebc_admin_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cwebc_city`
--
ALTER TABLE `cwebc_city`
  ADD CONSTRAINT `cwebc_city_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `cwebc_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cwebc_city_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `cwebc_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cwebc_state`
--
ALTER TABLE `cwebc_state`
  ADD CONSTRAINT `cwebc_state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `cwebc_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cwebc_user`
--
ALTER TABLE `cwebc_user`
  ADD CONSTRAINT `cwebc_user_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cwebc_city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `cwebc_user_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `cwebc_state` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `cwebc_user_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `cwebc_country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
